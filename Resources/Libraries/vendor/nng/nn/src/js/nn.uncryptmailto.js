
(function($nn) {

	function decryptCharcode(n,start,end,offset) {
		n = n + offset;
		if (offset > 0 && n > end) {
			n = start + (n - end - 1);
		} else if (offset < 0 && n < start) {
			n = end - (start - n - 1);
		}
		return String.fromCharCode(n);
	}

	function decryptString(enc,offset) {
		var dec = "";
		var len = enc.length;
		for(var i=0; i < len; i++) {
			var n = enc.charCodeAt(i);
			if (n >= 0x2B && n <= 0x3A) {
				dec += decryptCharcode(n,0x2B,0x3A,offset);	/* 0-9 . , - + / : */
			} else if (n >= 0x40 && n <= 0x5A) {
				dec += decryptCharcode(n,0x40,0x5A,offset);	/* A-Z @ */
			} else if (n >= 0x61 && n <= 0x7A) {
				dec += decryptCharcode(n,0x61,0x7A,offset);	/* a-z */
			} else {
				dec += enc.charAt(i);
			}
		}
		return dec;
	}

	window.nn_uncryptmailto = function( s, offset ) {
		location.href = decryptString( s, offset*1 );
	}

	/**
	 * Replace `name<i></i>99grad.de` with `name@99grad.de` after 
	 * short timeout to increase UX but hide it from bots.
	 * 
	 */
	$nn(function () {
		setTimeout(() => {
			$nn('[href*="nn_uncryptmailto"] i').each(function () {
				$nn(this).replace('@');
			});
		}, 1000)
	});
	
})(window.$nn);
