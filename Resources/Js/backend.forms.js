(function ($) {
	$(function () {

		// https://developer.wordpress.org/plugins/javascript/ajax/

		// Diese Variable wird durch `wp_localize_script()` in `\nn\wp::Form()->registerAjaxFormHandler()` gesetzt
		var gSettings = window.nnhelpers;

		/**
		 * Box zum Auswählen von Relationen.
		 * 
		 */
		$('[data-nnwp-metabox="relation"]').each(function () {

			var $me = $(this);
			var config = $me.data().config;

			var $search = $me.find('[type="search"]');
			var $results = $me.find('.results');
			$search.suggest([], {setValue:false} );

			var $template = $me.find('.template').clone();
			$me.find('.template').remove();

			// Autosuggest: Eintrag aus Liste wurde ausgewählt
			$search.on('nn.selected.suggest', function (obj) {
				$search.val('').suggest([]);
				result_selected( obj.detail );
			})

			// Löschen einer Zeile
			$me.on('click', '.remove', function () {
				$(this).closest('li').remove();
			});

			// Sortierbarkeit
			if (jQuery.fn.sortable) {
				jQuery($results.first()).sortable();
			}

			var lastSword = '';

			var iv;
			$search.keyup(function () {
				var sword = $(this).val();
				if (sword == lastSword) return;
				lastSword = sword;
				if (sword.length >= 2) {
					clearTimeout( iv );
					iv = setTimeout( function () {
						load_results( sword );
					}, 200);
				}
			});

			function load_results( sword ) {
				var data = {
					_nonce: gSettings._nonce,
					type: 'nnwp_ajax',
					api: 'relation',
					sword: sword,
					config: config
				}
				$.post( gSettings.ajax_url, data ).done(function(response) {
					var results =  (JSON.parse( response ) || {}).results;
					$search.suggest( results );
				}).fail(function ( err ) {
					console.log( err.status, err.statusText );
				});
			}

			function result_selected( obj ) {
				var $item = $template.clone();
				$item.find('span').text(obj.title);				
				$item.find('input').val(obj.ID);
				$results.append( $item.html() );
			}

		});

		/**
		 * Box für Media
		 * 
		 */
		$('[data-nnwp-metabox="media"]').each(function () {

			var $me = $(this);
			var config = $me.data().config;

			var $addBtn = $me.find('.add');
			var meta_gallery_frame;
			var api = window.wp.media;
			var images = [];

			var $results = $me.find('.results');
			var $template = $me.find('.template').clone();
			$me.find('.template').remove();

			// Löschen einer Zeile
			$me.on('click', '.remove', function () {
				$(this).closest('.item').remove();
			});

			// Sortierbarkeit
			if (jQuery.fn.sortable) {
				jQuery($results.first()).sortable();
			}

			$addBtn.click(function () {
				
				// falls bereits geöffnet: Nicht erneut initialisieren
				if (meta_gallery_frame) {
					meta_gallery_frame.open();
					return false;
				}

				// Modal vorbereiten
				meta_gallery_frame = api.frames.meta_gallery_frame = wp.media({
					//title: 'Medien wählen',
					//button: { text:  'Super!' },
					library: { type: 'image' },
					multiple: true
				});

				// Wenn Bilder ausgewählt wurden
				meta_gallery_frame.on('select', function() {
					var selection = meta_gallery_frame.state().get('selection');
					selection.each(function(attachment) {
						console.log(attachment);
						var obj = {
							id: attachment.attributes.id,
							url: attachment.attributes.url
						};
						images.push(obj);
					});
					build_gallery();
				});

				// Modal öffnen
				meta_gallery_frame.open();

				// Gallery aktualisieren
				function build_gallery() {
					$results.empty();
					images.forEach(function (obj) {
						var $item = $template.clone();
						$item.find('.image').css({backgroundImage:'url(' + obj.url + ')'});
						$item.find('input').val( obj.id );
						$results.append( $item.html() );
					});
				}

				return false;
			});
		});

	});
})(window.$nn);