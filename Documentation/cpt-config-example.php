<?php

$cptConfig = [
	'labels' => [
		'name' => 'SoFa Angebote',
		'singular_name' => 'Angebot',
	],
	'description' => 'Liste von SoFa Angeboten.',
	'public' => true,
    'hierarchical' => false,
	'has_archive' => true,
	'menu_icon'	=> 'dashicons-universal-access-alt',
	'menu_position' => 20,
	'supports' => ['title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes'],
    'taxonomies' => ['category'],

    // IMPORTANT: If single page isn't shown e.g. at "/nnsofa/title" , 
    // go to "settings -> permalinks" and switch/save between settings to rebuild parsing
	'rewrite' => ['slug' => 'nnsofa'],
    'show_in_rest' => true,
];

$eventForm = [
	'ctrl' => [
		'title'	=> 'Datum/Uhrzeit',
		'position' => 'left,top',
	],
	'columns' => [
		'time' => [
            'label' => 'Wann findet das Event statt?',
            'config' => [
                'type' => 'text',
                'rows' => '2',
                'enableRichtext' => true,
            ],
        ],
	],
];

$priceForm = [
	'ctrl' => [
		'title'	=> 'Preis',
		'position' => 'left,top',
	],
	'columns' => [
        'free' => [
            'label' => 'Die Veranstaltung ist kostenlos',
            'config' => [
                'type' => 'check',
            ],
        ],
        'price' => [
            'label' => 'Preise / Gebühren',
            'config' => [
                'type' => 'text',
                'rows' => '2',
                'enableRichtext' => true,
            ],
        ],
	],
];

$locationForm = [
	'ctrl' => [
		'title'	=> 'Veranstaltungs-Infos',
		'position' => 'right,top',
	],
	'columns' => [
        'event_location' => [
            'label' => 'Veranstaltungs-Ort (Bezeichnung)',
            'config' => [
                'type' => 'input',
            ],
        ],
        'event_street' => [
            'label' => 'Strasse',
            'config' => [
                'type' => 'input',
            ],
        ],
        'event_zip' => [
            'label' => 'PLZ',
            'config' => [
                'type' => 'input',
            ],
        ],
        'event_city' => [
            'label' => 'Ort',
            'config' => [
                'type' => 'input',
            ],
        ],
        'event_www' => [
            'label' => 'Webseite',
            'config' => [
                'type' => 'input',
                'placeholder' => 'https://...',
            ],
        ],
	],
];

$origanizerForm = [
	'ctrl' => [
		'title'	=> 'Anbieter-Infos',
		'position' => 'right,top',
	],
	'columns' => [
        'organizer_person' => [
            'label' => 'Leitung',
            'config' => [
                'type' => 'input',
            ],
        ],
        'organizer_street' => [
            'label' => 'Strasse',
            'config' => [
                'type' => 'input',
            ],
        ],
        'organizer_zip' => [
            'label' => 'PLZ',
            'config' => [
                'type' => 'input',
            ],
        ],
        'organizer_city' => [
            'label' => 'Ort',
            'config' => [
                'type' => 'input',
            ],
        ],
        'organizer_phone' => [
            'label' => 'Telefon',
            'config' => [
                'type' => 'input',
            ],
        ],
        'organizer_email' => [
            'label' => 'Email',
            'config' => [
                'type' => 'input',
            ],
        ],
	],
];

return [
    'config' => $cptConfig,
	'identifier' => '',
	'forms' => [
		'event' => $eventForm,
		'price' => $priceForm,
		'location' => $locationForm,
		'organizer' => $origanizerForm,
	]
];
