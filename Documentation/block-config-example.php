<?php

$form = [
	'ctrl' => [
		'title'	=> 'Adresse zeigen',
		'position' => 'left,top',
	],
	'columns' => [
		'title' => [
			'label' => 'Überschrift',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			],
		],
		'text' => [
			'label' => 'Text',
			'config' => [
				'type' => 'text',
                'rows' => '2',
                'enableRichtext' => true,
			],
		],
		'gender' => [
			'label' => 'Auswahl',
			'config' => [
				'type' => 'select',
				'items' => [
					['Herr', 'm'],
					['Frau', 'f'],
					['Divers', 'd'],
				]
			],
		],
		'height' => [
			'label' => 'Höhe (px)',
			'config' => [
				'type' => 'slider',
				'responsive' => true,
				'units' => [''],
				'min' => 0,
				'max' => 1000,
			],
		],
		'align' => [
			'label' => 'Ausrichtung',
			'config' => [
				'type' => 'select',
				'items' => \Nng\Nnhelpers\Utilities\Form::ALIGNMENTS
			],
		],
		'tag' => [
			'label' => 'Tag',
			'config' => [
				'type' => 'select',
				'items' => \Nng\Nnhelpers\Utilities\Form::TAGS,
				'default' => 'p',
			],
		],
		'categories' => [
			'label' => 'Kategorien',
			'config' => [
				'type' => 'category',
                'minitems' => 0,
                'maxitems' => 99,
			],
		],
		'check' => [
			'label' => 'Eine Checkbox',
			'config' => [
				'type' => 'check',
			],
		],
		'farbe' => [
			'label' => 'Eine Farbe',
			'config' => [
				'type' => 'colorpicker',
			],
		],
		'typo' => [
			'label' => 'Die Schrift',
			'config' => [
				'type' => 'typography',
			],
		],
		'icon' => [
			'label' => 'Das Icon',
			'config' => [
				'type' => 'icon',
			],
		],
	],
];


return [
	'identifier' => '',		// defaults to {$extKey}_{filename}
	'template' => '', 		// defaults to `EXT:{$extKey}/Resources/Templates/Blocks/{Filename}.html`
 	'forms' => [
		'address' => $form,
	]
];