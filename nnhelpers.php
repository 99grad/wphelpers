<?php
/*
	Plugin Name: WordPress Helpers (by 99°)
	Plugin URI: https://www.99grad.de
	description: Why write two lines of code, if one is enough?
	Version: 1.0
	Author: 99grad.de
	Author URI: https://www.99grad.de
	License: GPL2
*/

/*
	Using nnhelpers in your own plugin:

	Add this line in your plugin initialization function before using any methods from nnhelpers:
	
	```
	require_once( plugin_dir_path(__DIR__) . 'nnhelpers/autoload.php');
	```	
*/

require_once( dirname(__FILE__) . '/autoload.php' );
