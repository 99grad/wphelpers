# 99° WordPress Helpers (nnhelpers)

This is adaption of [nnhelpers for TYPO3](https://bitbucket.org/99grad/nnhelpers/src/master/)

The extension “nnhelpers” offers a collection of extremely simplified tools and methods to significantly speed up the development and update of WordPress extensions.

### Installation

- Download the `nnhelpers` Plugin from [https://bitbucket.org](https://bitbucket.org/99grad/wphelpers/downloads/)
- Unzip the downloaded folder
- __Important__: Rename the folder from `99grad-wphelpers-xxxxx` to `nnhelpers`
- Upload the folder `nnhelpers` to the folder `wp-content/plugins` of your WordPress installation
- Activate the Plugin in the WordPress backend

### Documentation

You can find the documentation here:
[http://wphelpers.99grad.de/](http://wphelpers.99grad.de/)