<?php

require_once( dirname(__FILE__) . '/Classes/Singleton.php' );
require_once( dirname(__FILE__) . '/Classes/Bootstrap.php' );

$bootstrap = \nn\bootstrap::makeInstance();
$bootstrap->register( dirname(__FILE__) . '/nnhelpers.php' );
