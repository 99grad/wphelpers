<?php

namespace nn\wp;

/**
 * Collection of static methods
 * 
 */
class GeneralUtility {

	/**
	 * ```
	 * // nn_helpers => NnHelpers
	 * \nn\wp\GeneralUtility::underscoredToUpperCamelCase('nn_helpers');
	 * ```
	 * @param string $string
	 * @return string
	 */
	public static function underscoredToUpperCamelCase($string)
	{
		return str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($string))));
	}

	/**
	 * ```
	 * // nn_helpers => nnHelpers
	 * \nn\wp\GeneralUtility::underscoredToLowerCamelCase('nn_helpers');
	 * ```
	 * @param string $string
	 * @return string
	 */
	public static function underscoredToLowerCamelCase($string)
	{
		return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($string)))));
	}

	/**
	 * ```
	 * // NnHelpers => nn_helpers
	 * \nn\wp\GeneralUtility::camelCaseToLowerCaseUnderscored('NnHelpers');
	 * ```
	 * @param string $string
	 * @return string
	 */
	public static function camelCaseToLowerCaseUnderscored($string)
	{
		$value = preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $string) ?? '';
		return mb_strtolower($value, 'utf-8');
	}

	/**
	 * Parse vendor name and extension name from namespace
	 * ```
	 * // ['vendor'=>'Nng', 'extName'=>'MyExt', 'extKey'=>'my_ext']
	 * \nn\wp\GeneralUtility::parseNamespace( '\Nng\MyExt' );
	 * ```
	 * @param string $string
	 * @return array
	 */
	public static function parseNamespace( $str = '' ) 
	{
		$str = ltrim( $str, '\\');
		
		list($vendor, $extName) = array_pad( explode('\\', $str), 2, '');
		$vendor = self::underscoredToUpperCamelCase($vendor);
		$extName = self::underscoredToUpperCamelCase($extName);

		return [
			'namespace' => trim("{$vendor}\\{$extName}", '\\'),
			'vendor'	=> $vendor,
			'extName' 	=> $extName,
			'extKey' 	=> self::camelCaseToLowerCaseUnderscored($extName),
		];
	}

	/**
	 * Encode a value for `javascript:...` in href of a link-tag
	 * 
	 * @param string $val
	 * @return string
	 */
	public static function quoteJSvalue( $value = '' ) 
	{
		return strtr(
			json_encode((string)$value, JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT|JSON_HEX_TAG),
			[
				'"' => '\'',
				'\\\\' => '\\u005C',
				' ' => '\\u0020',
				'!' => '\\u0021',
				'\\t' => '\\u0009',
				'\\n' => '\\u000A',
				'\\r' => '\\u000D'
			]
		);
	}

	public static function encryptEmail(string $string, $type = 99)
    {
        $out = '';
        // obfuscates using the decimal HTML entity references for each character
        if ($type === 'ascii') {
            foreach (preg_split('//u', $string, -1, PREG_SPLIT_NO_EMPTY) as $char) {
                $out .= '&#' . mb_ord($char) . ';';
            }
        } else {
            // like str_rot13() but with a variable offset and a wider character range
            $len = strlen($string);
            $offset = (int)$type;
            for ($i = 0; $i < $len; $i++) {
                $charValue = ord($string[$i]);
                // 0-9 . , - + / :
                if ($charValue >= 43 && $charValue <= 58) {
                    $out .= self::encryptCharcode($charValue, 43, 58, $offset);
                } elseif ($charValue >= 64 && $charValue <= 90) {
                    // A-Z @
                    $out .= self::encryptCharcode($charValue, 64, 90, $offset);
                } elseif ($charValue >= 97 && $charValue <= 122) {
                    // a-z
                    $out .= self::encryptCharcode($charValue, 97, 122, $offset);
                } else {
                    $out .= $string[$i];
                }
            }
        }
        return $out;
    }

	/**
     * Encryption (or decryption) of a single character.
     * Within the given range the character is shifted with the supplied offset.
     *
     * @param int $n Ordinal of input character
     * @param int $start Start of range
     * @param int $end End of range
     * @param int $offset Offset
     * @return string encoded/decoded version of character
     */
    public static function encryptCharcode($n, $start, $end, $offset)
    {
        $n = $n + $offset;
        if ($offset > 0 && $n > $end) {
            $n = $start + ($n - $end - 1);
        } elseif ($offset < 0 && $n < $start) {
            $n = $end - ($start - $n - 1);
        }
        return chr($n);
    }
}
