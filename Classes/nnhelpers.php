<?php

namespace nn;

use Nng\Nnhelpers\Utilities\Arrays;
use Nng\Nnhelpers\Utilities\Ajax;
use Nng\Nnhelpers\Utilities\Block;
use Nng\Nnhelpers\Utilities\Cache;
use Nng\Nnhelpers\Utilities\Convert;
use Nng\Nnhelpers\Utilities\CPT;
use Nng\Nnhelpers\Utilities\Db;
use Nng\Nnhelpers\Utilities\Elementor;
use Nng\Nnhelpers\Utilities\Environment;
use Nng\Nnhelpers\Utilities\Encrypt;
use Nng\Nnhelpers\Utilities\ExtensionManager;
use Nng\Nnhelpers\Utilities\Fal;
use Nng\Nnhelpers\Utilities\File;
use Nng\Nnhelpers\Utilities\Form;
use Nng\Nnhelpers\Utilities\Geo;
use Nng\Nnhelpers\Utilities\Http;
use Nng\Nnhelpers\Utilities\Page;
use Nng\Nnhelpers\Utilities\Post;
use Nng\Nnhelpers\Utilities\Registry;
use Nng\Nnhelpers\Utilities\Request;
use Nng\Nnhelpers\Utilities\Message;
use Nng\Nnhelpers\Utilities\SysCategory;
use Nng\Nnhelpers\Utilities\Settings;
use Nng\Nnhelpers\Utilities\Template;
use Nng\Nnhelpers\Utilities\Errors;

class wp {

	/**
	 * @return Arrays
	 */
	public static function Arrays( $arr = null ) {
		return new Arrays( $arr );
	}

	/**
	 * @return Ajax
	 */
	public static function Ajax() {
		return self::injectClass(Ajax::class);
	}
	
	/**
	 * @return Block
	 */
	public static function Block() {
		return self::injectClass(Block::class);
	}

	/**
	 * @return Cache
	 */
	public static function Cache() {
		return self::injectClass(Cache::class);
	}

	/**
	 * @return Convert
	 */
	public static function Convert( $config = null ) {
		return new Convert( $config );
	}

	/**
	 * @return CPT
	 */
	public static function CPT() {
		return self::injectClass( CPT::class );
	}
	
	/**
	 * @return Db
	 */
	public static function Db() {
		return self::injectClass(Db::class);
	}

	/**
	 * @return Elementor
	 */
	public static function Elementor() {
		return self::injectClass(Elementor::class);
	}

	/**
	 * @return Environment
	 */
	public static function Environment() {
		return self::injectClass(Environment::class);
	}

	/**
	 * @return Encrypt
	 */
	public static function Encrypt() {
		return self::injectClass(Encrypt::class);
	}

	/**
	 * @return ExtensionManager
	 */
	public static function ExtensionManager() {
		return self::injectClass(ExtensionManager::class);
	}
	
	/**
	 * @return Fal
	 */
	public static function Fal() {
		return self::injectClass(Fal::class);
	}

	/**
	 * @return File
	 */
	public static function File() {
		return self::injectClass(File::class);
	}

	/**
	 * @return Form
	 */
	public static function Form() {
		return self::injectClass(Form::class);
	}

	/**
	 * @return Geo
	 */
	public static function Geo() {
		return self::injectClass(Geo::class);
	}

	/**
	 * @return Http
	 */
	public static function Http() {
		return self::injectClass(Http::class);
	}

	/**
	 * @return Page
	 */
	public static function Page() {
		return self::injectClass(Page::class);
	}

	/**
	 * @return Post
	 */
	public static function Post() {
		return self::injectClass(Post::class);
	}

	/**
	 * @return Registry
	 */
	public static function Registry() {
		return self::injectClass(Registry::class);
	}

	/**
	 * @return Request
	 */
	public static function Request() {
		return self::injectClass(Request::class);
	}

	/**
	 * @return Errors
	 */
	public static function Errors() {
		return self::injectClass(Errors::class);
	}

	/**
	 * @return Message
	 */
	public static function Message() {
		return self::injectClass(Message::class);
	}

	/**
	 * @return SysCategory
	 */
	public static function SysCategory() {
		return self::injectClass(SysCategory::class);
	}

	/**
	 * @return Settings
	 */
	public static function Settings() {
		return self::injectClass(Settings::class);
	}
	
	/**
	 * @return Template
	 */
	public static function Template() {
		return self::injectClass(Template::class);
	}
   
	// ---------------------------------------------------------------------------------
	//  Funktionen, die so oft verwendet werden, dass sie nicht in einer eigenen
	//  Utility-Class gekapselt werden.

	/**
	 *  Eine Klasse über new Class\Name instanziieren.
	 * 
	 *  $example = \nn\wp::newClass( \My\Example\ClassName::class );
	 * 
	 *  @return mixed  
	 */
	public static function newClass($class) {
		if (!class_exists($class)) return false;
		
		if (is_a($class, \Nng\Nnhelpers\Singleton::class, true)) {
			return call_user_func($class . '::makeInstance');
		}

		return new $class;
	}

	/**
	 * Schnellste Art, eine Klasse zu instanziieren.
	 * ```
	 * $example = \nn\wp::injectClass( \My\Example\ClassName::class );
	 * ```
	 * @return mixed  
	 */
	public static function injectClass($class) 
	{
		if (!class_exists($class)) return false;
		$class = ltrim( $class, '\\');

		if (is_a($class, \Nng\Nnhelpers\Singleton::class, true)) {
			return call_user_func($class . '::makeInstance');
		}

		return new $class();
	}
		
	/**
	 *  Die Version von WordPress holen
	 * 
	 *  $wordpressVersion = \nn\wp::wpVersion();
	 * 
	 *	@return float
	 */
	public static function wpVersion() {
		return floor(123);
	}
	
	/**
	 *  Wirft eine Exception.
	 *  Alias zu \nn\wp::Errors()->Exception( $message, $code );
	 *  ```
	 *  \nn\wp::Exception( 'Damn.' );
	 *  \nn\wp::Exception( 'Damn.', '4711' );
	 *  ```
	 *  @param string $text     Fehler-Meldung
	 *  @param string $code     Fehler-Code (Optional)
	 *	@return void
	 */
	public static function Exception( $message = '', $code = '' ) {
	   \nn\wp::Errors()->Exception( $message, $code );
	}
	
	/**
	 *  Wirft einen Fehler.
	 *  Alias zu \nn\wp::Errors()->Error( $message, $code );
	 *  ```
	 *  \nn\wp::Error( 'Damn.' );
	 *  \nn\wp::Error( 'Damn.', '4711' );
	 *  ```
	 *  @param string $text     Fehler-Meldung
	 *  @param string $code     Fehler-Code (Optional)
	 *	@return void
	 */
	public static function Error( $message = '', $code = '' ) {
	   \nn\wp::Errors()->Error( $message, $code );
	}
	
	/**
	 *  Der bessere Debugger für alles.
	 *  Gibt auch Zeilen-Nummer und Class mit an, die den Debug aufgerufen hat – 
	 *  dadurch leichter wiederzufinden.
	 *  Kann QueryBuilder-Statements ausgeben, um MySQL-Queries zu debuggen. 
	 *  ```
	 *  \nn\wp::debug('Hallo!');
	 *  \nn\wp::debug($queryBuilder);
	 *  \nn\wp::debug($query);
	 *  ```
	 *	@return float
	 */
	public static function debug( $obj = null, $title = null ) {

		// Ermittelt, wo der Aufruf von debug() stattgefunden hat
		$backtrace = debug_backtrace();
		$backtrace = array_shift( $backtrace );

		$filename = $backtrace['file'];
		$line = $backtrace['line'];
		$callerInfo = ($title ? $title . ' // ' : '') . $filename . ' Zeile ' . $line;

		\OmegaCode\DebuggerUtility::var_dump($obj, $callerInfo, 8);
	}


	/**
	 * Ruft eine Methode in einem Objekt auf.
	 * Parameter werden als Referenz übergeben, können in Methode modifiziert werden.
	 *
	 * ```
	 * $result = \nn\wp::call( 'My\Extension\ClassName->method' );
	 * $result = \nn\wp::call( 'My\Extension\ClassName->method', $param1, $params2, $params3 );
	 * ```
	 * @var string $funcStr 			=> z.B. \Nng\Nnsubscribe\Service\NotifcationService->do_someting
	 * @var array $params				=> Parameter, die an Funktion übergeben werden sollen
	 * @var array $params2				=> zweiter Parameter, der an Funktion übergeben werden sollen
	 * @var array $params3				=> dritter Parameter, der an Funktion übergeben werden sollen
	 *
	 * @return mixed
	 */
	public static function call ( $funcStr, &$params = [], &$params2 = null, &$params3 = null, &$params4 = null ) 
	{
		if (!trim($funcStr)) self::Exception("\\nn\\wp::call() - Keine Klasse angegeben.");
		list($class, $method) = explode( '->', $funcStr );
		
		if (!class_exists($class)) self::Exception("\\nn\\wp::call({$class}) - Klasse {$class} existiert nicht.");
		
		$classRef = self::injectClass($class);
		if (!method_exists($classRef, $method)) self::Exception("\\nn\\wp::call() - Methode {$class}->{$method}() existiert nicht.");

		// $allParams = [&$params, &$params2, &$params3, &$params4];
		// return $classRef->$method( ...$allParams );

		if ($params4 != null) return $classRef->$method($params, $params2, $params3, $params4);		
		if ($params3 != null) return $classRef->$method($params, $params2, $params3);		
		if ($params2 != null) return $classRef->$method($params, $params2);
		return $classRef->$method($params);
	}

}