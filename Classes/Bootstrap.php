<?php

namespace nn;

/**
 * Bootscript für nnhelpers.
 * 
 * Übernimmt: 
 * - Registrieren des autoloaders
 * - Laden der `\nn\wp`-Klasse, damit alle `\nn\wp::Something()` Klassen verfügbar sind
 * - Registrieren der zentralen AJAX-Funktionen
 * 
 */
class bootstrap extends \Nng\Nnhelpers\Singleton 
{
	/**
	 * Flag, if `nnhelpers` was already registered
	 * @var boolean
	 */
	protected $registered;

	/**
	 * Path to `EXT:nnhelpers/nnhelpers.php
	 * @var string
	 */
	protected $pluginFile;
	
	/**
	 * Path to `EXT:nnhelpers/Classes/`-folder
	 * @var string
	 */
	protected $classPath;

	/**
	 * Path to the `autoload.php` of composer
	 * @var string
	 */
	protected $composerAutoloadPath;

	/**
	 * Registers `nnhelpers` autoloader and loads the external libraries.
	 * This method is executed BEFORE any other actions.
	 * 
	 */
	public function register( $pluginFile = '' ) 
	{
		if ($this->registered) return;
		$this->registered = true;

		$this->pluginFile = $pluginFile;

		$this->classPath = dirname(__FILE__) . '/';
		$this->composerAutoloadPath = dirname(__DIR__) . '/Resources/Libraries/vendor/autoload.php';

		$this->registerHooks();
		$this->loadDependencies();

		require_once( $this->composerAutoloadPath );

		\nn\wp::ExtensionManager()->registerAutoloader(['\Nng\Nnhelpers' => 'Classes']);

		add_action( 'plugins_loaded', [$this, 'registerDependentPlugins'] );
		add_action( 'init', [$this, 'init']);
	}

	/**
	 * Initializes foreign plugins which depend on `nnhelpers`.
	 * This hook is called before the WP `init` hook and gives the foreign
	 * plugins a chance to register actions that will listen to the `init` action.
	 * 
	 * @return void
	 */
	public function registerDependentPlugins() 
	{
		// usually triggers `\nn\wp::ExtensionManager()->registerExtension();` in the external plugins
		do_action( 'nnhelpers_loaded' );

		// now that all plugins that depend on `nnhelpers` have registered, create instances
		\nn\wp::ExtensionManager()->beforeInitializeApps();
	}

	/**
	 * Initializes `nnhelpers` and loads the external libraries.
	 * This method is only executed once during action `init`
	 * 
	 * @return void
	 */
	public function init() 
	{
		\nn\wp::ExtensionManager()->addExtConfiguration( 'nnhelpers' );

		\nn\wp::Ajax()->registerAjaxHandler();
		\nn\wp::Ajax()->registerEndpoint( 'nnwp_ajax', 'Nng\Nnhelpers\Controller\AjaxController->handleAjaxRequest' );

		$this->initRegisteredExtensions();
	}

	/**
	 * Init all external Plugins that have registered via `\nn\wp::ExtensionManager()->registerExtension()`
	 * Called in the `init` action of WordPress.
	 * 
	 * @return void
	 */
	public function initRegisteredExtensions() 
	{
		$registeredExtensions = \nn\wp::ExtensionManager()->getRegisteredExtensions();

		foreach ($registeredExtensions as $extKey=>$config) {

			// Create instance of main entry point (e.g. `\Nng\Nnaddress\App`)
			if ($app = $config['instance'] ?? false) {
				$app->init();
				$app->run();
			}
		}
	}

	/**
	 * Registrieren der Hooks für dieses plugin
	 * 
	 * @return void
	 */
	public function registerHooks() 
	{
		// Hook beim Installieren: Prüft dependecies und legt Ordner an
		register_activation_hook( $this->pluginFile, [\Nng\Nnhelpers\Hooks\Install::class, 'registerActivationAction']);

		// Annotation `@depends` in Plugin-Konfiguration ebenfalls parsen (Filter-Hook in https://developer.wordpress.org/reference/functions/get_file_data/)
		add_filter('extra_plugin_headers', function () { 
			return ['@depends']; 
		});
	}
	
	/**
	 * Laden von Klassen und Funktionen für dieses plugin
	 * 
	 * @return void
	 */
	public function loadDependencies() 
	{
		if (!function_exists('get_plugin_data')) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		// Helpers, die zum Booten gebraucht werden, um spl_autoload_register() zu nutzen
		require_once( $this->classPath . 'Utilities/Environment.php' );
		require_once( $this->classPath . 'Utilities/ExtensionManager.php' );
		require_once( $this->classPath . 'Utilities/File.php' );
		require_once( $this->classPath . 'Utilities/Registry.php' );
		require_once( $this->classPath . 'GeneralUtility.php' );
		require_once( $this->classPath . 'nnhelpers.php' );		
	}
}