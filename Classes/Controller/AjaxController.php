<?php

namespace Nng\Nnhelpers\Controller;

/**
 * 
 * ```
 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn&type=nnwp_ajax
 * ```
 * 
 */
class AjaxController extends AbstractAjaxController
{
	/**
	 * Zentraler Einstiegspunkt für alle Requests von den nnhelpers Formularelementen:
	 * ```
	 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn&type=nnwp_ajax
	 * ```
	 * @return array
	 */
	public function handleAjaxRequest( $args = [] ) 
	{
		if (!$this->verifyNonce()) {
			$this->unauthorized('\Nng\Nnhelpers\Controller\AjaxController->handleAjaxRequest(): nonce invalid.');
		}
		$method = $args['api'] . 'Action';

		if (!method_exists($this, $method)) {
			$this->notFound("\Nng\Nnhelpers\Controller\AjaxController->{$method}() not defined.");
		}
		return $this->$method( $args );
	}

	/**
	 * Relationen zu einen gegebenen Keyword zurückgeben.
	 * 
	 * Beispiel-Parameter aus JS:
	 * ```
	 * // https://sofa.99grad.dev/wp-admin/admin-ajax.php?action=nn
	 * var gSettings = window.nnhelpers;
	 * var config = { type:'relation', allowed_post_types:['nnaddress'] };
	 * var data = {_nonce: gSettings._nonce, type: 'nnwp_ajax', api: 'relation', sword: sword, config: config }
	 * $.post(gSettings.ajax_url, data).done(function(result) {
	 * 	console.log( result );
	 * });
	 * ```
	 * 
	 * @param array $args
	 * @return array
	 */
	public function relationAction( $args = [] ) 
	{
		$config = $args['config'] ?? [];
		if (is_string($config)) {
			$config = stripslashes($config ?? []);
			$config = json_decode($config, true); 
		}
		if (!$config) return [];

		$results = \nn\wp::Db()->findLike('posts', [
			'post_title'	=> '%' . $args['sword'] . '%', 
			'post_type'		=> $config['allowed_post_types'],
		], true);

		$finals = [];
		foreach ($results as $result) {
			$finals[] = [
				'type' 		=> $result['post_type'],
				'title' 	=> $result['post_title'],
				'ID' 		=> $result['ID'],
			];
		}

		return ['results'=>$finals];
	}

	public function testAction( $args = [] ) 
	{
		return ['fine'=>'visitor'];
	}

}
