<?php

namespace Nng\Nnhelpers\Controller;

class ElementorController extends AbstractElementorController 
{
	/**
	 * Called after the class was initialized and before the view
	 * is rendered. Perfect for adding own variables to the view using
	 * `$this->view->assignMultiple()`
	 * 
	 * @return void
	 */
	public function initializeView( $view = null ) {}
}