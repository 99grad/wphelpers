<?php

namespace Nng\Nnhelpers\Controller;

/**
 * Abstraktions-Klasse für häufig genutzte AJAX Methoden.
 * Der eigene AJAX-Endpoint sollte diesen extended.
 * 
 * ```
 * use \Nng\Nnhelpers\Controller\AbstractAjaxController;
 * 
 * class My\Extension\Controller\AjaxController extends AbstractAjaxController 
 * {
 *  ...
 * }
 * ```
 * 
 */
abstract class AbstractAjaxController 
{
    /**
     * @var boolean
     */
    var $nonceValid = false;

    public function setNonce( $nonce = '', $check = '' ) 
    {
        $this->nonceValid = \nn\wp::Encrypt()->verifyNonce($nonce, $check);
    }

    public function verifyNonce() 
    {
        return $this->nonceValid;
    }

    public function unauthorized( $message = '' ) 
    {
       $this->sendResponse( 403, $message ?: 'Unauthorized' );
    }

    public function notFound( $message = '' ) 
    {
       $this->sendResponse( 404, $message ?: 'Unauthorized' );
    }

    public function sendResponse( $code, $message ) 
    {
        header('HTTP/1.0 ' . $code);
        wp_send_json(['error'=>$message]);
        die();
    }
}
