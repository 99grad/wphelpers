<?php

namespace Nng\Nnhelpers\Controller;

/**
 * 
 * ```
 * use \Nng\Nnhelpers\Controller\AbstractCptController;
 * 
 * class My\Extension\Controller\CPT\MycptnameController extends AbstractCptController 
 * {
 *  ...
 * }
 * ```
 * 
 */
abstract class AbstractCptController extends \Nng\Nnhelpers\View\FluidBasedView
{
	/**
	 * @var string
	 */
	var $extKey = '';

	/**
	 * @var string
	 */
	var $postType = '';

	/**
	 * @var array
	 */
	var $configuration = [
		'identifier' => '',
		'controller' => '',
		'config' => [],
		'forms' => [],
		'plugin' => [],
	];

	/**
	 * 
	 */
	public function configure( $config = [] ) 
	{
		$this->configuration = $config;
		$this->postType = $config['identifier'];
		$this->extKey = $config['plugin']['extKey'];

		add_action( 'init', [$this, 'init'] );
		add_action( 'admin_init', [$this, 'addMetaFields'] );
		add_action( 'save_post', [$this, 'saveMetaFields'], 10, 2 );

		$templatePaths = $this->getTemplatePaths();
		$additionalTemplatePaths = $this->getAdditionalTemplatePaths();

		$this->view->setTemplateRootPaths(array_merge(
			$templatePaths['templateRootPaths'] ?? [],
			$additionalTemplatePaths['templateRootPaths'] ?? [],
		));
		$this->view->setPartialRootPaths(array_merge(
			$templatePaths['partialRootPaths'] ?? [],
			$additionalTemplatePaths['partialRootPaths'] ?? [],
		));
	}

	/**
	 * @return array
	 */
	public function getTemplatePaths() 
	{
		return [
			'templateRootPaths' => [
				'EXT:nnhelpers/Resources/Templates/',
				"EXT:{$this->extKey}/Resources/Templates/",
			],
			'partialRootPaths' => [
				'EXT:nnhelpers/Resources/Partials/',
				"EXT:{$this->extKey}/Resources/Partials/"
			],
		];
	}

	/**
	 * @return array
	 */
	public function getAdditionalTemplatePaths() 
	{
		return [
			'templateRootPaths' => [],
			'partialRootPaths' => [],
		];
	}


	/**
	 * @return self
	 */
	public function init() 
	{
		$this->addPostType();
		$this->initializeView( $this->view );
	}

	/**
	 * Register the new post-type.
	 * Called during `do_action('init')`
	 * 
	 * @return self
	 */
	public function addPostType() 
	{
		// Abort, if this CPT only extends the Meta-Fields of an existing Post-Type
		$config = $this->configuration['config'] ?? false;
		$extends = $this->configuration['extends'] ?? false;
		if (!$config || $extends) return;

		register_post_type( 
			$this->postType, 
			$config 
		);
		return $this;
	}


	/**
	 * 
 	 * @return self
	 */
	public function addMetaFields() 
	{
		$forms = $this->configuration['forms'] ?? [];

		$addToPostTypes = $this->configuration['extends'] ?? [$this->postType];

		foreach ($addToPostTypes as $postType) {

			foreach ($forms as $name=>$form) {

				$formIdentifier = $this->extKey . '_' . $name;
				$ctrl = $form['ctrl'];
				$title = $ctrl['title'] ?? $formIdentifier;
				
				$position = $ctrl['position'] ?: 'right,top';
				if (is_string($position)) {
					$tmp = [];
					if (strpos($position, 'right') !== false) 	$tmp['context'] = 'side';
					if (strpos($position, 'left') !== false) 	$tmp['context'] = 'advanced';
					if (strpos($position, 'top') !== false) 	$tmp['priority'] = 'high';
					if (strpos($position, 'bottom') !== false) 	$tmp['priority'] = 'low';
					$position = [$tmp['context'], $tmp['priority']];
				}
		
				$params = [
					'id' 		=> $formIdentifier,
					'title'		=> $title,
					'callback'	=> [$this, 'render'],
					'screen'	=> $postType,
					'context'	=> $position[0],
					'priority'	=> $position[1],
					'args'		=> $form
				];
	
				add_meta_box( ...array_values($params) );
			}
	
		}

		return $this;
	}

	/**
	 * Rendert das Formular und gibt es per `echo()` aus.
	 * 
	 * @return void
	 */
	public function render ($post = null, $callbackArgs = []) 
	{
		$form = $this->renderForm($post, $callbackArgs );
		echo $form;
	}

	/**
	 * Rendert das Formular
	 * 
	 * @return void
	 */
	public function renderForm ( $post = null, $callbackArgs = [] ) 
	{
		\nn\wp::Form()->addFormJsCss();

		$formIdentifier = $callbackArgs['id'];
		$formConfig = $callbackArgs['args'];
		$postMeta = get_post_meta( $post->ID );

		$postMetaWithoutPrefix = [];
		foreach ($postMeta as $k=>$v) {
			$k = preg_replace('/_' . $this->extKey .'_(.*)/', '\1', $k );
			$postMetaWithoutPrefix[$k] = array_shift($v);
		}

		$template = $formConfig['template'] ?? 'Form/CustomPostTypeFields';

		$this->view->assignMultiple([
			'nonce'				=> \nn\wp::Encrypt()->createNonce( __CLASS__ . $formIdentifier ),
			'formNamePrefix'	=> $formIdentifier,
			'gp'				=> $postMetaWithoutPrefix,
			'config' 			=> $formConfig,
		]);

		return $this->view->render( $template );
	}

	/**
	 * Wird nach dem Speichern aufgerufen,
	 * falls `\nn\wp::CPT()->addForm()` genutzt wurde.
	 * 
	 * Speichert die Meta-Daten für den Beitrag/Post/CPT in der Tabelle `post_meta`
	 * 
	 * @param integer $post_id
	 * @param object $post
	 * @return void
	 */
	public function saveMetaFields( $post_id, $post ) 
	{
		$forms = $this->configuration['forms'] ?? [];

		foreach ($forms as $name=>$form) {
			$formIdentifier = "{$this->extKey}_{$name}";
			$data = \nn\wp::Request()->GP( $formIdentifier );
			if (!$data) continue;

			if (!\nn\wp::Encrypt()->verifyNonce($data['nonce'] ?? '', __CLASS__ . $formIdentifier )) {
				die( __( 'Nonce security check failed. ' . get_class( $this ) . '->saveMetaFields()' ) );
			}
			unset($data['nonce']);
			foreach ($data as $key=>$meta_value) {
				$meta_key = "{$this->extKey}_{$key}";
				update_post_meta( $post_id, '_'.$meta_key, $meta_value );
			}
		}
	}

	/**
	 * This method can be overriden in the child-class.
	 * 
	 * It is called after the class was initialized and before the view
	 * is rendered. Perfect for adding own variables to the view using
	 * `$this->view->assignMultiple()`
	 * 
	 * @return void
	 */
	public function initializeView( $view = null ) {}

}
