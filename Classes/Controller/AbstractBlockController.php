<?php

namespace Nng\Nnhelpers\Controller;

use \nn\wp\GeneralUtility;

/**
 * 
 * ```
 * use \Nng\Nnhelpers\Controller\AbstractBlockController;
 * 
 * class My\Extension\Controller\Blocks\MyblockController extends AbstractBlockController 
 * {
 *  ...
 * }
 * ```
 * 
 */
abstract class AbstractBlockController extends \Nng\Nnhelpers\View\FluidBasedView
{
	/**
	 * @var string
	 */
	var $filename = '';

	/**
	 * @var string
	 */
	var $extKey = '';

	/**
	 * @var string
	 */
	var $extName = '';

	/**
	 * @var string
	 */
	var $vendor = '';

	/**
	 * @var string
	 */
	var $blockType = '';

	/**
	 * @var string
	 */
	var $modelClassName = 'Nng\Nnhelpers\Domain\Model\CPT\DefaultModel';

	/**
	 * @var \Nng\Nnhelpers\Domain\Model\CPT\AbstractModel
	 */
	var $model;

	/**
	 * @var array
	 */
	var $post = [];

	/**
	 * @var int
	 */
	var $postID = 0;
	
	/**
	 * @var array
	 */
	var $gp = [];

	/**
	 * @var array
	 */
	var $atts = [];

	/**
	 * @var string
	 */
	var $content = [];

	/**
	 * @var array
	 */
	var $configuration = [
		'identifier' => '',
		'template' => '',
		'controller' => '',
		'config' => [],
		'forms' => [],
		'plugin' => [],
	];

	/**
	 * 
	 */
	public function configure( $config = [] ) 
	{
		$this->configuration = $config;
		$this->blockType = $config['identifier'];
		$this->filename = $config['filename'];
		$this->extKey = $config['plugin']['extKey'];
		$this->extName = $config['plugin']['extName'];
		$this->vendor = $config['plugin']['vendor'];

		$modelClassName = "{$this->vendor}\\{$this->extName}\\Domain\Model\CPT\Post";
		if (class_exists( $modelClassName )) {
			$this->modelClassName = $modelClassName;
		}

		add_action( 'init', [$this, 'init'] );
		add_shortcode( $this->blockType, [$this, 'render'] );

		$this->view->setTemplateRootPaths([
			"EXT:{$this->extKey}/Resources/Templates/",
		]);
		$this->view->setPartialRootPaths([
			"EXT:{$this->extKey}/Resources/Partials/"
		]);
	}

	/**
	 * @return self
	 */
	public function init() {}
	
	/**
	 * @return self
	 */
	public function render( $atts = [], $content = '' ) 
	{
		$template = $this->configuration['template'];

		$postID = get_the_ID();
		$post = \nn\wp::Post()->getWithMeta( $postID );

		$this->model = \nn\wp::Convert( $post )->toModel( $this->modelClassName );

		$vars = [
			'gp'		=> \nn\wp::Request()->GP(),
			'atts' 		=> $atts ?: [],
			'content'	=> $content,
			'postID'	=> $postID,
			'post'		=> $post,
			'data'		=> $this->model,
		];

		foreach ($vars as $k=>$v) {
			$this->$k = $v;
		}
		
		$this->initializeView( $this->view );

		foreach ($vars as $k=>$v) {
			$this->view->assignMultiple([$k=>$this->$k]);
		}
		
		return $this->view->render( $template );
	}

	/**
	 * This method can be overriden in the child-class.
	 * 
	 * It is called after the class was initialized and before the view
	 * is rendered. Perfect for adding own variables to the view using
	 * `$this->view->assignMultiple()`
	 * 
	 * @return void
	 */
	public function initializeView( $view = null ) {}

}
