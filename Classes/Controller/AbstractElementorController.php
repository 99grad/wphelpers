<?php

namespace Nng\Nnhelpers\Controller;

use \nn\wp\GeneralUtility;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;

/**
 * 
 * ```
 * use \Nng\Nnhelpers\Controller\AbstractElementorController;
 * 
 * class My\Extension\Controller\Elementor\MythingController extends AbstractElementorController 
 * {
 *  ...
 * }
 * ```
 * 
 */
abstract class AbstractElementorController extends Widget_Base
{
	/**
	 * @var string
	 */
	public $title = '';

	/**
	 * @var string
	 */
	public $icon = 'fa fa-heading';

	/**
	 * @var array
	 */
	public $categories = ['basic'];

	/**
	 * @var array
	 */
	public $assets = ['js'=>[], 'css'=>[]];

	/**
	 * @var array
	 */
	public $queuedAssets = ['js'=>[], 'css'=>[]];

	/**
     * var \Nng\Nnhelpers\Utilities\Template
     */
    public $view;

	/**
	 * @var string
	 */
	var $slug = 'nnhelpers';

	/**
	 * @var string
	 */
	var $filename = '';

	/**
	 * @var string
	 */
	var $extKey = '';

	/**
	 * @var string
	 */
	var $extName = '';

	/**
	 * @var string
	 */
	var $vendor = '';

	/**
	 * @var string
	 */
	var $blockType = '';

	/**
	 * @var string
	 */
	var $modelClassName = 'Nng\Nnhelpers\Domain\Model\CPT\DefaultModel';

	/**
	 * @var \Nng\Nnhelpers\Domain\Model\CPT\AbstractModel
	 */
	var $model;

	/**
	 * @var array
	 */
	var $post = [];

	/**
	 * @var int
	 */
	var $postID = 0;
	
	/**
	 * @var array
	 */
	var $gp = [];

	/**
	 * @var array
	 */
	var $atts = [];

	/**
	 * @var string
	 */
	var $content = [];

	/**
	 * @var array
	 */
	var $configuration = [
		'identifier' => '',
		'template' => '',
		'controller' => '',
		'config' => [],
		'forms' => [],
		'plugin' => [],
	];

	/**
	 * 
	 */
	public function __construct( array $data = [], array $args = null ) {
		parent::__construct( $data, $args );
		$this->configure( \nn\wp::Elementor()->getConfiguration( $this ) );
	}

	/**
	 * 
	 */
	public function configure( $config = [] ) 
	{
		$this->configuration = $config;
		$this->filename = $config['filename'];
		$this->blockType = $config['identifier'];
		$this->slug = $config['identifier'];
		$this->cssSelectorPrefix = str_replace('_', '-', $config['identifier']) . '-';
		$this->extKey = $config['plugin']['extKey'];
		$this->extName = $config['plugin']['extName'];
		$this->vendor = $config['plugin']['vendor'];

		$modelClassName = "{$this->vendor}\\{$this->extName}\\Domain\Model\CPT\Post";
		if (class_exists( $modelClassName )) {
			$this->modelClassName = $modelClassName;
		}

		$this->view = \nn\wp::Template();

		$this->view->setTemplateRootPaths([
			"EXT:nnhelpers/Resources/Templates/",
			"EXT:{$this->extKey}/Resources/Templates/",
		]);
		$this->view->setPartialRootPaths([
			"EXT:nnhelpers/Resources/Partials/",
			"EXT:{$this->extKey}/Resources/Partials/",
		]);

		$this->addAssets();
	}
	
	/**
	 * Add JS / CSS to body
	 * 
	 * @return void
	 */
	public function addAssets() 
	{
		if ($js = $this->assets['js'] ?? []) {
			//$ids = \nn\wp::Page()->addJsLibrary( $js, ['elementor-frontend'], true );
			$ids = \nn\wp::Page()->addJsLibrary( $js );
			$this->queuedAssets['js'] = $ids;
		}
		if ($css = $this->assets['css'] ?? []) {
			$ids = \nn\wp::Page()->addCssLibrary( $css );
			$this->queuedAssets['css'] = $ids;
		}
	}

	/**
	 * 
	 */
	public function get_script_depends() {
		return [];
		return $this->queuedAssets['js'];
	}

	/**
	 * @return void
	 */
	public function render() 
	{
		$settings = $this->get_settings_for_display();

		$template = $this->configuration['template'];

		$postID = get_the_ID();
		$post = \nn\wp::Post()->getWithMeta( $postID );

		// $this->add_render_attribute( 'title', 'class', 'elementor-heading-title nn-n2l-' . $this->slug );
		// $this->add_render_attribute( 'title', 'data-media', esc_attr(json_encode($media)) );
		// $this->add_render_attribute( 'title', 'data-scale', ($settings['scale']['size'] ?? 100)/100 );

		$atts = $this->preRender( $settings );

		//$title_html = sprintf( '<%1$s %2$s>%3$s%4$s</%1$s>', $settings['tag'], $this->get_render_attribute_string( 'title' ), $icon, $title );

		$model = \nn\wp::Convert( $post )->toModel( $this->modelClassName );

		$vars = [
			'gp'		=> \nn\wp::Request()->GP(),
			'atts' 		=> $atts ?: [],
			'settings' 	=> $settings ?: [],
			'content'	=> $this->get_children(),
			'postID'	=> $postID,
			'post'		=> $post,
			'data'		=> $model,
		];

		foreach ($vars as $k=>$v) {
			$this->$k = $v;
		}
		
		$this->initializeView( $this->view );

		foreach ($vars as $k=>$v) {
			$this->view->assignMultiple([$k=>$this->$k]);
		}

		$html = $this->view->render( $template );

		// add special class to elementor-wrapper to indicate the element has no content
		if (trim(strip_tags($html)) == '') {
			$this->add_render_attribute( '_wrapper', 'class', 'nnhelpers-empty-elementor' );
		}

		echo $html;
	}

	/**
	 * PreRender all values.
	 * Called during the `render()` process before `initializeView()`
	 * 
	 * Example: 
	 * The `Icon`-control returns an array with ['library'=>'solid', 'value'=>'fas fa-star']
	 * To make rendering easier, this array is converted to `<i class="fas fa-star"></i>`
	 * and the set as viewVariable.
	 * 
	 * @param array $settings
	 * @return array
	 */
	public function preRender( $settings = [] ) 
	{
		$atts = [];
		$forms = $this->configuration['forms'] ?? [];
		foreach ($forms as $name=>$form) {
			$cols = $form['columns'];
			foreach ($cols as $key=>$formConfig) {
				$ucKey = GeneralUtility::underscoredToLowerCamelCase( $key );
				$atts[$ucKey] = $settings[$key] ?? '';
				$type = $formConfig['config']['type'] ?? false;
				if (!$type) continue;
				$method = 'preRender_' . $type;
				if (method_exists($this, $method)) {
					$this->$method( $atts, $key, $formConfig, $settings );
				}
			}
		}
		return $atts;
	}

	/**
	 * Preprocess `'type' => 'icon'`:
	 * Convert `['library'=>'solid', 'value'=>'fas fa-star']` to `<i class="fas fa-star"></i>`
	 * 
	 * @param array $atts
	 * @param string $key
	 * @param array $formConfig
	 * @param array $settings
	 * @return string
	 */
	public function preRender_icon ( &$atts = [], $key = '', $formConfig = [], $settings = [] ) 
	{
		$ucKey = ucfirst(GeneralUtility::underscoredToLowerCamelCase( $key ));
		$icon = $settings[$key] ?? [];
		if ( 'svg' === $icon['library'] ) {
			$html = \Elementor\Icons_Manager::render_uploaded_svg_icon( $icon['value'] );
		} else {
			$html = \Elementor\Icons_Manager::render_font_icon( $icon, ['aria-hidden'=>true], 'i' );
		}
		$atts['processed' . $ucKey] = $html;
	}

	/**
	 * Preprocess `'type' => 'slider'`:
	 * 
	 * @param string $key
	 * @param array $formConfig
	 * @param array $settings
	 * @return string
	 */
	public function preRender_slider ( &$atts = [], $key = '', $formConfig = [], $settings = [] ) 
	{
		$ucKey = ucfirst(GeneralUtility::underscoredToLowerCamelCase( $key ));
		$atts['cssSelector' . $ucKey] = $this->cssSelectorPrefix . $key;
	}
	
	/**
	 * Preprocess `'type' => 'colorpicker'`:
	 * 
	 * @param string $key
	 * @param array $formConfig
	 * @param array $settings
	 * @return string
	 */
	public function preRender_colorpicker ( &$atts = [], $key = '', $formConfig = [], $settings = [] ) 
	{
		$this->preRender_typography( $atts, $key, $formConfig, $settings );
	}

	/**
	 * Preprocess `'type' => 'typography'`:
	 * 
	 * @param string $key
	 * @param array $formConfig
	 * @param array $settings
	 * @return string
	 */
	public function preRender_typography ( &$atts = [], $key = '', $formConfig = [], $settings = [] ) 
	{
		$ucKey = ucfirst(GeneralUtility::underscoredToLowerCamelCase( $key ));

		// add a string to the attribute `class`. Use the identifier `$key` to find it later...
		$this->add_render_attribute( $key, 'class', $this->cssSelectorPrefix . $key );

		// render all attributes for the given key, e.g. a string like `class="my-class" data-something="..."`
		$atts['tagAttribute' . $ucKey] = $this->get_render_attribute_string( $key );

		// e.g. `nnaddress-show-title`
		$atts['cssSelector' . $ucKey] = $this->cssSelectorPrefix . $key;
	}

	/**
	 * Preprocess `'type' => 'relation'`:
	 * 
	 * @param string $key
	 * @param array $formConfig
	 * @param array $settings
	 * @return string
	 */
	public function preRender_relation( &$atts = [], $key = '', $formConfig = [], $settings = [] ) 
	{
		$ucKey = GeneralUtility::underscoredToLowerCamelCase( $key );
		$relations = \nn\wp::Post()->get($atts[$key], true);
		foreach ($relations as &$relation) {
			$relation = \nn\wp::Convert( $relation )->toModel();
		}
		$atts[$ucKey] = $relations;
	}


	/**
	 * This method can be overriden in the child-class.
	 * 
	 * It is called after the class was initialized and before the view
	 * is rendered. Perfect for adding own variables to the view using
	 * `$this->view->assignMultiple()`
	 * 
	 * @return void
	 */
	public function initializeView( $view = null ) {}


	public function get_name() 
	{
		//\nn\wp::debug($this->configuration['forms']); die();
		return $this->blockType;
	}

	public function get_title() 
	{
		return $this->title ?: $this->extName . '->' . $this->filename;
	}

	public function get_icon() 
	{
		return $this->icon;
	}

	public function get_categories() 
	{
		return $this->categories;
	}

	protected function register_controls() 
	{
		$forms = $this->configuration['forms'] ?? [];

		foreach ($forms as $formName=>$form) {
			$ctrl = $form['ctrl'] ?? [];
			$columns = $form['columns'] ?? [];

			$this->start_controls_section(
				'section_' . $formName,
				[
					'label' => __( $ctrl['title'], $this->slug ),
					'tab' => Controls_Manager::TAB_CONTENT,
				]
			);

			foreach ($columns as $key=>$element) {

				$type =  $element['config']['type'];
				$method = 'addControl_' . $type;
				
				if (!method_exists($this, $method)) {
					continue;
				}

				$this->$method( $key, $element );
			}

			$this->end_controls_section();
		}
	}


	/**
	 * 'type' => 'check'
	 * 
	 * @return void
	 */
	protected function addControl_check( $key, $element ) 
	{
		$config  = $element['config'];
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => Controls_Manager::SWITCHER,
				'default' => $config['default'] ?? 0,
				'return_value' => 1,
			]
		);
	}
	
	/**
	 * 'type' => 'number'
	 * 
	 * @return void
	 */
	protected function addControl_number( $key, $element ) 
	{
		$config  = $element['config'];
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => Controls_Manager::NUMBER,
				'default' => $config['default'] ?? 0,
				'min' => $config['min'] ?? 0,
				'max' => $config['max'] ?? 999999,
				'step' => $config['step'] ?? 1,
			]
		);
	}

	/**
	 * 'type' => 'input'
	 * 
	 * @return void
	 */
	protected function addControl_input( $key, $element ) 
	{
		$config  = $element['config'];
		
		$isResponsive = $config['responsive'] ?? false;
		$controlMethod = $isResponsive ? 'add_responsive_control' : 'add_control';

		$options = [
			'label' => __( $element['label'], $this->slug ),
			'description' => __( $element['description'] ?? '', $this->slug ),
			'type' => Controls_Manager::TEXT,
			'default' => esc_html__( $config['default'] ?? '', $this->slug ),
			'placeholder' => $config['placeholder'] ?? '',
		];
		
		$this->{$controlMethod}(
			$key,
			$options
		);
	}
	
	/**
	 * 'type' => 'slider'
	 * 
	 * @return void
	 */
	protected function addControl_slider( $key, $element ) 
	{
		$config  = $element['config'];
		
		$isResponsive = $config['responsive'] ?? true;
		$controlMethod = $isResponsive ? 'add_responsive_control' : 'add_control';
		
		$cssAttr = $config['cssAttr'] ?? $key;

		$options = \nn\wp::Arrays([
			'label' => __( $element['label'], $this->slug ),
			'description' => __( $element['description'] ?? '', $this->slug ),
			'type' => Controls_Manager::SLIDER,
			'default' => [
				'unit' => '%',
				'size' => 50,
			],
			'size_units' => [ 'px', 'em', '%', 'vw', 'vh' ],
			'range' => [
				'px' => [
					'min' => $config['min'] ?? 0,
					'max' => $config['max'] ?? 1200,
				],
			],
			'selectors' => [
				'{{WRAPPER}}.elementor-widget-' . $this->slug . ' .' . $this->cssSelectorPrefix . $key => $cssAttr . ': {{SIZE}}{{UNIT}};',
			],
		])->merge( $config );

		$this->{$controlMethod}(
			$key,
			$options
		);
	}
	
	/**
	 * 'type' => 'colorpicker'
	 * 
	 * @return void
	 */
	protected function addControl_colorpicker( $key, $element ) 
	{
		$config  = $element['config'];
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}}.elementor-widget-' . $this->slug . ' .' . $this->cssSelectorPrefix . $key => 'color: {{VALUE}};',
				],
			]
		);
	}
	
	/**
	 * 'type' => 'typography'
	 * 
	 * @return void
	 */
	protected function addControl_typography( $key, $element ) 
	{
		$config  = $element['config'];
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => $key,
				'description' => __( $element['description'] ?? '', $this->slug ),
				'scheme' => \Elementor\Core\Schemes\Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .' . $this->cssSelectorPrefix . $key,
			]
		);
	}
	
	/**
	 * 'type' => 'icon'
	 * 
	 * @return void
	 */
	protected function addControl_icon( $key, $element ) 
	{
		$config  = $element['config'];
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
			]
		);
	}

	/**
	 * 'type' => 'text'
	 * 
	 * @return void
	 */
	protected function addControl_text( $key, $element ) 
	{
		$config  = $element['config'];
		$enableRichtext = $config['enableRichtext'] ?? false;

		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => $enableRichtext ? Controls_Manager::WYSIWYG : Controls_Manager::TEXTAREA,
				'default' => esc_html__( $config['default'] ?? '', $this->slug ),
			]
		);
	}
	
	/**
	 * 'type' => 'select'
	 * 
	 * @return void
	 */
	protected function addControl_select( $key, $element ) 
	{
		$config  = $element['config'];
		$items = $config['items'] ?? [];

		$useIcons = $items[0][2] ?? false;
		$options = array_combine(
			array_column( $items, 1 ),
			array_column( $items, 0 ),
		);
		if ($useIcons) {
			foreach ($items as $n=>$item) {
				$k = $item[1];
				$options[$k] = [
					'icon' => $item[2],
					'title' => $options[$k],
				];
			}
		}

		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => $useIcons ? Controls_Manager::CHOOSE : Controls_Manager::SELECT,
				'default' => esc_html__( $config['default'] ?? '', $this->slug ),
				'options' => $options
			]
		);
	}

	/**
	 * 'type' => 'media'
	 * 
	 * @return void
	 */
	protected function addControl_media( $key, $element ) 
	{
		$config  = $element['config'];
		$allowMultiple = ($config['maxitems'] ?? 999) > 1;

		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'type' => $allowMultiple ? Controls_Manager::GALLERY : Controls_Manager::MEDIA,
				'default' => [],
			]
		);
	}

	/**
	 * 'type' => 'category'
	 * 
	 * @return void
	 */
	protected function addControl_category( $key, $element ) 
	{
		$config  = $element['config'];
		$type = $config['type'];
		$options = \nn\wp::Arrays(\nn\wp::SysCategory()->findAll( $type ))->key('term_id')->pluck('name')->toArray();
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $options,
				'default' => [],
			]
		);
	}
	
	/**
	 * 'type' => 'relation'
	 * 
	 * @return void
	 */
	protected function addControl_relation( $key, $element ) 
	{
		$config  = $element['config'];
		$type = $config['allowed_post_types'];
		$options = \nn\wp::Arrays(\nn\wp::Post()->getAll( $type ))->key('ID')->pluck('post_title')->toArray();
		$this->add_control(
			$key,
			[
				'label' => __( $element['label'], $this->slug ),
				'description' => __( $element['description'] ?? '', $this->slug ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $options,
				'default' => [],
			]
		);
	}

}
