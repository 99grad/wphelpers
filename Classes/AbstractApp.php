<?php

namespace Nng\Nnhelpers;

/**
 * 
 * ```
 * use \Nng\Nnhelpers\AbstractApp;
 * 
 * class My\Extension\App extends AbstractApp
 * {
 *  ...
 * }
 * ```
 * 
 */
abstract class AbstractApp 
{
	/**
	 * @var string
	 */
	public $extKey = '';

	/**
	 * @var string
	 */
	public $extName = '';

	/**
	 * @var string
	 */
	public $vendor = '';

	/**
	 * @var array
	 */
	public $configuration = [
		'pluginFile' 	=> '',
		'vendor' 		=> '',
		'extName' 		=> '',
		'extKey' 		=> '',
		'namespace' 	=> '',
	];

	/**
	 * List of paths to the CPT.
	 * 
	 * If `null` the paths will be automatically set by loading all
	 * CPT-configurations from `EXT:yourext/Configuration/CPT/*`
	 * 
	 * This property can be set to FALSE inside of your own App if
	 * you don't have any CPT in use.
	 * 
	 * @var array|boolean
	 */
	public $customPostTypes = [];
	
	/**
	 * Same for blocks
	 * 
	 * @var array|boolean
	 */
	public $blocks = [];

	/**
	 * and for Elementor
	 * 
	 * @var array|boolean
	 */
	public $elementorWidgets = [];


	/**
	 * Called immediatly after creating an instance of this class.
	 * Sets configuration needed for the App.
	 * 
	 * @return void
	 */
	public function configure( $config = [] ) 
	{
		$this->configuration = array_merge($this->configuration, $config);
		$this->extKey = $config['extKey'] ?? '';
		$this->extName = $config['extName'] ?? '';
		$this->vendor = $config['vendor'] ?? '';

		$this->customPostTypes = $this->parseCustomPostTypes();
	}

	/**
	 * Called 1x when site and plugins are loaded - on `do_action('plugins_loaded')`
	 * All Plugins are loaded and all `nnhelpers`-based plugins are registered here.
	 * 
	 * see: 
	 * `\nn\bootstrap->registerDependentPlugins()` 
	 * `\nn\wp::ExtensionManager()->initializeApps()`
	 * 
	 * @return void
	 */
	public function register() {}
	
	/**
	 * Called 1x when site and plugins are loaded - on `do_action('init')`
	 * 
	 * see: 
	 * `\nn\bootstrap->registerDependentPlugins()` 
	 * `\nn\wp::ExtensionManager()->initRegisteredExtensions()`
	 * 
	 * @return void
	 */
	public function init() {
		$this->blocks = $this->parseBlocks();
	}
	
	/**
	 * Called 1x when site and plugins are loaded - on `do_action('init')`
	 * immediatly after `init()` was called.
	 * 
	 * see: 
	 * `\nn\bootstrap->registerDependentPlugins()` 
	 * `\nn\wp::ExtensionManager()->initRegisteredExtensions()`
	 * 
	 * @return void
	 */
	public function run() {}

	/**
	 * Return list of the CPTs.
	 * 
	 * If no CPTs were set in the `$customPostTypes` property: Automatically read
	 * all CPT-configurations from the path `EXT:myplugin/Configuration/CPT/*`
	 * 
	 * @return array
	 */
	public function parseCustomPostTypes() 
	{
		if ($this->customPostTypes) {
			return $this->customPostTypes;
		}
		if ($this->customPostTypes === false) {
			return [];
		}
		
		$cpts = [];

		$cptDefinitions = \nn\wp::File()->readAll("EXT:{$this->extKey}/Configuration/CPT/", '*.php');
		$namespace = $this->configuration['namespace'];

		foreach ($cptDefinitions as $filename=>$config) {

			// nn_address --> NnAddress
			$ucFilename = \nn\wp\GeneralUtility::underscoredToUpperCamelCase( $filename );

			// default Controller ...
			$controller = \Nng\Nnhelpers\Controller\CptController::class;

			// ... can be overridden, if special Controller for this CPT exists
			$classesToCheck = [
				"{$namespace}\\Controller\\CPT\\{$ucFilename}Controller",
				"{$namespace}\\Controller\\CPT\\{$ucFilename}",
			];
			foreach ($classesToCheck as $className) {
				if (class_exists($className)) {
					$controller = $className;
					break;
				}
			}

			$config = \nn\wp::Arrays([
				'extKey'			=> $this->extKey,
				'vendor'			=> $this->vendor,
				'postType'			=> "{$this->extKey}_{$filename}",
				'controller' 		=> $controller,
				'plugin' 			=> $this->configuration,
				'identifier' 		=> "{$this->extKey}_{$filename}",
				'modelClassName'	=> "{$this->vendor}\\{$this->extName}\\Domain\\Model\\CPT\\{$ucFilename}",
			])->merge($config);

			$cpts[] = \nn\wp::CPT()->factory( $config );
		}

		return $cpts;
	}


	/**
	 * Return list of the Blocks.
	 * 
	 * @return array
	 */
	public function parseBlocks() 
	{
		if ($this->blocks) {
			return $this->blocks;
		}
		if ($this->blocks === false) {
			return [];
		}
		
		$blocks = [];

		$blockDefinitions = \nn\wp::File()->readAll("EXT:{$this->extKey}/Configuration/Blocks/", '*.php');
		$namespace = $this->configuration['namespace'];

		foreach ($blockDefinitions as $filename=>$config) {

			// ----------------------------------
			// SHORTCODE registration

			// nn_address --> NnAddress
			$ucFilename = \nn\wp\GeneralUtility::underscoredToUpperCamelCase( $filename );

			// default Controller ...
			$controller = \Nng\Nnhelpers\Controller\BlockController::class;

			// ... can be overridden, if special Controller for this CPT exists
			$classesToCheck = [
				"{$namespace}\\Controller\\Blocks\\{$ucFilename}Controller",
				"{$namespace}\\Controller\\Blocks\\{$ucFilename}",
			];
			foreach ($classesToCheck as $className) {
				if (class_exists($className)) {
					$controller = $className;
					break;
				}
			}

			$ucFilename = \nn\wp\GeneralUtility::underscoredToUpperCamelCase($filename);

			$config = \nn\wp::Arrays([
				'filename'		=> $filename,
				'controller' 	=> $controller,
				'plugin' 		=> $this->configuration,
				'identifier' 	=> "{$this->extKey}_{$filename}",
				'template' 		=> "EXT:{$this->extKey}/Resources/Templates/Blocks/{$ucFilename}",
			])->merge($config);

			$blocks[] = \nn\wp::Block()->factory( $config );

			// ----------------------------------
			// ELEMENTOR registration

			if (did_action( 'elementor/loaded' )) {

				$controller = \Nng\Nnhelpers\Controller\ElementorController::class;

				// ... can be overridden, if special Controller for this CPT exists
				$classesToCheck = [
					"{$namespace}\\Controller\\Elementor\\{$ucFilename}Controller",
					"{$namespace}\\Controller\\Elementor\\{$ucFilename}",
				];
				
				foreach ($classesToCheck as $className) {
					if (class_exists($className)) {
						$controller = $className;
						break;
					}
				}
	
				$config['controller'] = $controller;
				$config['template'] = "EXT:{$this->extKey}/Resources/Templates/Elementor/{$ucFilename}";
	
				$this->elementorWidgets[] = \nn\wp::Elementor()->factory( $config );
			}

		}

		return $blocks;
	}

	/**
	 * Called 1x after installing the plugin in the backend.
	 * Can be used to install database-tables, check dependencies etc.
	 * 
	 * See `\nn\wp::ExtensionManager()->initializeApps()` for details.
	 * 
	 * @return void
	 */
	public function install () 
	{
		$messages = [];
		$messages[] = 'Die Extension <code>' . $this->extKey . '</code> wurde installiert.';

		// If `EXT:myplugin/ext_tables.sql` exists, create tables in database
		$tables = \nn\wp::Db()->createTablesFromExtTablesFile("EXT:{$this->extKey}/ext_tables.sql");
		if ($tables) {
			$messages[] = 'Folgende Tabellen wurden angelegt: <code>' . join( '</code>, <code>', array_keys($tables) ) . '</code>';
		}

		\nn\wp::Message()->OK('Extension installiert', join('<br>', $messages) );
	}

	/**
	 * Called 1x after UNinstalling the plugin in the backend.
	 * Can be used to clean up the installation, remove tables etc.
	 * 
	 * See `\nn\wp::ExtensionManager()->initializeApps()` for details.
	 * 
	 * @return void
	 */
	public function uninstall () 
	{
		\nn\wp::Message()->OK('Extension deaktiviert', 'Die Extension <code>' . $this->extKey . '</code> wurde deaktiviert.' );
	}

}
