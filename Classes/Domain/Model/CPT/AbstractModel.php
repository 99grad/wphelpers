<?php

namespace Nng\Nnhelpers\Domain\Model\CPT;

class AbstractModel 
{
    public $ID;
    public $postAuthor;
    public $postDate;
    public $postDateGmt;
    public $postContent;
    public $postTitle;
    public $postExcerpt;
    public $postStatus;
    public $commentStatus;
    public $pingStatus;
    public $postPassword;
    public $postName;
    public $toPing;
    public $pinged;
    public $postModified;
    public $postModifiedGmt;
    public $postContentFiltered;
    public $postParent;
    public $guid;
    public $menuOrder;
    public $postType;
    public $postMimeType;
    public $postCommentCount;
}
