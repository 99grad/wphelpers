<?php

namespace Nng\Nnhelpers\Domain\Model;

class SysFile 
{
	/**
	 * @var integer
	 */
	public $ID = 0;

	/**
	 * @var integer
	 */
	public $width;

	/**
	 * @var integer
	 */
	public $height;

	/**
	 * @var string
	 */
	public $publicUrl;
	
	/**
	 * @var string
	 */
	public $alt;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $mime_type;

	/**
	 * @var string
	 */
	public $lazy;

	/**
	 * @var string
	 */
	public $filepath;

	/**
	 * @var string
	 */
	public $processedFilepath;

	/**
	 * @var array
	 */
	public $originalResource = [];

	/**
	 * @var string
	 */
	private $srcset = '';

	/**
	 * @var string
	 */
	private $sizes = '';

	/**
	 * @return array
	 */
	public function getSrcset() 
	{
		$this->calculateSrcset();
		return $this->srcset;
	}

	/**
	 * @return array
	 */
	public function getSizes() 
	{
		$this->calculateSrcset();
		return $this->sizes;
	}
	
	/**
	 * @return array
	 */
	public function updateSize() 
	{
		if ($this->processedFilepath && $info = getimagesize($this->processedFilepath)) {
			$this->width = $info[0];
			$this->height = $info[1];
			$this->mime_type = $info['mime'];
		}
		return [
			'width' 	=> $this->width, 
			'height'	=> $this->height
		];
	}

	/**
	 * Calculate srcset and sizes
	 * see: https://developer.wordpress.org/reference/functions/wp_get_attachment_image/
	 * 
	 * @return void
	 */
	public function calculateSrcset() 
	{
		if ($this->srcset || !$this->ID) return;

		$metadata = wp_get_attachment_metadata( $this->ID );

		if ($metadata) {
			
			$src = $this->filepath;
			$size_array = [absint( $metadata['width'] ), absint( $metadata['height'] )];
			$srcset     = wp_calculate_image_srcset( $size_array, $src, $metadata, $this->ID );
			$sizes      = wp_calculate_image_sizes( $size_array, $src, $metadata, $this->ID );

			if ( $srcset && ( $sizes || ! empty( $attr['sizes'] ) ) ) {
				$this->srcset = $srcset;
				$this->sizes = $sizes;
			}
		}
	}
}
