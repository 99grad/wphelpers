<?php

namespace Nng\Nnhelpers\Domain\Repository\CPT;

abstract class AbstractRepository {

	/**
	 * The `Model` that this repository will return
	 * 
	 * @var string
	 */
	var $model = 'Nng\Nnhelpers\Domain\Model\CPT\DefaultModel';

	/**
	 * The `posts`.`post_type` related to this repository
	 * 
	 * @var string
	 */
	var $postType = '';

	public function __construct() 
	{
		// get `Model` related to this Repository, e.g. `Nng\Nnaddress\Domain\Model\CPT\AddressModel`
		$className = get_class( $this );
		$modelClassName = \nn\wp::Db()->getModelForRepository( $className, false );

		if (class_exists($modelClassName)) {
			$this->model = $modelClassName;

			// get `post_type` related to the Model, e.g. `myextname_modelname`
			$config = \nn\wp::CPT()->getConfigurationArrayForModel( $modelClassName );
			$this->postType = $config['postType'];
		}
	}

	/**
	 * Return an instance of the QueryBuilder.
	 * Prefilter current `post_type`
	 * 
	 * @return QueryBuilder
	 */
	public function createQuery() 
	{
		$queryBuilder = \nn\wp::injectClass( \Nng\Nnhelpers\Utilities\QueryBuilder::class );
		$queryBuilder
			->setPostType( $this->postType );
		return $queryBuilder;
	}

	/**
	 * Magic PHP function: 
	 * Gets called whenever the method is not defined in this class.
	 * 
	 * @param string $name
	 * @param array $arguments
	 * @return mixed
	 */
	public function __call ( $name, $arguments ) 
	{
		// any method was called prefixed with `findBy*`
		if (strpos($name, 'findBy') === 0) {

			// `findByMyColumn` --> `my_column`
			$column = str_replace('findBy', '', $name);
			$column = \nn\wp\GeneralUtility::camelCaseToLowerCaseUnderscored( $column );

			// for intuitive use of `findByUid()`: special mapping of `uid` to `post.ID` 
			if ($column == 'uid' || $column == 'i_d') {
				$column = 'ID';
			}

			// ID is unique – so we will return only the first result 
			$returnFirstResult = $column == 'ID';

			$value = array_pop( $arguments );

			// are we searching in the default `wp_post` columns of the DB?
			$isPostColumn = in_array( $column, \Nng\Nnhelpers\Utilities\Db::$defaultPostColumnNames );

			$posts = [];

			// ... then we can do a simple query
			if ($isPostColumn) {
				$posts = \nn\wp::Db()->findByValues( 'posts', [$column=>$value] );
			} else {
				// ToDo!
			}

			if (!$posts) {
				return $returnFirstResult ? null : [];
			}

			// map array to Models
			foreach ($posts as &$post) {
				$meta = \nn\wp::Post()->getMeta( $post['ID'] );
				$post['_meta'] = $meta ?: [];
				$post = \nn\wp::Convert($post)->toModel( $this->model );
			}

			return $returnFirstResult ? array_shift($posts) : $posts;
		}
	}

	/**
	 * Find all entries
	 * 
	 * @return array
	 */
	public function findAll() 
	{
		if (!$this->postType) return [];
		return $this->findByPostType( $this->postType );
	}

}