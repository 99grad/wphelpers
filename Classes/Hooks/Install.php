<?php

namespace Nng\Nnhelpers\Hooks;

class Install
{
	/**
	 * Wird beim Installiern des plugins aufgerufen.
	 * 
	 * @return void
	 */
	public static function registerActivationAction () 
	{
		if (!current_user_can('activate_plugins')) {
			die('\Nng\Nnhelpers\Hooks\Install->registerActivationAction(): You are not privileged to activate this plugin.');
		}

		if (!self::createFolders()) {
			die('\Nng\Nnhelpers\Hooks\Install->createFolders(): Required folders could not be created.');
		}
	}

	/**
	 * Erforderliche Ordner erzeugen
	 * 
	 * @return boolean
	 */
	public static function createFolders() 
	{
		$folders = [
			\nn\wp::File()->absPath(\Nng\Nnhelpers\Utilities\Template::$cacheDir),
			\nn\wp::File()->absPath(\Nng\Nnhelpers\Utilities\File::$processedDir),
		];

		foreach ($folders as $path) {
			if (!file_exists($path)) {
				$success = \nn\wp::File()->mkdir( $path );
				if (!$success) return false;
			}
		}
		return true;
	}

}