<?php

namespace Nng\Nnhelpers\View;

class FluidBasedView extends AbstractView
{
    /**
     * var \Nng\Nnhelpers\Utilities\Template
     */
    public $view;

    public function __construct() 
    {
        $this->view = \nn\wp::Template();
    }
}