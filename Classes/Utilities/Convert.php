<?php

namespace Nng\Nnhelpers\Utilities;

use \nn\wp\GeneralUtility;

/**
 * 
 */
class Convert extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Cache the CPT and Model configurations
	 * 
	 * @var array
	 */
	protected $modelDefinitionCache = [];

	/**
	 * 	@var mixed
	 */
	protected $initialArgument;

	/**
	 * 	Klasse konstruieren.
	 */
	public function __construct ( $obj = null ) {
		$this->initialArgument = $obj;
		return $this;
	}

	/**
	 * Konvertiert ein Array oder UID in ein CPT-/Post-Model
	 * 
	 * ```
	 * // Post-Array konvertieren in bestimmten Typ
	 * $post = \nn\wp::Post()->getWithMeta( $ID );
	 * $model = \nn\wp::Convert( $postData )->toModel( \Nng\Nnaddress\Domain\Model\CPT\Address::class );
	 * 
	 * // Post-Array konvertieren, Typ automatisch ermitteln
	 * $model = \nn\wp::Convert( $postData )->toModel();
	 * 
	 * // ID eines Posts automatisch erkennen und konvertieren
	 * $model = \nn\wp::Convert( $ID )->toModel();
	 * ```
	 * 
	 * @return \Nng\Nnhelpers\Domain\Model\CPT\AbstractModel
	 */
	public function toModel( $className = '' ) 
	{
		$arr = $this->initialArgument;

		if (is_numeric($arr)) {
			$arr = \nn\wp::Post()->getWithMeta( $arr );
			if (!$arr) return [];
		}
		
		if (!$className && isset($arr['post_type']) ) {
			$className = \nn\wp::CPT()->getModelForPostType( $arr['post_type'] );
		}

		if (!$className) {
			return $arr;
		}

		if (!($this->modelDefinitionCache[$className] ?? false)) {
			$this->modelDefinitionCache[$className] = \nn\wp::CPT()->getConfigurationArrayForModel( $className );
		}

		$modelDefinition = $this->modelDefinitionCache[$className];
		$extKey = $modelDefinition['extKey'] ?? false;

		$model = \nn\wp::injectClass( $className );		
		if (!$model) return false;

		$metaFields = [];
		foreach (($modelDefinition['forms'] ?? []) as $name=>$config) {
			$metaFields = array_merge( $metaFields, $config['columns'] ?? [] );
		}

		foreach ($arr as $k=>$val) {
			if ($k == '_meta') continue;
			$k = GeneralUtility::underscoredToLowerCamelCase( $k );
			if ($k == 'id') $k = 'ID';
			if (!property_exists($model, $k)) continue;
			$model->{$k} = $val;
		}

		foreach ($metaFields as $k=>$config) {

			$propKey = '_' . $extKey . '_' . $k;
			$k = GeneralUtility::underscoredToLowerCamelCase( $k );
			
			if (!property_exists($model, $k)) continue;
			$val = $arr['_meta'][$propKey] ?? '';
			if (is_array($val)) {
				$val = array_pop($val);
			}
			
			$vald = @unserialize($val);
			if ($vald !== false ) $val = $vald;

			$type = $config['config']['type'] ?? false;

			switch ( $type ) {
				case 'relation':
					if (!$val) $val = [];
					foreach ($val as $i=>$uid) {
						$val[$i] = \nn\wp::Convert( $uid )->toModel();
					}
					break;
				case 'media':
					if (!$val) $val = [];
					foreach ($val as $i=>$uid) {
						$val[$i] = \nn\wp::Fal()->getSysFileByUid( $uid );
					}
					break;
			}

			$model->{$k} = $val;
		}

		return $model;
	}
}