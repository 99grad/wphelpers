<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * @see https://crocoblock.com/freemium/tools/wp-query-generator/
 * 
 * 
 */
class QueryBuilder extends \Nng\Nnhelpers\Singleton 
{

	/**
	 * Default query params for `WP_Query`
	 * @var array
	 */
	private $queryArray = [
		'posts_per_page' => -1,			// get ALL entries, not only first 10
		'post_status' => 'publish',		// ignore hidden (= draft/unpublished) records
	];

	/**
	 * Mapping of DB-fieldnames to "strange" keys for WP_Query
	 * 
	 * `db-field` => `WP_Query-key` 
	 * 
	 * @var array
	 */
	private $keyMapping = [
		'post_title' 	=> 'title',
		'post_type' 	=> 'post_type',
		'post_status' 	=> 'post_status',
		'ID' 			=> 'p',
		'post_author' 	=> 'author',
		'author_name' 	=> 'author_name',
		'post_name' 	=> 'name',
		'post_parent'	=> 'post_parent',
		'category'		=> 'category',
		'tag'			=> 'tag',
	];

	/**
	 * Constructor
	 * @return self
	 */
	public function __construct() 
	{
		return $this;
	}

	/**
	 * Set postType
	 * @return self
	 */
	public function setPostType( $type = 'post' ) 
	{
		$this->queryArray['post_type'] = $type;
	}

	/**
	 * Get/hide hidden records
	 * @return self
	 */
	public function setIgnoreEnableFields( $ignore = false ) 
	{
		if ($ignore === true) {
			unset( $this->queryArray['post_status'] );
		} else {
			$this->queryArray['post_status'] = 'publish';
		}
		return $this;
	}

	/**
	 * Set limit of results
	 * @return self
	 */
	public function setLimit( $num = 0 ) 
	{
		$this->queryArray['posts_per_page'] = intval($num);
		$this->queryArray['page'] = 1;
		return $this;
	}

	/**
	 * Set limit of results
	 * @return self
	 */
	public function setOffset( $num = 0 ) 
	{
		$this->queryArray['offset'] = intval($num);
		return $this;
	}
	
	/**
	 * Set ordering / sorting of results
	 * ```
	 * $query->setOrderings( 'post_title', 'ASC' );
	 * $query->setOrderings( ['post_title'=>'ASC', 'post_author'=>'ASC'] );
	 * ```
	 * @return self
	 */
	public function setOrderings( $arr = [], $order = 'ASC' ) 
	{
		if (is_string($arr)) {
			$arr = [$arr=>$order];
		}
		$this->queryArray['orderby'] = $arr;
		return $this;
	}
	
	/**
	 * Add AND constraint
	 * ```
	 * $query->logicalAnd( 
	 * 	$query->equals( 'post_title', 'Test' ),
	 * 	$query->equals( 'post_author', 'John' )
	 * );
	 * ```
	 * @return self
	 */
	public function logicalAnd() 
	{
		$constraints = func_get_args() ?: [];
		foreach ($constraints as $constraint) {
			
			[$field, $type, $value] = $constraint;

			$q = $this->keyMapping[$field] ?? false;
			
			if (!$q) {
				if (strpos($field, 'meta.') === 0) {
					$field = substr($field, 5);
					if (!isset($this->queryArray['meta_query'])) {
						$this->queryArray['meta_query'] = [
							'relation' => 'AND'
						];
					}
					$this->queryArray['meta_query'][] = [
						'key' 		=> $field,
						'value' 	=> $value,
						'compare' 	=> $type,
					];
					continue;
				}
			}

			if ($type == '=') {
				if ($q == 'category') 	$q = 'cat';
				if ($q == 'tag') 		$q = 'tag_id';
				$this->queryArray[$q] = $value;
			}
			if ($type == 'IN') {
				$this->queryArray[$q . '__in'] = $value;
			}
			if ($type == 'NOT_IN') {
				$this->queryArray[$q . '__not_in'] = $value;
			}
		}
		return $this;
	}
	
	/**
	 * Add equals constraint
	 * ```
	 * $query->logicalAnd( 
	 * 	$query->equals( 'post_title', 'Test' ),
	 * 	$query->equals( 'post_author', 'John' )
	 * );
	 * 
	 * $query->logicalAnd( 
	 * 	$query->equals( 'meta._nnsofa_event_www', 'https://www.eltville.de' ),
	 * 	$query->equals( 'meta._nnsofa_organizer_city', 'Wiesbaden' )
	 * );
	 * ```
	 * @return self
	 */
	public function equals( $field = '', $value = '' ) 
	{
		return [$field, '=', \nn\wp::Db()->quote($value)];
	}

	/**
	 * Add '>' constraint
	 * @return self
	 */
	public function greaterThan( $field = '', $value = '' ) 
	{
		return [$field, '>', \nn\wp::Db()->quote($value)];
	}

	/**
	 * Add '>=' constraint
	 * @return self
	 */
	public function greaterThanOrEqual( $field = '', $value = '' ) 
	{
		return [$field, '>=', \nn\wp::Db()->quote($value)];
	}

	/**
	 * Add '<' constraint
	 * @return self
	 */
	public function lessThan( $field = '', $value = '' ) 
	{
		return [$field, '>', \nn\wp::Db()->quote($value)];
	}
	
	/**
	 * Add '<=' constraint
	 * @return self
	 */
	public function lessThanOrEqual( $field = '', $value = '' ) 
	{
		return [$field, '<=', \nn\wp::Db()->quote($value)];
	}
	
	/**
	 * Add 'LIKE' constraint
	 * DO NOT add `%...%` around keyword - WP_Query handles this differently
	 * ```
	 * $query->logicalAnd( 
	 * 	$query->in( 'meta._myext_field', 'Wiesbaden' ),
	 * );
	 * ```
	 * @return self
	 */
	public function like( $field = '', $value = '' ) 
	{
		$str = \nn\wp::Db()->fullQuote($value);
		$str = trim( $str, "%'");
		return [$field, 'LIKE', $str ];
	}
	
	/**
	 * Add IN constraint
	 * ```
	 * $query->logicalAnd( 
	 * 	$query->in( 'category', [1,2,3] ),
	 * );
	 * $query->logicalAnd( 
	 * 	$query->in( 'tag', [1,2,3] ),
	 * );
	 * ```
	 * @return self
	 */
	public function in( $field = '', $value = '' ) 
	{
		return [$field, 'IN', \nn\wp::Db()->quote($value)];
	}
	
	/**
	 * Add NOT IN constraint
	 * ```
	 * $query->logicalAnd( 
	 * 	$query->notIn( 'category', [1,2,3] ),
	 * );
	 * $query->logicalAnd( 
	 * 	$query->notIn( 'tag', [1,2,3] ),
	 * );
	 * ```
	 * @return self
	 */
	public function notIn( $field = '', $value = '' ) 
	{
		return [$field, 'NOT_IN', \nn\wp::Db()->quote($value)];
	}


	/**
	 * Get records
	 * @return array
	 */
	public function execute() 
	{
		$results = [];

		$query = new \WP_Query( $this->queryArray );

		if (!$query->have_posts()) {
			return [];
		}

		while ( $query->have_posts() ) {
			$query->the_post();
			$results[] = (array) $query->post;
		}

		// restore original Post Data
		wp_reset_postdata();

		$postTypeCache = [];
		foreach ($results as &$post) {
			$postType = $post['post_type'];
			if (!isset($postTypeCache[$postType])) {
				$postTypeCache[$postType] = \nn\wp::CPT()->getModelForPostType( $postType );
			}
			if ($model = $postTypeCache[$postType]) {
				$meta = \nn\wp::Post()->getMeta( $post['ID'] );
				$post['_meta'] = $meta ?: [];
				$post = \nn\wp::Convert($post)->toModel( $model );
			}
		}

		return $results;
	}

}