<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Helper for rendering Fluid templates 
 * Typical usages:
 * 
 * ```
 * // Chaining
 * $view = \nn\wp::Template();
 * $view
 * 	->setLayoutRootPaths(['EXT:myplugin/Resources/Layouts/'])
 * 	->setPartialRootPaths(['EXT:myplugin/Resources/Partials/'])
 * 	->setTemplateRootPaths(['EXT:myplugin/Resources/Templates/'])
 * 	->assignMultiple(['a'=>123])
 * 	->render('Show');
 * 
 * // Alternative to `->render('Show')`:
 * $view
 * 	->setTemplate('Show')
 * 	->render();
 * 
 * // Using parameters instead of chaining:
 * \nn\wp::Template()->render( 'EXT:nninfografik/Resources/Private/Index.html', $vars );
 * \nn\wp::Template()->render( 'EXT:nninfografik/Resources/Private/Index.html', $vars, ['templateRootPaths'=>[..., ....]]);
 * \nn\wp::Template()->render( 'Index', $vars, 'nninfografik');
 * ```
 * 
 */
class Template
{
	/**
	 * @var \TYPO3Fluid\Fluid\View\TemplateView
	 */
	private $view;

	/**
	 * @var string
	 */
	public static $cacheDir = 'CACHE:nnhelpers/';

	/**
	 * @var array
	 */
	private $fluidPaths = [
		'template' => '',
		'layoutRootPaths' => [],
		'templateRootPaths' => [],
		'partialRootPaths' => [],
	];

	/**
	 * Fluid initialisieren
	 * 
	 */
	public function __construct() 
	{
		$view = new \TYPO3Fluid\Fluid\View\TemplateView();

		$renderingContext = $view->getRenderingContext();
		$renderingContext->setControllerName('');

		$cacheDir = \nn\wp::File()->absPath( self::$cacheDir );
		$cacheInstance = new \TYPO3Fluid\Fluid\Core\Cache\SimpleFileCache( $cacheDir );

		$view->setCache($cacheInstance);
		$view->getRenderingContext()->getViewHelperResolver()->addNamespace('f', ['Nng\\Nnhelpers\\ViewHelpers', 'TYPO3Fluid\\Fluid\\ViewHelpers']);
		$view->getRenderingContext()->getViewHelperResolver()->addNamespace('nn', 'Nng\\Nnhelpers\\ViewHelpers');

		$this->view = $view;
	}

	/**
	 * Setzt eine View Variable
	 * 
	 * @return self
	 */
	public function assign( $key = '', $val = '' ) 
	{
		$this->view->assignMultiple( [$key=>$val] );
		return $this;
	}
	
	/**
	 * Setzt View Variables
	 * 
	 * @return self
	 */
	public function assignMultiple( $vars = [] ) 
	{
		$this->view->assignMultiple( $vars );
		return $this;
	}
	
	/**
	 * @return self
	 */
	public function setLayoutRootPaths( $arr = [] ) 
	{
		$this->fluidPaths['layoutRootPaths'] = $arr;
		return $this;
	}
	
	/**
	 * @return self
	 */
	public function setTemplateRootPaths( $arr = [] ) 
	{
		$this->fluidPaths['templateRootPaths'] = $arr;
		return $this;
	}

	/**
	 * @return self
	 */
	public function setPartialRootPaths( $arr = [] ) 
	{
		$this->fluidPaths['partialRootPaths'] = $arr;
		return $this;
	}
	
	/**
	 * @return self
	 */
	public function setTemplate( $name = '' ) 
	{
		$this->fluidPaths['template'] = $name;
		return $this;
	}

	/**
	 * Rendert ein Fluid-Template
	 * 
	 * ```
	 * \nn\wp::Template()->render( 'EXT:nninfografik/Resources/Private/Index.html', $vars );
	 * \nn\wp::Template()->render( 'EXT:nninfografik/Resources/Private/Index.html', $vars, ['templateRootPaths'=>[..., ....]]);
	 * \nn\wp::Template()->render( 'Index', $vars, 'nninfografik');
	 * ```
	 * @param string $path
	 * @param array $vars
	 * @param array $templatePaths
	 * @return string
	 */
	public function render( $path = null, $vars = [], $templatePaths = [] ) 
	{
		if ($vars) {
			$this->view->assignMultiple( $vars );
		}

		if (!$templatePaths) {
			$templatePaths = $this->fluidPaths;
		}

		$path = $path ?: $this->fluidPaths['template'];

		if (is_string($templatePaths)) {
			$path = 'EXT:' . $templatePaths . '/Resources/Private/Templates/' . $path;
			$templatePaths = [];
		}

		$templateRootPath = '';
		$templateName = $path;

		if (strpos($path, ':') !== false) {

			// EXT:nninfografik/... --> /var/www/website/wp-plugins/nninfografik/...
			$path = \nn\wp::File()->absPath( $path );

			// var/www/.../Resources/Private/Index.html --> Index
			$templateName = pathinfo( $path, PATHINFO_FILENAME );

			$templateRootPath = pathinfo($path, PATHINFO_DIRNAME).'/';
		}
		
		// TemplateRootPaths automatisch ermitteln
		if ($templateRootPath) {
			$templatePaths['templateRootPaths'] = array_merge([
				$templateRootPath
			], $templatePaths['templateRootPaths'] ?? []);
		}

		$templatePaths['partialRootPaths'] = array_merge([
			$templateRootPath . 'Partials/',
			\nn\wp::File()->normalizePath($templateRootPath . '../Partials/'),
			\nn\wp::File()->normalizePath($templateRootPath . '../../Partials/'),
		], $templatePaths['partialRootPaths'] ?? []);

		$templatePaths['layoutRootPaths'] = array_merge([
			$templateRootPath . 'Layouts/',
			\nn\wp::File()->normalizePath($templateRootPath . '../Layouts/'),
			\nn\wp::File()->normalizePath($templateRootPath . '../../Layouts/')
		], $templatePaths['layoutRootPaths'] ?? []);


		// `EXT:`-Pfade in `templateRootPaths` etc. auflösen
		foreach ($templatePaths as $k=>&$arr) {
			if (!is_array($arr)) continue;
			$arr = array_unique($arr);
			foreach ($arr as &$p) {
				$p = \nn\wp::File()->absPath( $p );
			}
		}

		$paths = $this->view->getTemplatePaths();
		$paths->setLayoutRootPaths( $templatePaths['layoutRootPaths'] );
		$paths->setTemplateRootPaths( $templatePaths['templateRootPaths'] );
		$paths->setPartialRootPaths( $templatePaths['partialRootPaths'] );

		try {
			return $this->view->render( $templateName );
		} catch ( \Exception $e ) {
			return '<div class="alert alert-danger">' . $e->getMessage() . '</div>';
		}
	}
}