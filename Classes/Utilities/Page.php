<?php

namespace Nng\Nnhelpers\Utilities;

/**
 *
 */
class Page extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * URL zu einer Seite holen.
	 * Da Pages = Posts = Beiträge = Alles ist diese Methode ein
	 * Alias zu `\nn\wp::Post()->getLink( $postID )`
	 * ```
	 * \nn\wp::Page()->getLink( $post_ID );
	 * ```
	 * @param integer $postID
	 * @return string
	 */
	public function getLink( $postID = null ) 
	{
		return \nn\wp::Post()->getLink( $postID );
	}

	/**
	 * HTML-Code in <head> einschleusen
	 * Siehe `\nn\wp::Page()->addHeader()` für einfachere Version.
	 * ```
	 * \nn\wp::Page()->addHeaderData( '<script src="..."></script>' );
	 * ```
	 * @return void
	 */
	public function addHeaderData( $html = '' ) {
		//$this->getPageRenderer()->addHeaderData( $html );
	}
	
	/**
	 * HTML-Code vor Ende der <body> einschleusen
	 * Siehe `\nn\wp::Page()->addFooter()` für einfachere Version.
	 * ```
	 * \nn\wp::Page()->addFooterData( '<script src="..."></script>' );
	 * ```
	 * @return void
	 */
	public function addFooterData( $html = '' ) {
		//$this->getPageRenderer()->addFooterData( $html );
	}

	/**
	 * JS-Library in <head> einschleusen.
	 * ```
	 * \nn\wp::Page()->addJsLibrary( 'EXT:name/Resource/Public/Js/script.js' );
	 * \nn\wp::Page()->addJsLibrary( ['myidentifier'=>'EXT:name/Resource/Public/Js/script.js'] );
	 * \nn\wp::Page()->addJsLibrary( ['path'=>'EXT:name/Resource/Public/Js/script.js', 'identifier'=>'myidentifer', 'depends'=>'nn'] );
	 * ```
	 * @return string
	 */
	public function addJsLibrary( $path, $depends = [], $inFooter = false ) {

		$returnArray = true;

		// normalize: `EXT:path/to/file.js` --> `[['path'=>'EXT:path/to/file.js', 'identifier'=>'a6b754af17']]`
		if (!is_array($path)) {
			$returnArray = false;
			$path = \nn\wp::File()->absUrl( $path );
			$identifier = md5($path);
			$path = [['path'=>$path, 'identifier'=>$identifier]];
		}

		$identifiers = [];

		foreach ($path as $identifier=>$config) {

			// normalize `'myidentifier'=>'EXT:path/to/file.js'` to `['identifier'=>'myidentifier', path:'EXT:path/to/file.js']`
			if (is_string($config)) {
				$config = ['identifier'=>$identifier, 'path'=>$config];
			}

			$file = \nn\wp::File()->absUrl( $config['path'] );
			$identifier = $config['identifier'] ?? false;
			if (!$identifier) $identifier = md5( $file );

			$dependArr = $config['depends'] ?? [];
			if (is_string($dependArr)) $dependArr = [$dependArr];

			if ($depends) {
				$dependArr = array_merge($dependArr, $depends);
			}

			wp_register_script( $identifier, $file, $dependArr, '', $inFooter);
			wp_enqueue_script( $identifier );

			$identifiers[] = $identifier;
		}

		return $returnArray ? $identifiers : $identifier;
	}
	
	/**
	 * JS-Library am Ende der <body> einschleusen
	 * ```
	 * \nn\wp::Page()->addJsFooterLibrary( EXT:name/Resource/Public/Js/script.js' );
	 * ```
	 * @return string
	 */
	public function addJsFooterLibrary($path, $depends = [] ) {
		return $this->addJsLibrary($path, $depends, true);
	}

	/**
	 * JS-Datei in <head> einschleusen
	 * Siehe `\nn\wp::Page()->addHeader()` für einfachere Version.
	 * ```
	 * \nn\wp::Page()->addJsFile( 'EXT:name/Resource/Public/Js/script.js' );
	 * ```
	 * @return string
	 */
	public function addJsFile($path, $depends = [] ) {
		return $this->addJsLibrary($path, $depends);
	}
	
	/**
	 * JS-Datei am Ende der <body> einschleusen
	 * ```
	 * \nn\wp::Page()->addJsFooterFile( 'EXT:name/Resource/Public/Js/script.js' );
	 * ```
	 * @return string
	 */
	public function addJsFooterFile( $path, $depends = [] ) {
		return $this->addJsLibrary($path, $depends, true);	
	}
	
	/**
	 * CSS-Library in <head> einschleusen
	 * ```
	 * \nn\wp::Page()->addCssLibrary( 'EXT:name/Resource/Public/Css/style.css' );
	 * \nn\wp::Page()->addCssLibrary( ['nncss'=>'EXT:name/Resource/Public/Css/style.css'] );
	 * \nn\wp::Page()->addCssLibrary( 'EXT:name/Resource/Public/Css/depends-nncss.css', ['nncss'] );
	 * \nn\wp::Page()->addCssLibrary( ['path'=>'EXT:name/Resource/Public/Css/style.css', 'identifier'=>'myidentifer', 'depends'=>'nn'] );
	 * ```
	 * @return string
	 */
	public function addCssLibrary( $path, $depends = [] ) {

		// normalize: `EXT:path/to/style.css` --> `[['path'=>'EXT:path/to/style.css', 'identifier'=>'a6b754af17']]`
		if (!is_array($path)) {
			$path = \nn\wp::File()->absUrl( $path );
			$identifier = md5($path);
			$path = [['path'=>$path, 'identifier'=>$identifier]];
		}

		foreach ($path as $identifier=>$config) {

			// normalize `'myidentifier'=>'EXT:path/to/style.css'` to `['identifier'=>'myidentifier', path:'EXT:path/to/style.css']`
			if (is_string($config)) {
				$config = ['identifier'=>$identifier, 'path'=>$config];
			}

			$file = \nn\wp::File()->absUrl( $config['path'] );
			$identifier = $config['identifier'] ?? false;
			if (!$identifier) $identifier = md5( $file );

			$dependArr = $config['depends'] ?? $depends;
			if (is_string($dependArr)) $dependArr = [$dependArr];

			wp_enqueue_style( $identifier, $file, $depends );
		}
		return $identifier;
	}

	/**
	 * CSS-Datei in <head> einschleusen
	 * Siehe `\nn\wp::Page()->addHeader()` für einfachere Version.
	 * ```
	 * \nn\wp::Page()->addCss( 'EXT:name/Resource/Public/Css/style.js' );
	 * ```
	 * @return string
	 */
	public function addCssFile( $path, $depends = [] ) {
		return $this->addCssLibrary( $path, $depends );
	}

}