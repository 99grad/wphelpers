<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Methoden, um das Registrieren von AJAX-Enpoints zu vereinfachen.
 * 
 */
class Ajax extends \Nng\Nnhelpers\Singleton 
{
	/**
	 * 	@var array
	 */
	protected $registeredEndpoints = [];

	/**
	 * Einen Endpoint für AJAX-Requests registrieren.
	 * Die Registrierung MUSS für AJAX-Requests bei `add_action( 'init' )` passieren!
	 * 
	 * ```
	 * \nn\wp::Ajax()->registerEndpoint( $config );
	 * \nn\wp::Ajax()->registerEndpoint( $type, $action );
	 * ```
	 * 
	 * Beispiel:
	 * ```
	 * // Registrierung MUSS für AJAX-Requests bei `init` passieren!
	 * add_action( 'init', function() {
	 * 	\nn\wp::Ajax()->registerEndpoint([
	 * 		'type' => 12345,									// kann auch ein String sein, z.B. `my_demo`
	 * 		'func' => 'Nng\MyController\Thing::doSomething',	// kann auch `->doSomething` sein
	 * 	]);
	 * });
	 * 
	 * // Alternativ:
	 * add_action( 'init', function() {
	 * 	\nn\wp::Ajax()->registerEndpoint( 12345, 'Nng\MyController\Thing::doSomething' );
	 * });
	 * ```
	 * 
	 * Im Beispiel oben wäre der Endpoint erreichbar über:
	 * ```
	 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn&type=12345
	 * ```
	 * 
	 * Der Pfad zum Endpoint und alle anderen Einstellungen findet man in JS unter `window.nnhelpers`.
	 * Registriert wird die globale Variable unter `wp_localize_script()` in `registerAjaxHandler()` (s.u.)
	 * ```
	 * var gSettings = window.nnhelpers;
	 * $.post(gSettings.ajax_url, data).done(function(data) {
	 * 	console.log( data );
	 * })
	 * ```
	 * 
	 * @param array|string $type
	 * @param array|null $action
	 * @return array
	 */
	public function registerEndpoint( $type = '', $action = '' ) 
	{
		$config = $type;
		if (!is_array($type)) {
			$config = [
				'type' => $type,
				'func' => $action,
			];
		}

		$this->registeredEndpoints[$config['type']] = $config;
		return $this->registeredEndpoints;
	}

	/**
	 * Zentrale Funktion, die sich um Registrieren/Delegation der AJAX-Aufrufe
	 * aus den Custom Meta Boxes kümmert.
	 * 
	 * Wird 1x beim Booten von nnhelpers registriert.
	 * Empfängt danach alle Requests (authorisiert oder nicht-authorisiert!) an:
	 * ```
	 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn
	 * ```
	 * 
	 * ```
	 * \nn\wp::Ajax()->registerAjaxHandler();
	 * ```
	 * 
	 * @return void
	 */
	public function registerAjaxHandler() 
	{
		// ToDo: Prüfen, warum hier `admin_enqueue_scripts` oder `wp_enqueue_scripts` nicht funktionieren!
		$scriptIdentifier = \nn\wp::Page()->addJsLibrary('EXT:nnhelpers/Resources/Js/backend.js', ['jquery']);
		wp_localize_script(
			$scriptIdentifier,
			'nnhelpers',
			[
				'ajax_url' 	=> admin_url( 'admin-ajax.php?action=nn' ),
				'_nonce'    => \nn\wp::Encrypt()->createNonce( __CLASS__ ),
			]
		);
	
		// Prefix `admin_post_*` ist von WP vorgegeben!
		add_action( 'wp_ajax_nopriv_nn', [$this, 'handleAjaxRequest']);
		add_action( 'wp_ajax_nn', [$this, 'handleAjaxRequest']);	
	}

	/**
	 * Nach Aufruf per AJAX: Delegation an den Controller.
	 * ```
	 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn
	 * https://www.mysite.de/wp-admin/admin-ajax.php?action=nn&type=12345
	 * ```
	 * @return void
	 */
	public function handleAjaxRequest() 
	{
		$args = \nn\wp::Request()->GP();
		$type = $args['type'] ?? false;

		$handler = $this->registeredEndpoints[$type] ?? false;
		if (!$handler) {
			header('HTTP/1.0 404 Not found');
			wp_send_json(['error'=>"\\nn\\wp::Ajax()->handleAjaxRequest(): The AJAX endpoint '{$type}' was not registered."]);
			die();
		}

		$func = $handler['func'];
		$callStatic = strpos('::', $func) !== false;
		list($className, $method) = \nn\wp::Arrays($func)->trimExplode(['::', '->']);

		// Sicherstellen, dass Klasse und Methode existieren
		if (!class_exists($className)) {
			die("\\nn\\wp::Ajax()->handleAjaxRequest(): Class '{$className}' unknown.");
		}
		if (!method_exists($className, $method)) {
			die("\\nn\\wp::Ajax()->handleAjaxRequest(): Method '{$func}' not found.");
		}

		$nonce = $args['_nonce'] ?? '';

		if ($callStatic) {
			// Statische Methode wurde angegeben `My\Ext\Class::method`
			$result = call_user_func([$className, $method], $args);
		} else {
			// Methode in Instanz wurde angegeben `My\Ext\Class->method`
			$instance = \nn\wp::injectClass( $className );
			if (method_exists($instance, 'setNonce')) {
				$instance->setNonce( $nonce, __CLASS__ );
			}
			$result = $instance->$method( $args );
		}

		wp_send_json( $result );
	}

}