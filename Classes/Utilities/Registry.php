<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * 
 */
class Registry extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Eine Wert aus der Tabelle `options` holen.
	 * ```
	 * \nn\wp::Registry()->get( 'mykey' );
	 * ```
	 * @param string $key
	 * @return string|array
	 */
	public function get ( $key = '' ) 
	{
		return json_decode(json_encode(get_option( $key )), true);
	}
	
	/**
	 * Einen Wert in der Tabelle `options` speichern.
	 * 
	 * Daten in dieser Tabelle bleiben über die Session hinaus erhalten.
	 * Ein Scheduler-Job kann z.B. speichern, wann er das letzte Mal
	 * ausgeführt wurde.
	 * 
	 * Es kann ein Array oder String gespeichert werden.
	 * ```
	 * \nn\wp::Registry()->set( 'mykey', 'test' );
	 * \nn\wp::Registry()->set( 'mykey', ['test'=>'ok'] );
	 * ```
	 * @param string $key
	 * @param string|array $settings
	 * @return string|array
	 */
	public function set ( $key = '', $settings = [] ) 
	{
		if ($this->get($key) !== false) {
			update_option( $key, $settings );
			return $settings;
		}
		add_option( $key, $settings );
		return $settings;
	}
}