<?php

namespace Nng\Nnhelpers\Utilities;

use Elementor\Plugin;

/**
 * Helper für Elementor Widgets
 * 
 */
class Elementor extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * @var boolean|array
	 */
	protected $registeredWidgets = false;

	/**
	 * 
	 */
	public function factory( $config = [] ) 
	{
		$config = \nn\wp::Arrays([
			'controller' => \Nng\Nnhelpers\Controller\ElementorController::class
		])->merge($config, false, true);

		$className = $config['controller'];
		if (!class_exists($className)) {
			return \nn\wp::Message()->ERROR('\nn\wp::Elementor()', 'Klasse ' . $className . ' nicht gefunden' );
		}

		$isFirstWidget = $this->registeredWidgets === false;
		if ($isFirstWidget) $this->registeredWidgets = [];

		$this->registeredWidgets[$className] = [
			'configuration' => $config,
		];

		$controller = \nn\wp::injectClass( $className );
		$this->registeredWidgets[$className]['instance'] = $controller;

		if ($isFirstWidget) {
			add_action( 'elementor/widgets/register', [$this, 'registerWidgets']);
		}

		return $controller;
	}
	
	/**
	 * 
	 */
	public function registerWidgets() 
	{
		foreach ($this->registeredWidgets as $widget) {
			Plugin::instance()->widgets_manager->register( $widget['instance'] );
		}
	}

	/**
	 * 
	 */
	public function getConfiguration( $instance = null ) 
	{
		if (is_object($instance)) $instance = get_class($instance);
		return $this->registeredWidgets[$instance]['configuration'] ?? [];
	}

}