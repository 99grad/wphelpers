<?php

namespace Nng\Nnhelpers\Utilities;

use Intervention\Image\ImageManagerStatic as Image;
use Nng\Nnhelpers\Domain\Model\SysFile;

/**
 *
 */
class File extends \Nng\Nnhelpers\Singleton 
{

	/**
	 * Pfad zu verkleinerten Bildern
	 * @var string
	 */
	public static $processedDir = 'CONTENT:_processed_/';

	/**
	 * Mapping von file-suffix zu Dateitypen
	 * @var array
	 */
	static $TYPES = [
		'image'		=> ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'tif', 'tiff'],
		'video'		=> ['mp4', 'webm', 'mov'],
		'document'	=> ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'ai', 'indd', 'txt'],
		'pdf'		=> ['pdf'],
	];

	/**
	 * Prüft, ob eine Datei existiert.
	 * Gibt absoluten Pfad zur Datei zurück.
	 * ```
	 * \nn\wp::File()->exists('wp-content/bild.jpg');
	 * ```
	 * @return string|boolean
	 */
	public function exists ( $src = null ) {
		if (file_exists( $src )) return $src;
		$src = $this->absPath( $src );

		if (file_exists( $src )) return $src;
		return false;
	}

	/**
	 * Holt eine Liste von Dateien aus einem Verzeichnis
	 * ```
	 * \nn\wp::File()->filesInFolder('EXT:myplugin/path/to/folder/', '*.php');
	 * ```
	 * @return array
	 */
	public function filesInFolder ( $path = null, $pattern = '*' ) 
	{
		$path = $this->absPath( $path );
		$files = [];
		foreach (glob( $path . $pattern ) as $file) {
			$filename = pathinfo( $file, PATHINFO_FILENAME );
			$files[$filename] = $file;
		}
		return $files;
	}
	
	/**
	 * Holt den Inhalt einer Datei.
	 * 
	 * ```
	 * // Rohen Text als string holen
	 * \nn\wp::File()->read('EXT:myplugin/text.txt');
	 * 
	 * // PHP-Script mit `return` (z.B. ein array) holen 
	 * \nn\wp::File()->read('EXT:myplugin/array.php');
	 * 
	 * // PHP-Script als string holen 
	 * \nn\wp::File()->read('EXT:myplugin/array.php', false);
	 * ```
	 * @return string|boolean
	 */
	public function read ( $src = null, $parseScripts = true ) 
	{
		if (!$this->exists( $src )) return '';
		$src = $this->absPath( $src );
		$suffix = $this->suffix( $src );
		if ($suffix == 'php' && $parseScripts) {
			if ($cache = \nn\wp::Cache()->get($src, true)) {
				return $cache;
			}
			$data = require( $src );
			return \nn\wp::Cache()->set($src, $data, true);			
		}
		return file_get_contents( $src );
	}
	
	/**
	 * Holt den Inhalt aller Dateien aus einem Ordner
	 * ```
	 * // Alle Dateien mit der Endung `.php` holen
	 * \nn\wp::File()->readAll('EXT:myplugin/Configuration/CPT/', '*.php');
	 * 
	 * // Script NICHT parsen – PHP-Code wird als String zurückgegeben
	 * \nn\wp::File()->readAll('EXT:myplugin/Configuration/CPT/', '*.php', false);
	 * ```
	 * @return array
	 */
	public function readAll ( $src = null, $pattern = '*', $parseScripts = true ) 
	{
		$files = $this->filesInFolder( $src, $pattern );
		foreach ($files as $name=>$file) {
			$files[$name] = $this->read( $file, $parseScripts );
		}
		return $files;
	}

	/**
	 * Absolute URL zu einer Datei generieren.
	 * Gibt den kompletten Pfad zur Datei inkl. `https://.../` zurück.
	 * 
	 * ```
	 * // => https://www.myweb.de/fileadmin/bild.jpg
	 * \nn\wp::File()->absUrl( 'fileadmin/bild.jpg' );
	 * 
	 * // => https://www.myweb.de/fileadmin/bild.jpg
	 * \nn\wp::File()->absUrl( 'https://www.myweb.de/fileadmin/bild.jpg' );
	 * 
	 * // => /var/www/vhost/somewhere/fileadmin/bild.jpg
	 * \nn\wp::File()->absUrl( 'https://www.myweb.de/fileadmin/bild.jpg' );
	 * ```
	 * 
	 * @return string
	 */
	public function absUrl( $file = null ) {
		$baseUrl = \nn\wp::Environment()->getBaseURL();
		$file = $this->resolvePathPrefixes( $file );
		$file = str_replace( $baseUrl, '', $file );
		return $baseUrl . ltrim( $file, '/' );
	}

	/**
	 * Absolute URL zu einer Datei enfernen
	 * 
	 * ```
	 * // => fileadmin/bild.jpg
	 * \nn\wp::File()->stripAbsUrl( 'https://www.myweb.de/fileadmin/bild.jpg' );
	 * ```
	 * 
	 * @return string
	 */
	public function stripAbsUrl( $url = null ) {
		$baseUrl = \nn\wp::Environment()->getBaseURL();
		$file = str_replace( $baseUrl, '', $url );
		return ltrim( $file, '/' );
	}

	/**
	 * Gibt Pfad zu Datei / Ordner OHNE absoluten Pfad.
	 * Optional kann ein Prefix angegeben werden. 
	 * 
	 * Beispiel:
	 * ```
	 * \nn\wp::File()->stripPathSite('var/www/website/fileadmin/test.jpg'); 		==>	fileadmin/test.jpg
	 * \nn\wp::File()->stripPathSite('var/www/website/fileadmin/test.jpg', true); 	==>	var/www/website/fileadmin/test.jpg
	 * \nn\wp::File()->stripPathSite('fileadmin/test.jpg', true); 					==>	var/www/website/fileadmin/test.jpg
	 * \nn\wp::File()->stripPathSite('fileadmin/test.jpg', '../../'); 				==>	../../fileadmin/test.jpg
	 * ```
	 * @return string
	 */
	public function stripPathSite( $file, $prefix = false ) {
		$pathSite = \nn\wp::Environment()->getPathSite();
		$file = str_replace($pathSite, '', $file);
		if ($prefix === true) {
			$file = $pathSite . $file;
		} else if ($prefix !== false) {
			$file = $prefix . $file;
		}
		return $file;
	}

	/**
	 * Absoluter Pfad zu einer Datei auf dem Server.
	 * 
	 * Gibt den kompletten Pfad ab der Server-Root zurück, z.B. ab `/var/www/...`.
	 * Falls der Pfad bereits absolut war, wird er unverändert zurückgegeben.
	 * 
	 * ```
	 * \nn\wp::File()->absPath('/var/www/website/wp-content/uploads/bild.jpg'); 	// => /var/www/website/wp-content/uploads/bild.jpg
	 * \nn\wp::File()->absPath('EXT:nnhelpers'); 					 				// => /var/www/website/wp-content/plugins/nnhelpers/
	 * ```
	 * 
	 * @return string
	 */
	public function absPath ( $file = null ) {		
		$file = $this->stripPathSite( $file );		
		$file = $this->stripAbsUrl( $file );
		return $this->resolvePathPrefixes( $file, true );
	}

	/**
	 * EXT: Prefix auflösen
	 * ```
	 * \nn\wp::File()->resolvePathPrefixes('EXT:nnhelpers'); 						=> /wp-content/plugins/nnhelpers/
	 * ```
	 * @return string
	 */
	public function resolvePathPrefixes ( $file = null, $absolute = false ) {
		// `EXT:extname` => `EXT:extname/`
		if (strpos($file, 'EXT:') == 0 && !pathinfo($file, PATHINFO_EXTENSION)) {
			$file = rtrim($file, '/') . '/';
		}
		
		$pluginFolder = \nn\wp::Environment()->getPathPlugins();
		$cacheFolder = \nn\wp::Environment()->getVarPath();
		$contentFolder = \nn\wp::Environment()->getContentPath();

		$file = strtr($file, [
			'EXT:'		=> $pluginFolder,
			'PLUGIN:'	=> $pluginFolder,
			'CACHE:'	=> $cacheFolder,
			'TMP:'		=> $cacheFolder,
			'CONTENT:'	=> $contentFolder,
		]);

		$pathSite = \nn\wp::Environment()->getPathSite();
		$file = str_replace( $pathSite, '', $file );

		if (!$absolute) {
			return $file;
		}

		return $pathSite . $file;
	}

	/**
	 * Löst ../../-Angaben in Pfad auf.
	 * Funktioniert sowohl mit existierenden Pfaden (per realpath) als auch
	 * nicht-existierenden Pfaden.
	 * ```
	 * \nn\wp::File()->normalizePath( 'fileadmin/test/../bild.jpg' );		=>	fileadmin/bild.jpg
	 * ```
	 * @return string	
	 */
	public function normalizePath( $path ) {
		$hasTrailingSlash = substr($path,-1) == '/';
		$hasStartingSlash = substr($path,0,1) == '/';

		$path = array_reduce(explode('/', $path), function($a, $b) {
			if ($a === 0 || $a === null) $a = '/';
			if ($b === '' || $b === '.') return $a;
			if ($b === '..') return dirname($a);
			return preg_replace('/\/+/', '/', "{$a}/{$b}");
		}, 0);

		if (!$hasStartingSlash) $path = ltrim( $path, '/');
		$isFolder = is_dir( $path ) || $hasTrailingSlash;
		$path = rtrim( $path, '/' );
		if ($isFolder) $path .= '/';
		return $path;
	}

	/**
	 * Einen Ordner anlegen
	 * ```
	 * \nn\wp::File()->mkdir( 'wp-content/mein/ordner/' );
	 * \nn\wp::File()->mkdir( 'EXT:myplugin/mein/ordner/' );
	 * ```
	 * @return boolean
	 */
	public function mkdir( $path = '' ) {
		if ($this->exists($path)) return true;
		$path = $this->absPath(rtrim($path, '/').'/');
		wp_mkdir_p( $path );
		return $this->exists( $path );
	}

	/**
	 * Gibt an, ob die Datei in ein Bild konvertiert werden kann
	 * ```
	 * \nn\wp::File()->isConvertableToImage('bild.jpg');	=> gibt true zurück
	 * \nn\wp::File()->isConvertableToImage('text.ppt');	=> gibt false zurück
	 * ```
	 * @return boolean
	 */
	public function isConvertableToImage ( $filename = null ) {
		if (!$filename) return false;
		$suffix = $this->suffix($filename);
		$arr = array_merge(self::$TYPES['image'], self::$TYPES['pdf']);
		return in_array($suffix, $arr);
	}

	/**
	 * Gibt die Art der Datei anhand des Datei-Suffixes zurück
	 * ```
	 * \nn\wp::File()->type('bild.jpg');	=> gibt 'image' zurück
	 * \nn\wp::File()->type('text.doc');	=> gibt 'document' zurück
	 * ```
	 * @return string
	 */
	// filetype
	public function type ( $filename = null ) {
		if (!$filename) return false;
		$suffix = $this->suffix($filename);
		foreach (self::$TYPES as $k=>$arr) {
			if (in_array($suffix, $arr)) return $k; 
		}
		return 'other';
	}

	/**
	 * Gibt den Suffix der Datei zurück
	 * ```
	 * \nn\wp::File()->suffix('bild.jpg');	=> gibt 'jpg' zurück
	 * ```
	 * @return string
	 */
	public function suffix ( $filename = null ) {
		if (!$filename) return false;
		$suffix = strtolower(pathinfo( $filename, PATHINFO_EXTENSION ));
		if ($suffix == 'jpeg') $suffix = 'jpg';
		return $suffix;
	}

	/**
	 * Ersetzt den suffix für einen Dateinamen.
	 * ```
	 * \nn\wp::File()->suffix('bild', 'jpg');				//	=> bild.jpg
	 * \nn\wp::File()->suffix('bild.png', 'jpg');			//	=> bild.jpg
	 * \nn\wp::File()->suffix('pfad/zu/bild.png', 'jpg');	//	=> pfad/zu/bild.jpg
	 * ```
	 * @return string
	 */
	public function addSuffix ( $filename = null, $newSuffix = '' ) {
		$suffix = strtolower(pathinfo( $filename, PATHINFO_EXTENSION ));
		if ($suffix) {
			$filename = substr($filename, 0, -strlen($suffix)-1);
		}
		return $filename . '.' . $newSuffix;
	}
	
	/**
	 * Gibt den Suffix für einen bestimmten Mime-Type / Content-Type zurück.
	 * Sehr reduzierte Variante – nur wenige Typen abgedeckt.
	 * Umfangreiche Version: https://bit.ly/3B9KrNA
	 * ```
	 * \nn\wp::File()->suffixForMimeType('image/jpeg');	=> gibt 'jpg' zurück
	 * ```
	 * @return string
	 */
	public function suffixForMimeType ( $mime = '' ) {
		$mime = array_pop(explode('/', strtolower($mime)));
		$map = [
			'jpeg' 	=> 'jpg',
			'jpg' 	=> 'jpg',
			'gif' 	=> 'gif',
			'png' 	=> 'png',
			'pdf' 	=> 'pdf',
			'tiff' 	=> 'tif',
		];
		foreach ($map as $sword=>$suffix) {
			if (strpos($mime, $sword) !== false) {
				return $suffix;
			}
		}
		return $mime;
	}

	/**
	 * Bild manipulieren / croppen
	 * ```
	 * \nn\wp::File()->process( 'path/to/file/portrait.jpg', ['maxWidth'=>200] );
	 * \nn\wp::File()->process( 'EXT:myplugin/images/portrait.jpg', ['maxWidth'=>200] );
	 * \nn\wp::File()->process( $sysFile, ['maxWidth'=>200] );
	 * \nn\wp::File()->process( $fileId, ['maxWidth'=>200] );
	 * \nn\wp::File()->process( $fileId, ['maxWidth'=>200, 'cropVariant'=>'square'] );
	 * \nn\wp::File()->process( $fileId, ['maxWidth'=>200, 'crop'=>['x'=>0.1, 'y'=>0, 'width'=>0.5, 'height'=>1]] );
	 * ```
	 * @param integer|SysFile|string $fileId
	 * @param array $processing
	 * @return mixed
	 */
	public function process( $sysFile = '', $processing = [], $returnObj = false ) {

		$opts = ['width', 'height', 'maxWidth', 'maxHeight', 'cropVariant', 'crop'];
		$needsManipulation = false;
		$manipulation = [];

		foreach ($opts as $k) {
			$val = $processing[$k] ?? null;
			if ($val) $needsManipulation = true;
			$manipulation[$k] = $val;
		}

		if (!$sysFile) return false;

		// `maxWidth`, `maxHeight`, `width`, `height`, `cropVariant`, `absolute`
		foreach ($processing as $k=>$v) {
			${$k} = $v;
		}

		// `123` übergeben? Posts = SysFiles ;)
		if (is_numeric($sysFile)) {
			$sysFile = \nn\wp::Fal()->getSysFileByUid( $sysFile );		
		}

		// `path/to/file.jpg` übergeben?
		else if (is_string($sysFile)) {
			$sysFile = \nn\wp::Fal()->createSysFile( $sysFile );
		}

		if (!$sysFile) return false;

		// SVGs und andere nicht-konvertierbare Dateiformate nicht weiter bearbeiten
		if (!$this->isConvertableToImage( $sysFile->filepath )) {
			$needsManipulation = false;
		}

		// Nichts zu tun? Dann ohne Bearbeitung zurückgeben
		if (!$needsManipulation) {
			return $returnObj ? $sysFile : $sysFile->publicUrl;
		}

		// Bearbeitung
		$absFilePath = $sysFile->filepath;
		if (!$absFilePath) return false;

		$relPath = \nn\wp::File()->stripPathSite($absFilePath);
		$cacheIdentifier = substr(\Nng\Nnhelpers\Utilities\Cache::getIdentifier([$relPath=>$manipulation]), 0, 10);

		// `myfilename.jpg` => `myfilename_a5a8fe123a.jpg`
		$suffix = $this->suffix( $relPath );
		$filename = substr(pathinfo( $relPath, PATHINFO_FILENAME ), 0, 36) . '_' . $cacheIdentifier . '.' . $suffix;
		
		// `myfilename_a5a8fe123a.jpg` => `var/www/mysite/wp-content/_proccessed_/a/5/myfilename_a5a8fe123a.jpg`
		$subfolder = join('/', array_slice(str_split($cacheIdentifier), 0, 2)).'/';
		$processedFolder = \nn\wp::File()->absPath( self::$processedDir ) . $subfolder;
		$absProcessedFilePath = $processedFolder . $filename;

		$this->mkdir($processedFolder);

		$srcWidth = $sysFile->width;
		$srcHeight = $sysFile->height;

		// Bild exitistiert noch nicht?
		if (!file_exists( $absProcessedFilePath )) {
			$img = Image::make( $absFilePath );

			if ($manipulation['width'] || $manipulation['height']) {

				$width = $manipulation['width'];
				$height = $manipulation['height'];

				$cropWidth = strpos($width, 'c') !== false;
				$cropHeight = strpos($height, 'c') !== false;
				
				$width = intval($width);
				$height = intval($height);
				
				if ($cropWidth) {
					$manipulation['maxHeight'] = intval($height);
					$manipulation['maxWidth'] = null;
				}
				if ($cropHeight) {
					$manipulation['maxWidth'] = intval($width);
					$manipulation['maxHeight'] = null;
				}

				$ratio = $height/$width;
				$f = min(1/$width * $srcWidth, 1/$height * $srcHeight);

				$relWidth = $width * $f;
				$relHeight = $ratio * $relWidth;
				$offsetX = ($srcWidth - $relWidth)/2;
				$offsetY = ($srcHeight - $relHeight)/2;

				//\nn\wp::debug( "src: {$srcWidth}x{$srcHeight} -- to: {$width}x{$height} -- f: {$f} -- rel: {$relWidth}x{$relHeight}" );

				$manipulation['crop'] = [
					'x' 		=> 1 / $srcWidth * $offsetX,
					'y' 		=> 1 / $srcHeight * $offsetY, 
					'width'		=> 1 / $srcWidth * $relWidth,
					'height'	=> 1 / $srcHeight * $relHeight,
				];

			}

			if ($crop = $manipulation['crop']) {
				$img->crop( 
					intval($crop['width'] * $srcWidth),
					intval($crop['height'] * $srcHeight),
					intval($crop['x'] * $srcWidth),
					intval($crop['y'] * $srcHeight)
				);
			}

			if ($manipulation['maxWidth'] || $manipulation['maxHeight']) {
				$img->resize( $manipulation['maxWidth'], $manipulation['maxHeight'], function ($constraint) {
					$constraint->aspectRatio();
				});	
			}

			$img->save( $absProcessedFilePath );
		}
		
		$sysFile->processedFilepath = $absProcessedFilePath;
		$sysFile->publicUrl = $this->absUrl( $absProcessedFilePath );
		$sysFile->updateSize();

		if ($returnObj) {
			return $sysFile;
		}

		return $sysFile->publicUrl;
	}

}