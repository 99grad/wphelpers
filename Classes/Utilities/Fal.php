<?php

namespace Nng\Nnhelpers\Utilities;

use Nng\Nnhelpers\Domain\Model\SysFile;

/**
 * WordPress hat eigentlich keine FAL – wir adaptieren das Konzept
 * hier aber für WordPress.
 * 
 * SysFiles werden in WordPress einfach als Posts gespeichert.
 * 
 */
class Fal extends \Nng\Nnhelpers\Singleton 
{
	/**
	 * Eine SysFile anhand der UID holen 
	 * ```
	 * \nn\wp::Fal()->getSysFileByUid( 123 );
	 * ```
	 * @param integer $uid
	 * @return SysFile
	 */
	public function getSysFileByUid( $fileId = null ) 
	{
		$post = \nn\wp::Post()->getWithMeta($fileId);
		$publicUrl = $post['guid'] ?? false;
		if (!$publicUrl) return false;

		/*
		$image_meta = wp_get_attachment_metadata( $fileId );
		\nn\wp::debug( $image_meta );
		//*/
		$absPath = \nn\wp::File()->absPath( $publicUrl );		
		$sysFile = \nn\wp::injectClass( \Nng\Nnhelpers\Domain\Model\SysFile::class );
		$sysFile->ID = $fileId;
		$sysFile->publicUrl = $publicUrl;
		$sysFile->filepath = $absPath;
		$sysFile->processedFilepath = $absPath;
		$sysFile->alt = $post['_meta']['_wp_attachment_image_alt'][0] ?? '';
		$sysFile->title = $post['post_title'];
		$sysFile->description = $post['post_content'] ?: $post['post_excerpt'];
		$sysFile->mime_type = $post['post_mime_type'];
		$sysFile->originalResource = $post;
		
		$sysFile->updateSize();

		if (wp_lazy_loading_enabled('img', 'wp_get_attachment_image')) {
            $sysFile->lazy = wp_get_loading_attr_default('wp_get_attachment_image');
        }

		return $sysFile;
	}

	/**
	 * Eine SysFile anhand eines Datei-Pfades erzeugen 
	 * ```
	 * \nn\wp::Fal()->createSysFile( 'path/to/file.jpg' );
	 * \nn\wp::Fal()->createSysFile( 'EXT:myext/path/to/file.jpg' );
	 * ```
	 * @param string $file
	 * @return SysFile
	 */
	public function createSysFile( $file = '' ) 
	{
		$file = \nn\wp::File()->absPath($file);
		if (!file_exists($file)) return false;
		
		$publicUrl = \nn\wp::File()->absUrl($file);
		$sysFile = \nn\wp::injectClass( \Nng\Nnhelpers\Domain\Model\SysFile::class );
		$sysFile->publicUrl = $publicUrl;
		$sysFile->filepath = $file;

		if (\nn\wp::File()->isConvertableToImage($file)) {
			$sysFile->updateSize();
		}

		return $sysFile;
	}
	
	/**
	 * Eine SysFile (in der Datenbank-Tabelle `post`) anhand eines Datei-Pfades erzeugen 
	 * ```
	 * \nn\wp::Fal()->fromFile( 'path/to/file.jpg' );
	 * \nn\wp::Fal()->fromFile( 'EXT:myext/path/to/file.jpg' );
	 * ```
	 * @param string $file
	 * @return SysFile
	 */
	public function fromFile( $file = '', $attachToPostID = 0 ) 
	{
		$file = \nn\wp::File()->absPath($file);
		if (!file_exists($file)) return false;
	
		$absUrl = \nn\wp::File()->absUrl($file);
		if ($exists = \nn\wp::Db()->findOneByValues('posts', ['guid'=>$absUrl])) {
			return $this->getSysFileByUid($exists['ID']);
		}

		$filetype = wp_check_filetype( basename( $file ), null );
		$wp_upload_dir = wp_upload_dir();
		
		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => \nn\wp::File()->absUrl($file), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);
		
		$attach_id = wp_insert_attachment( $attachment, $file, $attachToPostID );
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		
		if ($attachToPostID) {
			set_post_thumbnail( $attachToPostID, $attach_id );
		}

		return $this->getSysFileByUid($attach_id);
	}

}