<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Vereinfacht die Verwendung der FlashMessages.
 * 
 * Im Backend: FlashMessages werden automatisch ganz oben ausgegeben
 * ```
 * \nn\wp::Message()->OK('Titel', 'Infotext');
 * \nn\wp::Message()->ERROR('Titel', 'Infotext');
 * ```
 */
class Message extends \Nng\Nnhelpers\Singleton {    

	/**
	 * Gibt eine "OK" Flash-Message aus
	 * ```
	 * \nn\wp::Message()->OK('Titel', 'Infotext');
	 * ```
	 * @return void
	 */
    public function OK( $title = '', $text = '' ) {
		$this->flash( $title, $text, 'success' );
	}

	/**
	 * Gibt eine "ERROR" Flash-Message aus
	 * ```
	 * \nn\wp::Message()->ERROR('Titel', 'Infotext');
	 * ```
	 * @return void
	 */
    public function ERROR( $title = '', $text = '' ) {
		$this->flash( $title, $text, 'error' );
	}
	
	/**
	 * Gibt eine "WARNING" Flash-Message aus
	 * ```
	 * \nn\wp::Message()->WARNING('Titel', 'Infotext');
	 * ```
	 * @return void
	 */
    public function WARNING( $title = '', $text = '' ) {
		$this->flash( $title, $text, 'warning' );
	}

	/**
	 * 
	 * @return void
	 */
    public function flash( $title = '', $text = '', $type = 'success', $queueID = null ) {
		add_action( 'admin_notices', function () use ($title, $text, $type ) {
			echo '
				<div class="notice notice-' . $type . ' is-dismissible">
					<p><b>' . $title . '</b><br>' . $text . '</p>
				</div>
			';
		});
	}

}