<?php

namespace Nng\Nnhelpers\Utilities;

/**
 *
 */
class Post extends \Nng\Nnhelpers\Singleton 
{   
	/**
	 * Einen Post anhand seiner `ID` holen.
	 * ```
	 * \nn\wp::Post()->get( $post_ID );
	 * \nn\wp::Post()->get( [$post_ID, post_ID] );
	 * \nn\wp::Post()->get( $post_ID, true );
	 * ```
	 * @param string|integer|array $post_ID
	 * @param boolean $includeMeta
	 * @return string
	 */
	public function get( $post_ID = null, $includeMeta = false ) 
	{
		if (!$post_ID) return [];
		$returnArray = is_array($post_ID);

		if (!$returnArray) $post_ID = [$post_ID];
		$posts = [];

		foreach ($post_ID as $ID) {
			$post = get_post( $ID, \ARRAY_A ) ?: [];
			if (!$post) continue;
			if ($includeMeta) {
				$postMeta = get_post_meta( $ID ) ?: [];
				$post['_meta'] = $postMeta;
			}
			$posts[] = $post;
		}
		return $returnArray ? $posts : array_shift($posts);
	}
	
	/**
	 * Meta-Daten eines Post anhand der `post.ID` holen.
	 * ```
	 * \nn\wp::Post()->getMeta( $post_ID );
	 * ```
	 * @param string|integer $post_ID
	 * @param boolean $includeMeta
	 * @return string
	 */
	public function getMeta( $post_ID = null ) 
	{
		$postMeta = get_post_meta( $post_ID ) ?: [];
		return $postMeta;
	}

	/**
	 * Einen Post inkl. Meta-Daten holen.
	 * Alias to `\nn\wp::Post()->get( $ID , true )`
	 * ```
	 * \nn\wp::Post()->getWithMeta( $post_ID );
	 * \nn\wp::Post()->getWithMeta( $post_ID, true );
	 * ```
	 * @param string|integer $post_ID
	 * @return string
	 */
	public function getWithMeta( $post_ID = null ) 
	{
		return $this->get( $post_ID, true );
	}
	
	/**
	 * URL zu einem Post holen.
	 * ```
	 * \nn\wp::Post()->getLink( $post_ID );
	 * \nn\wp::Post()->getLink( $wp_post );
	 * \nn\wp::Post()->getLink( ['post_ID'] );
	 * ```
	 * @param integer $postID
	 * @return string
	 */
	public function getLink( $postID = null ) 
	{
		if (is_array($postID)) {
			$postID = $postID['ID'] ?? $postID['post_ID'];
		}
		$url = get_permalink($postID);
        return $url;
	}

	/**
	 * ALLE Post anhand eines bestimmten `post_type` holen.
	 * ```
	 * \nn\wp::Post()->getAll( $type, $includeMeta );
	 * ```
	 * @param string $type
	 * @param boolean $includeMeta
	 * @return string
	 */
	public function getAll( $type = 'post', $includeMeta = false ) 
	{
		$posts = \nn\wp::Db()->findByValues('posts', ['post_type'=>$type, 'post_status'=>'publish']);
		if ($includeMeta) {
			foreach ($posts as &$post) {
				$postMeta = get_post_meta( $posts['ID'] ) ?: [];
				$post['_meta'] = $postMeta;
			}
		}
		return $posts;
	}

}