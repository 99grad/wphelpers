<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Fehler und Exceptions ausgeben
 */
class Errors extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Eine Exception werfen mit Backtrace
	 * ```
	 * \nn\wp::Errors()->Exception('Damn', 1234);
	 * ```
	 * Ist ein Alias zu:
	 * ```
	 * \nn\wp::Exception('Damn', 1234);
	 * ```
	 * @return void
	 */
    public function Exception ( $message, $code = null ) {
		if (!$code) $code = time();
		throw new \Exception( $message, $code );
	}
	
	/**
	 * Einen Error werfen mit Backtrace
	 * ```
	 * \nn\wp::Errors()->Error('Damn', 1234);
	 * ```
	 * Ist ein Alias zu:
	 * ```
	 * \nn\wp::Error('Damn', 1234);
	 * ```
	 * @return void
	 */
    public function Error ( $message, $code = null ) {
		if (!$code) $code = time();
		throw new \Error( $message, $code );
	}

}