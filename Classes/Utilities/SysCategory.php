<?php

namespace Nng\Nnhelpers\Utilities;

/**
 *
 */
class SysCategory extends \Nng\Nnhelpers\Singleton {

	/**
	 * Liste aller Kategorien holen
	 * ```
	 * \nn\wp::SysCategory()->findAll();
	 * ```
	 * 
	 * Weitere Optionen:
	 * ```
	 * // Nur Kategorien des Typs `post` holen:
	 * \nn\wp::SysCategory()->findAll('post');
	 * 
	 * // Nur Kategorien holen, die auch Beiträge als Relationen haben:
	 * \nn\wp::SysCategory()->findAll( false );
	 * 
	 * // Nur Kategorien holen, die vom Typ `post` sind und nur wenn sie auch Beiträge als Relationen haben:
	 * \nn\wp::SysCategory()->findAll('post', false);
	 * ```
	 * @return array
	 */
	public function findAll ( $type = '', $includeEmpty = true ) {

		if ($type === false) {
			$includeEmpty = false;
		}
		if ($cache = \nn\wp::Cache()->get( [__METHOD__, $type, $includeEmpty], true )) {
			return $cache;
		}
		$args = [
			'hide_empty'	=> $includeEmpty ? 0 : 1,
			'orderby'		=> 'name',
			'order'			=> 'ASC'
		];
		if ($type) {
			$args['taxonomy'] = $type;
		}

		$categories = get_terms( $args );

		foreach ($categories as &$cat) {
			$cat = $cat->to_array();
		}
		
		$categoryUids = array_column($categories,'term_id');
		if (!$categoryUids) return [];

		if ($metaData = \nn\wp::Db()->findByValues('termmeta', ['term_id'=>$categoryUids])) {
			foreach ($categories as &$cat) {
				$uid = $cat['term_id'];
				foreach ($metaData as &$row) {
					if ($row['term_id'] == $uid) {
						$key = $row['meta_key'];
						$cat[$key] = $row['meta_value'];
					}
				}
			}
		}

		if ($options = \nn\wp::Registry()->get('wplp_category_image')) {
			$optionsByTermId = \nn\wp::Arrays( $options )->key( 'term_id' )->toArray();
			foreach ($categories as &$cat) {
				$uid = $cat['term_id'];
				$cat['wplp_category_image'] = $optionsByTermId[$uid]['image'] ?? [];
			}
		}

		return \nn\wp::Cache()->set( [__METHOD__, $type, $includeEmpty], $categories, true );
	}
	
	/**
	 * Liste aller sys_categories holen, `uid` als Key zurückgeben
	 * ```
	 * \nn\wp::SysCategory()->findAllByUid();
	 * ```
	 * @return array
	 */
	public function findAllByUid ( $branchUid = null ) {

		$allCategories = $this->findAll( $branchUid );
		$allCategoriesByUid = [];
		foreach ($allCategories as $cat) {
			$allCategoriesByUid[$cat['term_id']] = $cat;
		}
		return $allCategoriesByUid;

	}

	/**
	 * sys_categories anhand von uid(s) holen.
	 * ```
	 * \nn\wp::SysCategory()->findByUid( 12 );
	 * \nn\wp::SysCategory()->findByUid( '12,11,5' );
	 * \nn\wp::SysCategory()->findByUid( [12, 11, 5] );
	 * ```
	 * @return array
	 */
	public function findByUid( $uidList = null ) {

		$returnFirst = !is_array($uidList) && is_numeric($uidList);
		$uidList = \nn\wp::Arrays($uidList)->intExplode();
		$allCategoriesByUid = $this->findAllByUid();
		$result = [];
		foreach ($uidList as $uid) {
			if ($cat = $allCategoriesByUid[$uid]) {
				$result[$uid] = $cat;
			}
		}
		return $returnFirst ? array_shift($result) : $result;

	}

	/**
	 * 	Den gesamten SysCategory-Baum (als Array) holen.
	 * 	Jeder Knotenpunkt hat die Attribute 'parent' und 'children', um
	 * 	rekursiv durch Baum iterieren zu können.
	 *	```
	 *	\nn\wp::SysCategory()->getTree();
	 *	\nn\wp::SysCategory()->getTree( $uid );
	 *	```
	 * 	ToDo: Prüfen, ob Caching sinnvoll ist
	 * 
	 * 	@return array
	 */
	public function getTree ( $branchUid = null, $type = null ) 
	{
		// Alle Kategorien laden
		$allCategories = $this->findAll( $type );
		
		// Array mit uid als Key erstellen
		$categoriesByUid = [0=>['children'=>[]]];
		foreach ($allCategories as &$sysCategory) {			
			$sysCategory['children'] = [];
			$sysCategory['_parent'] = [];
			$categoriesByUid[$sysCategory['term_id']] = $sysCategory;
		}

		// Baum generieren
		foreach ($categoriesByUid as $uid=>$sysCatArray) {
			$parentUid = $sysCatArray['parent'] ?? 0;
			$parent = &$categoriesByUid[$parentUid];
			$categoriesByUid[$uid]['_parent'] = $parent;
			if ($parentUid == 0 && $uid == 0) continue;
			$parent['children'][$uid] = &$categoriesByUid[$uid];
			//unset($categoriesByUid[$uid]);
		}
		
		// Wurzel
		$root = $categoriesByUid[0]['children'] ?: [];

		// Ganzen Baum – oder nur bestimmten Branch zurückgeben?
		if (!$branchUid) return $root;
		return $root[$branchUid] ?: [];
	}

	/**
	 * UIDs aller children eines Kategorie-Baums holen.
	 * Rekursive Methode, liefert eine flache Liste / Array mit
	 * der UID der ChildNodes als Key und dem Node als Value.
	 * 
	 * ```
	 * \nn\wp::SysCategory()->flatten( $tree );
	 * \nn\wp::SysCategory()->flatten( [10 => ['name'=>'Eins'], 20 => ['name'=>'Zwei']] );
	 * \nn\wp::SysCategory()->flatten( ['name'=>'Eins', 'children'=>[...]] );
	 * ```
	 * @return array
	 */
	public function flatten( $nodeArr = [], &$result = [], $maxDepth = 5 ) 
	{
		$maxDepth--;
		if ($maxDepth == 0) return $result;

		if ($children = $nodeArr['children'] ?? false) {
			if ($uid = $nodeArr['term_id'] ?? false) {
				$result[$uid] = $nodeArr;
			}
			$nodeArr = $children;
		}

		foreach ($nodeArr as $uid=>$node) {
			$result[$uid] = $node;
			if ($node['children'] ?? false) {
				$this->flatten( $node['children'], $result, $maxDepth );
			} 
		}

		return $result;
	}
}
