<?php

namespace Nng\Nnhelpers\Utilities;

/**
 *
 */
class Environment extends \Nng\Nnhelpers\Singleton 
{

	/**
	 *  Gibt die baseUrl zurück, inkl. http(s) Protokoll z.B. https://www.webseite.de/
	 *	```
	 *	\nn\wp::Environment()->getBaseURL();
	 *	```
	 * 	@return string
	 */
	public function getBaseURL () {
		if ($baseUrl = get_home_url()) return $baseUrl.'/';
		$server = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}/";
		return $server;
	}

	/**
	 * Absoluten Pfad zu dem `/wp-content/cache`-Verzeichnis von WordPress holen.
	 * Dieses Verzeichnis speichert WordPress temporäre Cache-Dateien.
	 * 
	 * ```
	 * // /full/path/to/wp-content/cache/
	 * $path = \nn\t3::Environment()->getVarPath();
	 * ```
	 */
	public function getVarPath() {
		return $this->getPathSite() . 'wp-content/cache/';
	}
	
	/**
	 * Absoluten Pfad zu dem `/wp-content/`-Verzeichnis von WordPress holen.
	 * 
	 * ```
	 * // /full/path/to/wp-content/
	 * $path = \nn\t3::Environment()->getContentPath();
	 * ```
	 */
	public function getContentPath() {
		if (defined('WP_CONTENT_DIR')) {
			return WP_CONTENT_DIR . '/';
		}
		return $this->getPathSite() . 'wp-content/';
	}

	/**
	 * Absoluten Pfad zum WordPress-Root-Verzeichnis holen. z.B. `/var/www/website/`
	 * ```
	 * \nn\wp::Environment()->getPathSite()
	 * ```
	 * @return string
	 */
	public function getPathSite ( $file = null ) {
		return \ABSPATH;
	}

	/**
	 * Absoluten Pfad zum WordPress-Plugin-Verzeichnis holen. z.B. `/var/www/website/wp-content/plugins/`
	 * ```
	 * \nn\wp::Environment()->getPathPlugins()
	 * ```
	 * @return string
	 */
	public function getPathPlugins () {
		return \WP_PLUGIN_DIR . '/';
	}
	
	/**
	 * Prüft, ob sich der User im Backend befindet, sprich: Die WordPress-Seite über `/wp-admin` bearbeitet.
	 * Gibt nur `TRUE` zurück, wenn sich der User wirklich in der Admin-Oberfläche des Backend befindet, nicht
	 * im Frontend bei eingeloggtem Backend-User.
	 * ```
	 * \nn\wp::Environment()->isBackend()
	 * ```
	 * @return boolean
	 */
	public function isBackend () {
		$url = $_SERVER['REQUEST_URI'] ?? '';
		if (stripos($url, 'wp-admin/') !== false) return true;
		return is_admin() && defined( 'DOING_AJAX' ) && \DOING_AJAX;
	}
	
	/**
	 * Prüft, ob sich der User im Frontend befindet, sprich: Nicht im Verzeichnis `/wp-admin`.
	 * ```
	 * \nn\wp::Environment()->isFrontend()
	 * ```
	 * @return boolean
	 */
	public function isFrontend () {
		return !$this->isBackend();
	}

}