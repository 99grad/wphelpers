<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * 
 */
class Settings extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Extension-Konfiguration holen.
	 * 
	 * Da in WordPress so ziemlich alles in der Option-Tabelle gespeichert wird,
	 * nutzen wir hier den speziellen Prefix `extconf_`, damit Konflikte vermieden
	 * werden. 
	 * ```
	 * \nn\wp::Settings()->getExtConf( 'extname' );
	 * ```
	 * @return mixed
	 */
	public function getExtConf( $extName = '' ) 
	{
		return \nn\wp::Registry()->get( 'extconf_' . $extName ) ?: [];
	}

	/**
	 * Extension-Konfiguration schreiben.
	 * ```
	 * \nn\wp::Settings()->setExtConf( 'extname', ['key'=>'value', ...] );
	 * \nn\wp::Settings()->setExtConf( 'extname', 'key', 'value' );
	 * ```
	 * @return mixed
	 */
	public function setExtConf( $extName = '', $key = '', $value = '' ) 
	{
		$conf = $this->getExtConf( $extName ) ?: [];

		if (is_string($key)) {
			$update = [];
			$update[$key] = $value;
		} else {
			$update = $key;
		}
		$conf = array_merge($conf, $update);
		unset($conf['nonce']);

		return \nn\wp::Registry()->set( 'extconf_' . $extName, $conf );
	}

	/**
	 * Variable von einem gegebenen Pfad holen
	 * ```
	 * \nn\t3::Settings()->getFromPath('L', \nn\t3::Request()->GP());
	 * \nn\t3::Settings()->getFromPath('a.b', ['a'=>['b'=>1]]);
	 * ```
	 * Existiert auch als ViewHelper:
	 * ```
	 * {nnt3:ts.setup(path:'pfad.zur.setup')}
	 * ```
	 * @return array
	 */
	public function getFromPath( $tsPath = '', $setup = null ) 
	{
		$parts = \nn\wp::Arrays($tsPath)->trimExplode('.');
		$setup = $setup ?: [];

		$root = array_shift($parts);
		$plugin = array_shift($parts);

		$setup = $setup[$root] ?? [];
		if (!$plugin) return $setup;

		$setup = $setup[$plugin] ?? [];
		if (!count($parts)) return $setup;

		while (count($parts) > 0) {
			$part = array_shift($parts);
			if (count($parts) == 0) {
				return isset($setup[$part]) && is_array($setup[$part]) ? $setup[$part] : ($setup[$part] ?? ''); 
			}
			$setup = $setup[$part];
		}

		return $setup;
	}
}