<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Einfache Weiterleitungen machen, URLs bauen
 */
class Http extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Zu einer Seite weiterleiten
	 * ```
	 * \nn\wp::Http()->redirect( 'https://www.99grad.de' );
	 * \nn\wp::Http()->redirect( 10 );										// => path/to/pageId10
	 * \nn\wp::Http()->redirect( 10, ['test'=>'123'] );						// => path/to/pageId10&test=123
	 * \nn\wp::Http()->redirect( 10, ['test'=>'123'], 'tx_myext_plugin' );	// => path/to/pageId10&tx_myext_plugin[test]=123
	 * ```
	 * @return void
	 */
    public function redirect ( $pidOrUrl = null, $vars = [], $varsPrefix = '' ) {

		if (!$varsPrefix) unset($vars['id']);

		if ($varsPrefix) {
			$tmp = [$varsPrefix=>[]];
			foreach ($vars as $k=>$v) $tmp[$varsPrefix][$k] = $v;
			$vars = $tmp;
		}
		
		$link = $this->buildUri( $pidOrUrl, $vars, true );
		wp_safe_redirect($link); 
		exit();
	}

	/**
	 * URI bauen, funktioniert im Frontend und Backend-Context.
	 * Berücksichtigt realURL
	 * ```
	 * \nn\wp::Http()->buildUri( 123 );
	 * \nn\wp::Http()->buildUri( 123, ['test'=>1], true );
	 * ```
	 * @return string
	 */
	public function buildUri ( $pageUid, $vars = [], $absolute = false ) {

		// Keine pid übergeben? Dann selbst ermitteln zu aktueller Seite
		if (!$pageUid) $pageUid = \nn\wp::Page()->getPid();

		// String statt pid übergeben? Dann Request per PHP bauen
		if (!is_numeric($pageUid)) 
		{	
			// relativer Pfad übergeben z.B. `/link/zu/seite`
			if (strpos($pageUid, 'http') === false) {
				$pageUid = \nn\wp::Environment()->getBaseURL() . ltrim( $pageUid, '/' );
			}
		
			$parsedUrl = parse_url($pageUid);

			parse_str($parsedUrl['query'], $parsedParams);
			if (!$parsedParams) $parsedParams = [];

			$parsedParams = \nn\wp::Arrays( $parsedParams )->merge( $vars );
			$reqStr = $parsedParams ? http_build_query( $parsedParams ) : false;

			$path = $parsedUrl['path'] ?: '/';
			return "{$parsedUrl['scheme']}://{$parsedUrl['host']}{$path}" . ($reqStr ? '?'.$reqStr : '');
		}

		$uri = '?id=' . $pageUid;

		// setAbsoluteUri( TRUE ) geht nicht immer...
		if ($absolute) {
			$uri = \nn\wp::File()->absPath($uri);
		}

		return $uri;
	}

}