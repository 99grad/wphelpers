<?php

namespace Nng\Nnhelpers\Utilities;

use \wpdb;

/**
 *
 */
class Db extends \Nng\Nnhelpers\Singleton 
{

	private $queryBuilder = null;

	/**
	 * Default name of columns from table `wp_posts`
	 * @var array
	 */
	static $defaultPostColumnNames = ['ID', 'post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_title', 'post_excerpt', 'post_status', 'comment_status', 'ping_status', 'post_password', 'post_name', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt', 'post_content_filtered', 'post_parent', 'guid', 'menu_order', 'post_type', 'post_mime_type', 'comment_count'];

	/**
	 * Primary keys for the standard tables
	 * @var array
	 */
	static $primaryKeys = [
		'posts' 	=> 'ID',
		'postmeta' 	=> 'meta_id',
		'terms' 	=> 'term_id',
		'termmeta' 	=> 'meta_id',
		'users' 	=> 'ID',
		'usermeta' 	=> 'umeta_id',
		'options' 	=> 'option_id',
		'term_relationships' => 'object_id',
	];

	/**
	 * Constructor
	 * 
	 */
	public function __construct() {
		global $wpdb;
		$this->queryBuilder = $wpdb;
		return $this;
	}

	/**
	 * Verbindet mit einer anderen Datenbank
	 * ```
	 * \nn\wp::Db()->connect( $user, $password, 'my_db', 'localhost');
	 * ```
	 * @return wpdb
	 */
	public function connect( $username = '', $password = '', $db = '', $host = 'localhost' ) {
		$instance = new \Nng\Nnhelpers\Utilities\Db();
		$instance->queryBuilder = new \wpdb($username, $password, $db, $host);
		return $instance;
	}
	
	/**
	 * Holt den WordPress QueryBuilder
	 * ```
	 * \nn\wp::Db()->getQueryBuilder( $table );
	 * ```
	 * @return wpdb
	 */
	public function getQueryBuilder( $table = '' ) {
		return $this->queryBuilder;
	}
	
	/**
	 * Holt den WordPress Tabellen-Namen inkl. Prefix
	 * ```
	 * \nn\wp::Db()->getTableName( 'posts' );	// => 'Z7cLyas_posts'
	 * ```
	 * @return string
	 */
	public function getTableName( $table = '' ) {
		return $this->queryBuilder->prefix . $this->quote($table);
	}
	
	/**
	 * Den PRIMARY KEY für eine Tabelle holen
	 * ```
	 * \nn\wp::Db()->getPrimaryKey( 'posts' );		// => 'ID'
	 * \nn\wp::Db()->getPrimaryKey( 'postmeta' );	// => 'meta_id'
	 * ```
	 * @return string
	 */
	public function getPrimaryKey( $table = '' ) {
		return self::$primaryKeys[$table] ?? 'ID';
	}

	/**
	 * Findet einen Eintrag anhand der UID.
	 * ```
	 * \nn\t3::Db()->findByUid('posts', 12);
	 * \nn\t3::Db()->findByUid('posts', 12, true);
	 * ```
	 * @param string $table
	 * @param int $uid
	 * @return array
	 */
	public function findByUid( $table = '', $uid = null ) 
	{
		$key = $this->getPrimaryKey( $table );
		$rows = $this->findByValues( $table, [$key => $uid] );
		return $rows ? array_shift($rows) : [];
	}

	/**
	 * Holt ALLE Eintrag aus einer Datenbank-Tabelle.
	 * ```
	 * // Alle Datensätze holen.
	 * \nn\wp::Db()->findAll('termmeta');
	 * ```
	 * @return array
	 */
	public function findAll( $table = '' ) {
		$table = $this->getTableName($table);
		$queryBuilder = $this->getQueryBuilder();
		return $queryBuilder->get_results('SELECT * FROM ' . $table . ' WHERE 1', \ARRAY_A);
	}
	
	/**
	 * Findet EINEN Eintrag anhand von gewünschten Feld-Werten.
	 * ```
	 * // SELECT * FROM posts WHERE post_title = 'test'
	 * \nn\t3::Db()->findOneByValues('posts', ['post_title'=>'test']);
	 * 
	 * // SELECT * FROM posts WHERE firstname = 'david' AND username = 'john'
	 * \nn\t3::Db()->findOneByValues('posts', ['firstname'=>'david', 'username'=>'john']); 
	 * ```
	 * @param string $table
	 * @param array $whereArr
	 * @return array
	 */
	public function findOneByValues( $table = null, $whereArr = [] ) 
	{
		$result = $this->findByValues( $table, $whereArr );
		return $result ? array_shift($result) : []; 
	}

	/**
	 * Holt Einträge aus einer Datenbank-Tabelle.
	 * ```
	 * // Alle Datensätze holen.
	 * \nn\wp::Db()->findByValues('posts', 12);
	 * \nn\wp::Db()->findByValues('posts', ['ID'=>12]);
	 * \nn\wp::Db()->findByValues('posts', ['ID'=>[12, 13, 14]]);
	 * \nn\wp::Db()->findByValues('posts', ['ID'=>[12, 13, 14]]); 
	 * ```
	 * @return array
	 */
	public function findByValues( $table = NULL, $whereArr = [], $ignoreEnableFields = false ) 
	{
		$table = $this->getTableName($table);
		$queryBuilder = $this->getQueryBuilder();
		
		if (is_numeric($whereArr)) {
			$whereArr = ['ID'=>$whereArr];
		}
		$constraints = [];
		foreach ($whereArr as $k=>$v) {
			$k = $this->quote($k);
			$v = $this->fullQuote($v);
			if (is_array($v)) {
				$constraints[] = "{$k} IN (" . join(',', $v) . ")";
			} else {
				$constraints[] = "{$k}={$v}";
			}
		}
		$where = join(' AND ', $constraints);
		return $queryBuilder->get_results('SELECT * FROM ' . $table . ' WHERE ' . $where, \ARRAY_A);
	}
	
	/**
	 * Holt Einträge aus einer Datenbank-Tabelle über LIKE.
	 * ``` 
	 * // Mit LIKE:
	 * \nn\wp::Db()->findLike('posts', ['title'=>'%test%', 'post_type'=>'post']);
	 * 
	 * // ... WHERE (title LIKE '%test%' AND title LIKE '%nice%') AND (post_type = 'post')
	 * \nn\wp::Db()->findLike('posts', ['title'=>['%test%', '%nice%'], 'post_type'=>'post']);
	 * 
	 * // ... WHERE (title LIKE '%test%' OR title LIKE '%nice%') AND (post_type = 'post')
	 * \nn\wp::Db()->findLike('posts', ['title'=>['%test%', '%nice%'], 'post_type'=>'post'], $useOr );
	 * ```
	 * @return array
	 */
	public function findLike( $table = NULL, $whereArr = [], $useOr = false ) 
	{
		$join = $useOr ? 'OR' : 'AND';
		$queryBuilder = $this->getQueryBuilder();
		
		$constraints = [];
		foreach ($whereArr as $k=>$v) {
			$k = $this->quote($k);
			$v = $this->fullQuote($v);

			if (!is_array($v)) $v = [$v];
			$subConstraint = [];
			foreach ($v as $n) {
				if (strpos($n, '%') === false) {
					$subConstraint[] = "{$k}={$n}";
				} else {
					$subConstraint[] = "{$k} LIKE {$n}";
				}
			}
			$constraints[] = '(' . join(' ' .$join. ' ', $subConstraint) . ')';
		}

		if ($hiddenConstraint = $this->getHiddenConstraint( $table )) {
			$constraints[] = $hiddenConstraint;
		}

		$where = join(' AND ', $constraints);
		$table = $this->getTableName($table);
		
		$query = 'SELECT * FROM ' . $table . ' WHERE ' . $where;
		//echo $query;die();
		return $queryBuilder->get_results($query, \ARRAY_A);
	}

	/**
	 * Holt die Sichtbarkeits-Restriktion für eine Tabelle, damit gelöschte oder
	 * unveröffentlichte Datensätze nicht erscheinen.
	 *  
	 */
	public function getHiddenConstraint( $table ) 
	{
		switch ($table) {
			case 'posts':
				return "post_status = 'publish'";
		}
		return '';
	}

	/**
	 * Eine “rohe” Query an die Datenbank absetzen. Näher an der Datenbank geht nicht. 
	 * Du bist für alles selbst verantwortlich. Injections steht nur Deine (hoffentlich ausreichende :) 
	 * Intelligenz entgegen.
	 * ```
	 * // Variablen IMMER über escapen!
	 * $keyword = \nn\t3::Db()->quote('suchbegriff');
	 * $rows = \nn\t3::Db()->statement( "SELECT FROM posts WHERE bodytext LIKE '%${keyword}%'");
	 * 
	 * // oder besser gleich prepared statements verwenden:
	 * $rows = \nn\t3::Db()->statement( 'SELECT FROM posts WHERE bodytext LIKE :str', ['str'=>"%${keyword}%"] );;
	 * ```
	 * @return mixed
	 */
	public function statement( $statement = '', $params = [] ) {
		$queryBuilder = $this->getQueryBuilder();
		if ($params) {
			$statement = $queryBuilder->prepare( $statement, $params );
		}
		return $queryBuilder->get_results( $statement, \ARRAY_A );
	}
	
	/**
	 * Escape eines Strings (oder eines Arrays) für Queries.
	 * Gibt die Werte OHNE umschließende Anführungszeichen zurück.
	 * ```
	 * $str = \nn\wp::Db()->quote('test');
	 * $str = \nn\wp::Db()->quote([1,2,3]);
	 * ```
	 * @param array|string $strOrArray
	 * @return string|array
	 */
	public function quote( $strOrArray = '', $fullQuote = false ) 
	{
		if (is_array($strOrArray)) {
			foreach ($strOrArray as &$v) {
				$v = $this->quote( $v, $fullQuote );
			}
			return $strOrArray;
		}
		$str = str_replace('%', '[#]', $strOrArray);
		$str = esc_sql( $str );
		$str = str_replace('[#]', '%', $strOrArray);
		if ($fullQuote && !is_numeric($str)) {
			$str = "'{$str}'";
		}
		return $str;
	}
	
	/**
	 * Escape eines Strings (oder eines Arrays) für Queries.
	 * Gibt die Werte MIT umschließenden Anführungszeichen zurück.
	 * ```
	 * $str = \nn\wp::Db()->fullQuote('te"st');  	// => 'te\"st'
	 * $str = \nn\wp::Db()->fullQuote([1,2,'a"3']);	// => [1,2,'a\"']
	 * ```
	 * @param array|string $strOrArray
	 * @return string|array
	 */
	public function fullQuote( $strOrArray = '' ) 
	{
		return $this->quote( $strOrArray, true );
	}

	/**
	 * Parsed eine `ext_tables.sql`-Datei und erzeugt die Tabellen in
	 * der WordPress-Datenbank.
	 * ```
	 * \nn\wp::Db()->createTablesFromExtTablesFile('EXT:myplugin/ext_tables.sql');
	 * ```
	 * @param string $path
	 * @return boolean|array
	 */
	public function createTablesFromExtTablesFile( $path = '' ) 
	{
		$sql = \nn\wp::File()->read( $path );
		if (!$sql) return false;

		// strip comments
		$sql = preg_replace('/(#(.*))/m', '', $sql);

		// prefix used in DB for this WP-installation
		$tablePrefix = $this->queryBuilder->prefix;
		
		// create an array with all tables to create. Only needed as return value
		$createdTables = [];
		preg_match_all('/(CREATE TABLE )(.*)( \()/m', $sql, $matches);
		foreach ($matches[2] as $match) {
			$createdTables[$match] = [
				'tableName' => $tablePrefix . $match,
			];
		}

		// prefix tablename in statement with global DB-prefix
		$sql = preg_replace('/(CREATE TABLE )(.*)( \()/m', "\\1{$tablePrefix}\\2\\3", $sql);

		// add charset
		$charset = $this->queryBuilder->get_charset_collate();
		$statements = explode('CREATE TABLE', trim($sql));
		array_shift($statements);
		foreach ($statements as &$statement) {
			$statement = trim('CREATE TABLE' . $statement);
			$lastBracket = strrpos($statement, ')');
			$statement = substr($statement, 0, $lastBracket+1) . ' ' . $charset . substr($statement, $lastBracket+1, strlen($statement));
		}

		// Additional rules.
		foreach ($statements as &$statement) {
			// remove whitespaces
			$statement = preg_replace(["/(\s+)\n/", "/(\n+)/"], "\n", $statement);
			
			// "You must have two spaces between the words PRIMARY KEY and the definition of your primary key."
			$statement = preg_replace('/PRIMARY KEY(\s+)\(/', 'PRIMARY KEY  (', $statement);

			// check: https://codex.wordpress.org/Creating_Tables_with_Plugins
		}
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		// execute
		foreach ($statements as &$statement) {
			dbDelta( $statement );
		}
		
		return $createdTables;
	}

	/**
	 * Instanz des Repositories für ein Model (oder einen Model-Klassennamen) holen.
	 * ```
	 * \nn\wp::Db()->getRepositoryForModel( \My\Domain\Model\Name::class );
	 * \nn\wp::Db()->getRepositoryForModel( $myModel );
	 * ```
	 * @param mixed $className
	 * @return mixed
	 */
	public function getRepositoryForModel( $className = null, $returnRepoInstance = true ) 
	{
		if (!is_string($className)) $className = get_class($className);
		$repositoryName = str_replace(
			['\\Domain\\Model', '_Domain_Model_'],
			['\\Domain\\Repository', '_Domain_Repository_'],
			$className
		) . 'Repository';
		if (!$returnRepoInstance) return $repositoryName;
		return \nn\wp::injectClass( $repositoryName );
	}
	
	/**
	 * Instanz des Models für ein Repository (oder einen Repository-Klassennamen) holen.
	 * ```
	 * \nn\wp::Db()->getModelForRepository( \My\Domain\Repository\Name::class );
	 * \nn\wp::Db()->getModelForRepository( $myRepository );
	 * ```
	 * @param mixed $className
	 * @return mixed
	 */
	public function getModelForRepository( $className = null, $returnModelInstance = true ) 
	{
		if (!is_string($className)) $className = get_class($className);
		$modelName = preg_replace(
			['/\\\\Domain\\\\Repository/', '/_Domain_Repository_/', '/Repository$/'],
			['\\Domain\\Model', '_Domain_Model_', ''],
			$className
		);
		if (!$returnModelInstance) return $modelName;
		return \nn\wp::injectClass( $modelName );
	}

	/**
	 * Datenbank-Eintrag einfügen. Simpel und idiotensicher.
	 * 
	 * ```
	 * $insertArr = \nn\t3::Db()->insert('table', ['bodytext'=>'...']);
	 * ```
	 * 
	 * @param mixed $table
	 * @param array $data
	 * @return mixed 
	 */
    public function insert ( $table = '', $data = [] ) 
	{
		$prefixedTable = $this->getTableName( $table );
		$queryBuilder = $this->getQueryBuilder();
		if ($queryBuilder->insert( $prefixedTable, $data )) {
			$ID = $queryBuilder->insert_id;
			return $this->findByUid( $table, $ID );
		}
		return false;
	}

	/**
	 * Datenbank-Eintrag aktualisieren. Schnell und einfach.
	 * 
	 * Beispiele:
	 * ```
	 * // UPDATES table SET title='new' WHERE uid=1 
	 * \nn\t3::Db()->update('table', ['title'=>'new'], 1);
	 * 
	 * // UPDATE table SET title='new' WHERE email='david@99grad.de' AND pid=12
	 * \nn\t3::Db()->update('table', ['title'=>'new'], ['email'=>'david@99grad.de', 'pid'=>12, ...]);
	 * ```
	 * 
	 * Mit `true` statt einer `$uid` werden ALLE Datensätze der Tabelle geupdated.
	 * ```
	 * // UPDATE table SET test='1' WHERE 1
	 * \nn\t3::Db()->update('table', ['test'=>1], true);
	 * ```
	 * 
	 * @param mixed $table
	 * @param array $data
	 * @param int $uid
	 * @return mixed
	 */
    public function update ( $table = '', $data = [], $uid = null ) 
	{	
		$prefixedTable = $this->getTableName( $table );
		$queryBuilder = $this->getQueryBuilder();

		if (is_numeric($uid)) {
			$uid = ['ID'=>$uid];
		}
		if (!is_array($uid) && $uid !== true) {
			\nn\wp::Exception( '\nn\t3::Db()->update() - keine ID übergeben!' );
			return '';	
		}
		return $queryBuilder->update( $prefixedTable, $data, $uid );
	}
}