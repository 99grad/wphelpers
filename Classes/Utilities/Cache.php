<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * 	Methoden, zum Lesen und Schreiben in den Cache.
 * 
 */
class Cache extends \Nng\Nnhelpers\Singleton {
	
	/**
	 * Lädt Inhalt des Caches anhand eines Identifiers.
	 * Der Identifier ist ein beliebiger String oder ein Array, der den Cache eindeutif Identifiziert.
	 * ```
	 * \nn\wp::Cache()->get('myid');
	 * \nn\wp::Cache()->get(['pid'=>1, 'uid'=>'7']);
	 * \nn\wp::Cache()->get(['func'=>__METHOD__, 'uid'=>'17']);
	 * ```
	 * 
	 * @param mixed $identifier	String oder Array zum Identifizieren des Cache
	 * @param mixed $useRamCache	temporärer Cache in $GLOBALS statt Caching-Framework
	 * 
	 * @return mixed
	 */
	public function get( $identifier = '', $useRamCache = false ) {
		$identifier = self::getIdentifier( $identifier );

		// Ram-Cache verwenden? Einfache globale.
		if ($useRamCache && ($cache = $GLOBALS['nnhelpers_cache'][$identifier] ?? false)) {
			return $cache;
		}		
		return false;
	}

	/**
	 * Schreibt einen Eintrag in den Cache.
	 * Der Identifier ist ein beliebiger String oder ein Array, der den Cache eindeutif Identifiziert.
	 * ```
	 * // Klassische Anwendung im Controller: Cache holen und setzen
	 * if ($cache = \nn\wp::Cache()->get('myid')) return $cache;
	 * ...
	 * $cache = $this->view->render();
	 * return \nn\wp::Cache()->set('myid', $cache);
	 * ```
	 * 
	 * ```
	 * // RAM-Cache verwenden? TRUE als dritter Parameter setzen
	 * \nn\wp::Cache()->set('myid', $dataToCache, true);
	 * 
	 * // Dauer des Cache auf 60 Minuten festlegen
	 * \nn\wp::Cache()->set('myid', $dataToCache, 3600);
	 * 
	 * // Als key kann auch ein Array angegeben werden
	 * \nn\wp::Cache()->set(['pid'=>1, 'uid'=>'7'], $html);
	 * ````
	 * @param mixed $indentifier	String oder Array zum Identifizieren des Cache
	 * @param mixed $data			Daten, die in den Cache geschrieben werden sollen. (String oder Array)
	 * @param mixed $useRamCache	`true`: temporärer Cache in $GLOBALS statt Caching-Framework. 
	 * 								`integer`: Wie viele Sekunden cachen?
	 *
	 * @return mixed
	 */
	public function set( $identifier = '', $data = null, $useRamCache = false ) {

		$identifier = self::getIdentifier( $identifier );
		$lifetime = 86400;
		
		if ($useRamCache === true) {
			if (!isset($GLOBALS['nnhelpers_cache'])) {
				$GLOBALS['nnhelpers_cache'] = [];
			}
			return $GLOBALS['nnhelpers_cache'][$identifier] = $data;
		}
	}

	/**
	 * Wandelt übergebenen Cache-Identifier in brauchbaren String um.
	 * Kann auch ein Array als Identifier verarbeiten.
	 * 
	 * @param mixed $indentifier
	 * @return string
	 */
	public static function getIdentifier( $identifier = null ) {
		if (is_array($identifier)) {
			$identifier = json_encode($identifier);
		}
		return md5($identifier);
	}
	
	/**
	 * Löscht den Seiten-Cache. Alias zu `\nn\wp::Page()->clearCache()`
	 * ```
	 * \nn\wp::Cache()->clearPageCache( 17 );		// Seiten-Cache für pid=17 löschen
	 * \nn\wp::Cache()->clearPageCache();			// Cache ALLER Seiten löschen
	 * ```
	 *
	 * @param mixed $pid 	pid der Seite, deren Cache gelöscht werden soll oder leer lassen für alle Seite
	 * @return void
	 */
	public function clearPageCache( $pid = null ) {
		//return \nn\wp::Page()->clearCache( $pid );
	}
	
	/**
	 * Löscht alle `nnhelpers`-Caches:
	 * 
	 * - RAM-Caches
	 * - CachingFramework-Caches, die per `\nn\wp::Cache()->set()` gesetzt wurde
	 * - Datei-Caches, die per `\nn\wp::Cache()->write()` gesetzt wurde
	 * 
	 * ```
	 * \nn\wp::Cache()->clear();
	 * ```
	 *
	 * @return void
	 */
	public function clear() {

		// $this->cacheManager->flushCaches();

		// RAM Cache löschen
		$GLOBALS['nnhelpers_cache'] = [];

		// File-Cache löschen
		// $files = \nn\wp::Environment()->getVarPath() . "/cache/code/nnhelpers/*.php";
		// foreach (glob($files) as $file) {
		// 	unlink( $file );
		// }
	}
	
	/**
	 * Statischen Datei-Cache schreiben.
	 * 
	 * Schreibt eine PHP-Datei, die per `$cache = require('...')` geladen werden kann.
	 * 
	 * Angelehnt an viele Core-Funktionen und Extensions (z.B. mask), die statische PHP-Dateien
	 * ins Filesystem legen, um performancelastige Prozesse wie Klassenpfade, Annotation-Parsing etc.
	 * besser zu cachen. Nutzt bewußt __nicht__ die Core-Funktionen, um jeglichen Overhead zu
	 * vermeiden und größtmögliche Kompatibilität bei Core-Updates zu gewährleisten.
	 * 
	 * ```
	 * $cache = ['a'=>1, 'b'=>2];
	 * $identifier = 'myid';
	 * 
	 * \nn\wp::Cache()->write( $identifier, $cache );
	 * $read = \nn\wp::Cache()->read( $identifier );
	 * ```
	 * Das Beispiel oben generiert eine PHP-Datei mit diesem Inhalt:
	 * ```
	 * <?php
	 * return ['_' => ['a'=>1, 'b'=>2]];
	 * ```
	 *
	 * @return string|array
	 */
	public function write( $identifier, $cache ) {
		/*
		$this->set( $identifier, $cache, true );

		$identifier = self::getIdentifier( $identifier );
		$phpCode = '<?php return ' . var_export(['_' => $cache], true) . ';';

		$path = \nn\wp::Environment()->getVarPath() . "cache/code/nnhelpers/{$identifier}.php";
		\TYPO3\CMS\Core\Utility\GeneralUtility::writeFileToTypo3tempDir( $path, $phpCode );

		return $cache;
		*/
	}

	/**
	 * Statischen Datei-Cache lesen.
	 * 
	 * Liest die PHP-Datei, die per `\nn\wp::Cache()->write()` geschrieben wurde.
	 * ```
	 * $cache = \nn\wp::Cache()->read( $identifier );
	 * ```
	 * Die PHP-Datei ist ein ausführbares PHP-Script mit dem `return`-Befehl. 
	 * Sie speichert den Cache-Inhalt in einem Array.
	 * ```
	 * <?php
	 * 	return ['_'=>...];
	 * ```
	 * 
	 * @return string|array
	 */
	public function read( $identifier ) {
		/*
		if ($cache = $this->get( $identifier, true )) return $cache;
		$identifier = self::getIdentifier( $identifier );
		$path = \nn\wp::Environment()->getVarPath() . "/cache/code/nnhelpers/{$identifier}.php";
		
		if (!file_exists($path)) {
			return null;
		}

		$cache = require( $path );
		return $cache['_'];
		*/
	}

	
}