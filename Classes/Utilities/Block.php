<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Helper für Blocks und Shortcodes
 * 
 * 
 */
class Block {
   
	protected $name = '';
	protected $forms = [];
	protected $config = [];

	/**
	 * 
	 */
	public function factory( $config = [] ) 
	{
		$config = \nn\wp::Arrays([
			'controller' => \Nng\Nnhelpers\Controller\BlockController::class
		])->merge($config, false, true);

		$controller = \nn\wp::injectClass( $config['controller'] ?? false );
		if (!$controller) {
			return \nn\wp::Message()->ERROR('\nn\wp::Block()', 'Klasse ' . $config['controller'] . ' nicht gefunden' );
		}
		$controller->configure( $config );
		return $controller;
	}
}