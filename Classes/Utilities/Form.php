<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Rendern von Formularen
 * 
 */
class Form extends \Nng\Nnhelpers\Singleton {

	const ALIGNMENTS = [
		['Left', 'left', 'eicon-text-align-left'],
		['Center', 'center', 'eicon-text-align-center'],
		['Right', 'right', 'eicon-text-align-right'],
		['Justified', 'justify', 'eicon-text-align-justify'],
	];

	const TAGS = [
		['H1', 'h1'],
		['H2', 'h2'],
		['H3', 'h3'],
		['H4', 'h4'],
		['H5', 'h5'],
		['H6', 'h6'],
		['div', 'div'],
		['span', 'span'],
		['p', 'p'],
	];
	
	/**
	 * Fügt die JS und CSS Dateien für das Backend ein
	 * ```
	 * \nn\wp::Form()->addFormJsCss();
	 * ```
	 * @return void
	 */
	public function addFormJsCss() 
	{
		// CSS und JS Dateien für die Formulare einbinden
		\nn\wp::Page()->addCssLibrary('EXT:nnhelpers/Resources/Libraries/vendor/nng/nn/dist/nn.min.css');
		\nn\wp::Page()->addCssLibrary('EXT:nnhelpers/Resources/Css/backend.css');

		\nn\wp::Page()->addJsLibrary(['nn'=>'EXT:nnhelpers/Resources/Libraries/vendor/nng/nn/dist/nn.min.js']);
		\nn\wp::Page()->addJsLibrary('EXT:nnhelpers/Resources/Js/backend.forms.js', ['nn']);
	}

	/**
	 * Rendert ein Formular.
	 * ```
	 * // Als $path kann der Pfad zum Formular-Konfig-Array angegeben werden
	 * \nn\wp::Form()->render( 'EXT:myplugin/Configuration/CPT/myformdata.php', $vars );
	 * 
	 * // ... oder das Array selbst
	 * \nn\wp::Form()->render( ['ctrl'=>[...], 'columns'=>'...'], $vars );
	 * 
	 * // Eigenes Template zum Rendern verwenden?
	 * \nn\wp::Form()->render( ['ctrl'=>[...], 'columns'=>'...'], $vars, 'EXT:Path/To/FormTemplate.html' );
	 * \nn\wp::Form()->render( ['ctrl'=>[...], 'columns'=>'...'], $vars, 'Form/Template' );
	 * 
	 * // ... oder mit Angabe der Pfade zu den Templates, Layouts, Partials
	 * \nn\wp::Form()->render( ['ctrl'=>[...], 'columns'=>'...'], $vars, ['template'=>'Form/Template', 'formNamePrefix'=>'my_prefix', 'templateRootPaths'=>[...]] );
	 * ```
	 * @param string|array $path
	 * @param array $gp
	 * @param array $options
	 * @return string
	 */
	public function render ( $path = '', $gp = [], $options = [] )
	{
		$this->addFormJsCss();

		if (is_string($options)) {
			$options = [
				'template' 	=> $options,
			];
		}

		if (!is_array($path)) {
			$file = \nn\wp::File()->absPath( $path );
			$config = require_once( $file );
		} else {
			$config = $path;
			$path = json_encode($path);
		}
		$uid = md5($path);

		$options = array_merge([
			'action' 			=> '',
			'template' 			=> 'Form/DefaultForm',
			'templateRootPaths'	=> ['EXT:nnhelpers/Resources/Templates/'],
			'partialRootPaths'	=> ['EXT:nnhelpers/Resources/Partials/'],
			'formNamePrefix'	=> '_nnhelpers_cpt['.$uid.']',
		], $options);

		$vars = [
			'nonce' 			=> $options['nonce'] ?? \nn\wp::Encrypt()->createNonce( 'nnhelpers' . $uid ),
			'gp' 				=> $gp,
			'config' 			=> $config,
			'formNamePrefix' 	=> $options['formNamePrefix'],
			'action' 			=> $options['action'],
			'returnUrl' 		=> \nn\wp::File()->absUrl(\nn\wp::Request()->getUri()),
		];

		$html = \nn\wp::Template()->render($options['template'], $vars, $options);
		return $html;
	}

	/**
	 * Rendert die Felder für eigene Felder in einem CPT (Custom Post Type)
	 * Dieses Formular hat kein `<form>`-Element um die Felder
	 * ```
	 * \nn\wp::Form()->renderCptFields( 'EXT:myplugin/Configuration/CPT/myformdata.php', $vars );
	 * \nn\wp::Form()->renderCptFields( ['ctrl'=>[...], 'columns'=>'...'], $vars );
	 * ```
	 * @param string|array $path
	 * @param array $gp
	 * @param array $options
	 * @return string
	 */
	public function renderCptFields( $path = '', $gp = [], $options = [] ) 
	{
		$options['template'] = 'Form/CustomPostTypeFields';
		return $this->render( $path, $gp, $options );
	}

}