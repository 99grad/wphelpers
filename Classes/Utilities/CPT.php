<?php

namespace Nng\Nnhelpers\Utilities;

use \nn\wp\GeneralUtility;

/**
 * Helper für Custom Post Types (CPT)
 * 
 * Erlaubt das einfache einfügen einen neuen Post-Types inkl. der Formulare
 * mit Eingabefeldern, Checkboxen etc.
 * 
 * Typischer Einsatz in eigener Extension:
 * ```
 * \nn\wp::CPT()
 * 		->addPostType('nnaddress', [...])
 * 		->addForm('Titel', 'EXT:myplugin/pfad/zum/formulararray.php')
 * 		->addForm(...);
 * 
 * \nn\wp::CPT()
 * 		->getPostType('page')
 * 		->addForm('Titel', 'EXT:myplugin/pfad/zum/formulararray.php');
 * ```
 * 
 */
class CPT extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * @var array
	 */
	protected $registeredPostTypes = [];

	/**
	 * @var array
	 */
	protected $registeredPostTypesByModel = [];

	/**
	 * This method is called from `AbstractApp->parseCustomPostTypes()`.
	 * It registers a custom post type (CPT) and caches the configuration.
	 * 
	 * @param array $config
	 * @return \Nng\Nnhelpers\Controller\AbstractCptController
	 */
	public function factory( $config = [] ) 
	{
		$config = \nn\wp::Arrays([
			'controller' => \Nng\Nnhelpers\Controller\CptController::class
		])->merge($config, false, true);
		
		$controller = \nn\wp::injectClass( $config['controller'] ?? false );
		if (!$controller) {
			return \nn\wp::Message()->ERROR('\nn\wp::CPT()', 'Klasse ' . $config['controller'] . ' nicht gefunden' );
		}
		$controller->configure( $config );

		$this->registeredPostTypes[$config['identifier']] = $config;
		$this->registeredPostTypesByModel[$config['modelClassName']] = &$this->registeredPostTypes[$config['identifier']];

		return $controller;
	}

	/**
	 * Holt das Konfigurations-Array für ein CPT-Model
	 * 
	 * ```
	 * \nn\wp::CPT()->getConfigurationArrayForModel( \Nng\Nnhelpers\Domain\Model\CPT\MyModel::class );
	 * ```
	 * @param string $model
	 * @return array
	 */
	public function getConfigurationArrayForModel( $model = '' ) 
	{
		if ( $cache = $this->registeredPostTypesByModel[$model] ?? false ) {
			return $cache;
		}

		$model = ltrim( $model, '\\');
		preg_match('/(.*)\\\(.*)\\\Domain\\\Model\\\CPT\\\(.*)/i', $model, $matches);

		if (!$matches[3]) return [];

		$extKey = GeneralUtility::camelCaseToLowerCaseUnderscored( $matches[2] );
		$postType = GeneralUtility::camelCaseToLowerCaseUnderscored( $matches[3] );
		$path = 'EXT:' . $extKey . '/Configuration/CPT/' . $postType . '.php';

		$config = \nn\wp::File()->read( $path );
		if (!$config) return [];

		$config['extKey'] = $extKey;
		$config['postType'] = $postType;
		if (!$config['identifier']) {
			$config['identifier'] = $extKey . '_' . $postType;
		}
		return $config;
	}

	/**
	 * Holt den CPT-Model-Namen für einen post_type
	 * 
	 * ```
	 * \nn\wp::CPT()->getModelForPostType( 'nnaddress_address' );
	 * ```
	 * @param string $postType
	 * @return string
	 */
	public function getModelForPostType( $postType = '' ) 
	{
		return $this->registeredPostTypes[$postType]['modelClassName'] ?? false;
	}

	/**
	 * ALLE Post anhand eines bestimmten `post_type` holen.
	 * Alias zu `\nn\wp::Post()->getAll( $type, $includeMeta )`
	 * ```
	 * \nn\wp::Post()->getAll( $type, $includeMeta );
	 * ```
	 * @param string $type
	 * @param boolean $includeMeta
	 * @return string
	 */
	public function getAll( $type = 'post', $includeMeta = false ) 
	{
		return \nn\wp::Post()->getAll( $type, $includeMeta );
	}
}