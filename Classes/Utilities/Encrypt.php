<?php

namespace Nng\Nnhelpers\Utilities;

/**
 * Verschlüsseln und Hashen von Passworten
 */
class Encrypt extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * Verschlüsselungsalgorithmen
	 * 
	 */
	const ENCRYPTION_METHOD = 'aes-256-cbc';
	const ENCRYPTION_HMAC 	= 'sha3-512';

	/**
	 * Erzeugt eine nonce zum Schutz von Formularen.
	 * ```
	 * $nonce = \nn\wp::Encrypt()->createNonce( __CLASS__ );
	 * $valid = \nn\wp::Encrypt()->verifyNonce( $nonce, 'someid' );
	 * 
	 * $nonce = \nn\wp::Encrypt()->createNonce( 'someid' );
	 * $valid = \nn\wp::Encrypt()->verifyNonce( $nonce, __CLASS__ );
	 * ```
	 * @param string $key
	 * @return string
	 */
	public function createNonce( $key = '' ) 
	{
		$saltingKey = $this->getSaltingKey();
		return wp_create_nonce( $saltingKey . $key . date('-Ymd') );
	}

	/**
	 * Prüft, ob die gegebene nonce gültig ist.
	 * ```
	 * \nn\wp::Encrypt()->verifyNonce( $nonce, 'someid' );
	 * ```
	 * @param string $nonce
	 * @param string $key
	 * @return string
	 */
	public function verifyNonce( $nonce = '', $key = '' ) 
	{
		$saltingKey = $this->getSaltingKey();
		return wp_verify_nonce( $nonce, $saltingKey . $key . date('-Ymd') );
	}

	/**
	 * Holt den Enryption / Salting Key aus der Extension Konfiguration für `nnhelpers`.
	 * Falls im Extension Manager noch kein Key gesetzt wurde, wird er automatisch generiert
	 * und in der `LocalConfiguration.php` gespeichert.
	 * ```
	 * \nn\wp::Encrypt()->getSaltingKey();
	 * ```
	 * @return string
	 */
	public function getSaltingKey() {
		if ($key = \nn\wp::Settings()->getExtConf('nnhelpers')['saltingKey'] ?? false) {
			return $key;
		}

		$key = base64_encode(json_encode([
			base64_encode(openssl_random_pseudo_bytes(32)), 
			base64_encode(openssl_random_pseudo_bytes(64))
		]));

		if (!\nn\wp::Settings()->setExtConf( 'nnhelpers', 'saltingKey', $key)) {
			\nn\wp::Exception('Please first set the encryption key in the Extension-Manager for nnhelpers!');
		}
		return $key;
	}

	/**
	 * Verschlüsselt einen String oder ein Array.
	 * 
	 * Im Gegensatz zu `\nn\wp::Encrypt()->hash()` kann ein verschlüsselter Wert per `\nn\wp::Encrypt()->decode()`
	 * wieder entschlüsselt werden. Diese Methods eignet sich daher __nicht__, um sensible Daten wie z.B. Passworte 
	 * in einer Datenbank zu speichern. Dennoch ist der Schutz relativ hoch, da selbst identische Daten, die mit
	 * dem gleichen Salting-Key verschlüsselt wurden, unterschiedlich aussehen.
	 * 
	 * Für die Verschlüsselung wird ein Salting Key generiert und in dem Extension Manager von `nnhelpers` gespeichert.
	 * Dieser Key ist für jede Installation einmalig. Wird er verändert, dann können bereits verschlüsselte Daten nicht
	 * wieder entschlüsselt werden.
	 * ```
	 * \nn\wp::Encrypt()->encode( 'mySecretSomething' );
	 * \nn\wp::Encrypt()->encode( ['some'=>'secret'] );
	 * ```
	 * Komplettes Beispiel mit Verschlüsselung und Entschlüsselung:
	 * ```
	 * $encryptedResult = \nn\wp::Encrypt()->encode( ['password'=>'mysecretsomething'] );
	 * echo \nn\wp::Encrypt()->decode( $encryptedResult )['password'];
	 * 
	 * $encryptedResult = \nn\wp::Encrypt()->encode( 'some_secret_phrase' );
	 * echo \nn\wp::Encrypt()->decode( $encryptedResult );
	 * ```
	 * @return string
	 */
	public function encode( $data = '' ) {
		[$key1, $key2] = json_decode(base64_decode( $this->getSaltingKey() ), true);

		$data = json_encode(['_'=>$data]);
			
		$method = self::ENCRYPTION_METHOD;   
		$iv_length = openssl_cipher_iv_length($method);
		$iv = openssl_random_pseudo_bytes($iv_length);
			
		$first_encrypted = openssl_encrypt($data, $method, base64_decode($key1), OPENSSL_RAW_DATA, $iv);   
		$second_encrypted = hash_hmac(self::ENCRYPTION_HMAC, $first_encrypted, base64_decode($key2), TRUE);
				
		$output = base64_encode($iv . $second_encrypted . $first_encrypted);   
		return $output;   
	}

	/**
	 * Entschlüsselt einen String oder ein Array.
	 * Zum Verschlüsseln der Daten kann `\nn\wp::Encrypt()->encode()` verwendet werden.
	 * Siehe `\nn\wp::Encrypt()->encode()` für ein komplettes Beispiel.
	 * ```
	 * \nn\wp::Encrypt()->decode( '...' );
	 * ```
	 * @return string
	 */
	public function decode( $data = '' ) {

		[$key1, $key2] = json_decode(base64_decode( $this->getSaltingKey() ), true);
		$mix = base64_decode($data);

		$method = self::ENCRYPTION_METHOD;  
		$iv_length = openssl_cipher_iv_length($method);
				
		$iv = substr($mix, 0, $iv_length);
		$second_encrypted = substr($mix, $iv_length, 64);
		$first_encrypted = substr($mix, $iv_length + 64);
	
		$data = openssl_decrypt($first_encrypted, $method, base64_decode($key1), OPENSSL_RAW_DATA, $iv);
		$second_encrypted_new = hash_hmac(self::ENCRYPTION_HMAC, $first_encrypted, base64_decode($key2), TRUE);
		
		if (hash_equals($second_encrypted, $second_encrypted_new)) {
			$data = json_decode( $data, true );
			return $data['_'] ?? null;
		}
		
		return false;
	}
	
	/**
	 * Einfaches Hashing, z.B. beim Check einer uid gegen ein Hash.
	 * 
	 * ```
	 * \nn\wp::Encrypt()->hash( $uid );
	 * ```
	 * Existiert auch als ViewHelper:
	 * ```
	 * {something->nnt3:encrypt.hash()}
	 * ```
	 * @return string
	 */
	public function hash( $string = '' ) {
		$salt = $this->getSaltingKey();
		return preg_replace('/[^a-zA-Z0-9]/', '', base64_encode( sha1("{$string}-{$salt}", true )));
	}

	/**
	 * Ein JWT (Json Web Token) erzeugen, signieren und `base64`-Encoded zurückgeben.
	 * 
	 * __Nicht vergessen:__ Ein JWT ist zwar "fälschungssicher", weil der Signatur-Hash nur mit 
	 * dem korrekten Key/Salt erzeugt werden kann – aber alle Daten im JWT sind für jeden
	 * durch `base64_decode()` einsehbar. Ein JWT eignet sich keinesfalls, um sensible Daten wie
	 * Passwörter oder Logins zu speichern!
	 * ```
	 * \nn\wp::Encrypt()->jwt(['test'=>123]);
	 * ```
	 * @param array $payload
	 * @return string
	 */
	public function jwt( $payload = [] ) {
		$header = [
			'alg' => 'HS256',
			'typ' => 'JWT',
		];
		$signature = $this->createJwtSignature($header, $payload);
		return join('.', [
			base64_encode(json_encode($header)),
			base64_encode(json_encode($payload)),
			base64_encode($signature)
		]);
	}

	/**
	 * Ein JWT (Json Web Token) parsen und die Signatur überprüfen.
	 * Falls die Signatur valide ist (und damit der Payload nicht manipuliert wurde), wird der
	 * Payload zurückgegeben. Bei ungültiger Signatur wird `FALSE` zurückgegeben.
	 * ```
	 * \nn\wp::Encrypt()->parseJwt('adhjdf.fsdfkjds.HKdfgfksfdsf');
	 * ```
	 * @param string $token
	 * @return array|false
	 */
	public function parseJwt( $token = '' ) {
		$parts = explode('.', $token);
		$header = json_decode(base64_decode( array_shift($parts)), true);
		$payload = json_decode(base64_decode( array_shift($parts)), true);
		$signature = base64_decode(array_shift($parts));
		
		$checkSignature = $this->createJwtSignature($header, $payload);
		if ($signature !== $checkSignature) return FALSE;
		$payload['token'] = $token;
		
		return $payload;
	}

	/**
	 * Signatur für ein JWT (Json Web Token) erzeugen. 
	 * Die Signatur wird später als Teil des Tokens mit vom User übertragen.
	 * ```
	 * $signature = \nn\wp::Encrypt()->createJwtSignature(['alg'=>'HS256', 'typ'=>'JWT'], ['test'=>123]);
	 * ```
	 * @param array $header
	 * @param array $payload
	 * @return string
	 */
	public function createJwtSignature( $header = [], $payload = [] ) {
		return hash_hmac(
			'sha256',
			base64_encode(json_encode($header)) . '.' . base64_encode(json_encode($payload)), 
			$this->getSaltingKey()
		);
	}

}