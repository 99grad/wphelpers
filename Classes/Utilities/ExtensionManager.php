<?php

namespace Nng\Nnhelpers\Utilities;

use \nn\wp\GeneralUtility;

/**
 * Alles, was mit Plugins zu tun hat.
 * 
 */
class ExtensionManager extends \Nng\Nnhelpers\Singleton {
   
	/**
	 * @var array
	 */
	public $registeredExtensions = [];

	/**
	 * @var array
	 */
	public $packageStates = [];

	/**
	 * @var array
	 */
	public $registeredAutoloadPaths = [];


	/**
	 * Registriert einen zentralen Autoloader für alle Extensions, die `nnhelpers`
	 * verwenden und sich per `\nn\wp::ExtensionManager()->registerExtension()` registriert haben.
	 * 
	 * @return void
	 */
	public function __construct() 
	{
		$this->packageStates = \nn\wp::Registry()->get('nnhelpers_extmanager') ?: [];

		spl_autoload_register( function($classname) {

			$parsed = GeneralUtility::parseNamespace( $classname );
			$namespace = $parsed['namespace'];

			// Vender/Namespace nicht registriert? Dann Abbruch.
			$classPath = $this->registeredAutoloadPaths[ $namespace ] ?? false;
			if (!$classPath) return;

			// \Nng\MyExt\Utility\Test --> Utility\Test
			$subpath = ltrim(str_replace($namespace, '', $classname), '\\');

			// Utility\Test --> Utility/Test.php
			$subpath = str_replace('\\', DIRECTORY_SEPARATOR, $subpath) . '.php';

			// /var/www/path/to/plugin/Classes/Utility/Test.php
			$filename = $classPath .  $subpath;

			if (file_exists( $filename) ) {
				include_once $filename;
			}
		});
	}

	/**
	 * Registriert ein Plugin, dass durch `nnhelpers` initialisiert
	 * und konfiguriert wird.
	 * ```
	 * \nn\wp::ExtensionManager()->registerExtension( '\Nng\Nnaddress', $config );
	 * ```
	 * @return self
	 */
	public function registerExtension( $namespace = '', $config = [] ) 
	{
		$parsed = GeneralUtility::parseNamespace( $namespace );
		$extKey = $parsed['extKey'];
		$pluginPath = \nn\wp::File()->absPath("EXT:{$extKey}/");
		$config = array_merge([
			'path'			=> $pluginPath,
			'pluginFile'	=> $pluginPath . $extKey . '.php',
			'vendor'		=> '',
			'extName'		=> '',
			'extKey'		=> '',
			'instance'		=> false,						// Enthält nach `beforeInitializeApps()` eine Instanz der App
			'app' 			=> "{$namespace}\\App",			// `\Nng\Myext\App::class` --> Klasse / Haupteinstiegspunkt mit der `init()` methode
			'autoload' 		=> [$namespace => 'Classes']	// `['\Nng\Myext' => 'Classes']` --> Pfad(e) für psr-4 autoload
		], $parsed, $config);

		if ($autoloadPaths = $config['autoload']) {
			$this->registerAutoloader( $autoloadPaths );
		}

		$this->registeredExtensions[$parsed['extKey']] = $config;
		return $this;
	}
	
	/**
	 * Holt die Liste der registrierten Extensions
	 * ```
	 * \nn\wp::ExtensionManager()->getRegisteredExtensions();
	 * ```
	 * @return array
	 */
	public function getRegisteredExtensions() 
	{
		return $this->registeredExtensions;
	}

	/**
	 * ```
	 * \nn\wp::ExtensionManager()->beforeInitializeApps()
	 * ```
	 * @return void
	 */
	public function beforeInitializeApps() 
	{
		$stateChanged = false;

		foreach ($this->registeredExtensions as $extKey=>$config) {
			
			if ($config['instance'] ?? false) continue;

			if ($app = \nn\wp::injectClass( $config['app'] )) {
				$app->configure( $config );
				$app->register();

				// Plugin wurde frisch aktiviert. `install()` aufrufen
				$packageState = $this->packageStates[$extKey] ?? [];
				if (!$packageState) {
					$app->install();
					
					$this->packageStates[$extKey] = $config;
					$stateChanged = true;
				}

				$this->registeredExtensions[$extKey]['instance'] = $app;
			}
		}

		// Prüfen, ob extensions DEAKTIVIERT wurden
		$inactive = array_diff(
			array_keys($this->packageStates),
			array_keys($this->registeredExtensions)
		);

		// Für jede deaktivierte Extension: Kurze "Wiederbelebung", um noch 1 x `uninstall()` aufrufen zu können
		if ($inactive) {
			foreach ($inactive as $extKey) {
				if ($config = $this->packageStates[$extKey] ?? []) {
					$this->registerAutoloader($config['autoload']);
					if ($app = \nn\wp::injectClass( $config['app'] )) {
						$app->configure( $config );
						$app->uninstall();
					}
				}
				unset($this->packageStates[$extKey]);
			}
			$stateChanged = true;
		}
		
		if ($stateChanged) {
			\nn\wp::Registry()->set('nnhelpers_extmanager', $this->packageStates);
		}
	}

	/**
	 * PSR4-autoloader registrieren
	 * ```
	 * \nn\wp::ExtensionManager()->registerAutoloader(['\Nng\Myext' => 'Classes']);
	 * ```
	 * @param array $pathArr
	 * @return self
	 */
	public function registerAutoloader ( $pathArr = [] ) 
	{
		if (!$pathArr) {
			return $this;
		}
		foreach ($pathArr as $namespace=>$folder) {
			$parsed = GeneralUtility::parseNamespace( $namespace );
			$path = \nn\wp::File()->absPath("EXT:{$parsed['extKey']}/{$folder}/");
			$this->registeredAutoloadPaths[$parsed['namespace']] = $path;
		}
		return $this;
	}

	/**
	 * 
	 * ```
	 * // default: Nutzt die `ext_conf_template.txt` im plugin-Ordner
	 * \nn\wp::ExtensionManager()->addExtConfiguration( 'myextname' );
	 * 
	 * // Pfad zu eigenem Array (oder ext_conf_template.txt)
	 * \nn\wp::ExtensionManager()->addExtConfiguration( 'myextname', 'EXT:myextname/path/to/formArray.php' );
	 * ```
	 * @return void
	 */
	public function addExtConfiguration ( $extKey = '', $config = '' ) 
	{
		if (!$config) {
			$config = "EXT:{$extKey}/ext_conf_template.txt";
		}
		if (!is_array($config)) {
			if (\nn\wp::File()->suffix($config) == 'php') {
				$config = require( \nn\wp::File()->absPath($config) );
			} else {
				$config = \nn\wp::File()->read($config);
				$config = $this->parseExtConfFormat( $config );
			}
		}
		
		$extInfo = $this->extInfo( $extKey );
		$config['ctrl']['title'] = $extInfo['Name'] ?? '';

		// Wenn admin-Menü aufgebaut wird...
		add_action( 'admin_menu', function () use ($extKey, $config) {

			// ... dann füge einen Menüpunkt in die Seitenleiste "Einstellungen" ein ...
			add_options_page( "{$extKey} Settings", "{$extKey}", 'manage_options', "{$extKey}-plugin", function () use ($config, $extKey) {

				// ... und rendere das Formular, das beim Speichern die action `admin_post_nnhelpers_save_extconf` aufruft
				$settings = \nn\wp::Settings()->getExtConf($extKey);
				echo \nn\wp::Form()->render( $config, $settings, [
					'nonce'				=> \nn\wp::Encrypt()->createNonce( __CLASS__ ),
					'formNamePrefix' 	=> 'nnhelpers',
					'action' 			=> 'nnhelpers_save_extconf',
					'template' 			=> 'Form/ExtConfigurationForm'
				]);
			});
		});

		// Prefix `admin_post_` ist von WP vorgegeben!
		add_action( 'admin_post_nnhelpers_save_extconf', function () use ($extKey) {
			$gp = \nn\wp::Request()->GP();
			$config = $gp['nnhelpers'];
			
			if (!\nn\wp::Encrypt()->verifyNonce( $config['nonce'], __CLASS__ )) {
				die('Invalid nonce value.');
			}
			
			\nn\wp::Settings()->setExtConf($extKey, $config);
			if ($returnUrl = \nn\wp::Request()->GP('returnUrl')) {
				\nn\wp::Http()->redirect($returnUrl);
			}
		});
	}

	/**
	 * Konvertiert das `ext_conf_template.txt`-Format in das Form-Array-Format
	 * ```
	 * \nn\wp::ExtensionManager()->parseExtConfFormat( $text );
	 * ```
	 * @param string $text
	 * @return array
	 */
	public function parseExtConfFormat( $text ) 
	{
		$formData = [
			'ctrl' => [
				'title'	=> 'Konfiguration',
			],
			'columns' => []
		];

		$typeToFieldMapping = [
			'boolean' 	=> 'check',
			'text' 		=> 'text',
			'string' 	=> 'input',
		];

		preg_match_all("/(#.*)\n(.*)/m", $text, $matches);
		foreach ($matches[1] ?? [] as $i=>$field) {
			$options = \nn\wp::Arrays($field)->trimExplode(';');
			$conf = [];
			foreach ($options as $option) {
				list($key, $val) = \nn\wp::Arrays($option)->trimExplode('=', false);
				$conf[$key] = $val;
			}
			list($varName, $defaultVal) =  \nn\wp::Arrays($matches[2][$i])->trimExplode('=', false);
			$type = $typeToFieldMapping[$conf['type']] ?? false;
			if (!$type) continue;
			$formData['columns'][$varName] = [
				'label' => $conf['label'],
				'config' => [
					'type' => $type,
					'default' => $defaultVal,
				],
			];
		}

		return $formData;
	}

	/**
	 * Informationen zum Plugin aus den Annotations des Plugin-Scripts holen.
	 * 
	 * ```
	 * ```
	 * @param string $extKey
	 * @return array
	 */
	public function extInfo( $extKey = '' ) 
	{
		if(!function_exists('get_plugin_data')){
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}
		return get_plugin_data( \nn\wp::File()->absPath("EXT:{$extKey}/{$extKey}.php") );
	}
}