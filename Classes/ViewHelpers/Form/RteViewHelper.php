<?php
namespace Nng\Nnhelpers\ViewHelpers\Form;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * 
 */
class RteViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('content', 'string', 'Text');
        $this->registerArgument('editorId', 'string', 'Editor ID');
        $this->registerArgument('name', 'string', 'Textfield name');
        $this->registerArgument('settings', 'array', 'Settings');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $content = $arguments['content'] ?: $renderChildrenClosure();
        $settings = $arguments['settings'] ?: [];
        $editorId = $arguments['editorId'] ?: $arguments['name'];
        $editorId = preg_replace('/[^0-9a-z]*/', '', $editorId);

        // https://developer.wordpress.org/reference/classes/_WP_Editors/parse_settings/
        if ($settings['rows'] ?? false) {
            $settings['textarea_rows'] = $settings['rows'];
        }
        if ($arguments['name'] ?? false) {
            $settings['textarea_name'] = $arguments['name'];
        }

        ob_start();
        wp_editor( $content, $editorId, $settings );
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

}
