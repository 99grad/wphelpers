<?php

namespace Nng\Nnhelpers\ViewHelpers\Form;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class SelectViewHelper extends AbstractTagBasedViewHelper
{
	/**
	 * @var string
	 */
	protected $tagName = 'select';

	/**
	 * @var mixed
	 */
	protected $selectedValue;

	/**
	 * Initialize arguments.
	 */
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerTagAttribute('name', 'string', 'Name');
		$this->registerTagAttribute('value', 'string', 'Value');
		$this->registerTagAttribute('size', 'string', 'Size of input field');
		$this->registerTagAttribute('disabled', 'string', 'Specifies that the input element should be disabled when the page loads');
		$this->registerArgument('options', 'array', 'Associative array with internal IDs as key, and the values are displayed in the select box. Can be combined with or replaced by child f:form.select.* nodes.');
		$this->registerArgument('multiple', 'boolean', 'If set multiple options may be selected.', false, false);
		$this->registerArgument('required', 'boolean', 'If set no empty value is allowed.', false, false);
	}

	/**
	 * Render the tag.
	 *
	 * @return string rendered tag.
	 */
	public function render()
	{
		$tagContent = $this->renderChildren();

		if ($this->arguments['multiple']) {
			$this->tag->addAttribute('multiple', '');
		}

		if ($this->arguments['required']) {
			$this->tag->addAttribute('required', '');
		}

		if ($this->arguments['disabled']) {
			$this->tag->addAttribute('disabled', '');
		}
		
		if ($name = $this->arguments['required']) {
			$this->tag->addAttribute('name', $name);
		}

		$options = [];
		foreach ($this->arguments['options'] as $k=>$v) {
			$options[] = "<option value=\"{$k}\">{$v}</option>";
		}

		$this->tag->forceClosingTag(true);
		$this->tag->setContent(join('', $options) . $tagContent);

		$html = $this->tag->render();
		return $html;
	}

}
