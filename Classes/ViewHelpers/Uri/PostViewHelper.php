<?php
namespace Nng\Nnhelpers\ViewHelpers\Uri;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Holt den Link zu einem Post.
 * Da Post = Beitrag = Media kann dieser ViewHelper für alles Arten von Links verwendet werden.
 * ```
 * {nn:uri.post(postId:14)}
 * ```
 */
class PostViewHelper extends AbstractViewHelper
{
	use CompileWithRenderStatic;

	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('postId', 'integer', 'UID of the post to link to');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
		$postId = $arguments['postId'] ?: $renderChildrenClosure();
		return \nn\wp::Post()->getLink( $postId );
	}

}
