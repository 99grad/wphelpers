<?php
namespace Nng\Nnhelpers\ViewHelpers\Uri;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Siehe `ImageViewHelper` für mehr Infos
 * 
 */
class ImageViewHelper extends \Nng\Nnhelpers\ViewHelpers\ImageViewHelper
{
	use CompileWithRenderStatic;

	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
		$src = $arguments['src'] ?: $renderChildrenClosure();

		$fileObj = \nn\wp::File()->process( $src, $arguments, true );
		if (!$fileObj) return '';

		return $fileObj->publicUrl;
	}

}
