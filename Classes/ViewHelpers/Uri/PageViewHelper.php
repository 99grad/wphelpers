<?php
namespace Nng\Nnhelpers\ViewHelpers\Uri;

use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * 
 */
class PageViewHelper extends PostViewHelper 
{
    /**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('pageUid', 'integer', 'UID of the post to link to');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $arguments['postId'] = $arguments['postId'] ?: $arguments['pageUid'];
		return parent::renderStatic( $arguments, $renderChildrenClosure, $renderingContext );
	}
}
