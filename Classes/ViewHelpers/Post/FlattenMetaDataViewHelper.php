<?php
namespace Nng\Nnhelpers\ViewHelpers\Post;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Makes it easy to access
 * 
 * ```
 * // before:
 * {post._meta._myextkey_price.0->f:debug()}
 * 
 * // after:
 * {nn:post.flattenMetaData(prefix:'myextkey')}
 * {price->f:debug()}
 * ```
 * 
 * Options:
 * ```
 * 
 * // use specific `post`-data
 * {nn:post.flattenMetaData(post:post, prefix:'myextkey')}
 * {post->nn:post.flattenMetaData(prefix:'myextkey', name:'setvarname')}
 * {post->nn:post.flattenMetaData(prefix:'myextkey', name:'setvarname')}
 * 
 * // use `post` from fluid-variables
 * {nn:post.flattenMetaData(prefix:'myextkey', name:'setvarname')}
 * ```
 * 
 */
class FlattenMetaDataViewHelper extends AbstractViewHelper
{
	use CompileWithRenderStatic;

	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('post', 'array', 'Post-data');
		$this->registerArgument('prefix', 'string', 'prefix of meta-data to flatten');
		$this->registerArgument('name', 'string', 'variable to set (optional)');
	}

	/**
	 * @return null
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
		
		$provider = $renderingContext->getVariableProvider();

		$post = $arguments['post'] ?? $renderChildrenClosure() ?: $provider->get('post');
		$prefix = $arguments['prefix'] ?: '';
		$name = $arguments['name'] ?? '';

		$postMeta = $post['_meta'] ?? [];

		$vars = [];

		foreach ($postMeta as $k=>$v) {
			$v = array_pop($v);
			if ($prefix) {
				$k = str_replace("_{$prefix}_", '', $k);
			} else {
				$k = preg_replace("/_([^_]*)_(.*)/", '\2', $k);
			}
			if (!$name) {
				$provider->add($k, $v);
			}
			$vars[$k] = $v;
		}

		if ($name) {
			$provider->add($name, $vars);
		}

	}

}
