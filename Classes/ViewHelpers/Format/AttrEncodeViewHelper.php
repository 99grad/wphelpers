<?php
namespace Nng\Nnhelpers\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Encode eines Arrays oder Strings für Verwendung in einem HTML-Attribut
 * 
 * ```
 * {data->nn:format.attrEncode()}
 * {nn:format.attrEncode(str:'Hallöchen!')}
 * ```
 * 
 */
class AttrEncodeViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('str', null, 'string or array to encode');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $str = $arguments['str'] ?: $renderChildrenClosure();
        if (!is_string($str)) {
            $str = json_encode($str);
        }
        return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
    }

}
