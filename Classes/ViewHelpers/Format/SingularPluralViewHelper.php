<?php
namespace Nng\Nnhelpers\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * ```
 * {str->f:format.singularPlural(items:items, delimiter:'|')}
 * ```
 */
class SingularPluralViewHelper extends AbstractViewHelper
{
	use CompileWithRenderStatic;

	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('str', 'string', 'String with singular|plural text');
		$this->registerArgument('items', null, 'Array');
		$this->registerArgument('delimiter', 'string', 'Delimiter');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
		$str = $arguments['str'] ?: $renderChildrenClosure();
		$parts = \nn\wp::Arrays($str)->trimExplode($arguments['delimiter'] ?: '|');
		$items = $arguments['items'];
		$isSingular = is_array($items) && count($items) <= 1;
		return count($parts) == 1 || $isSingular ? $parts[0] : $parts[1];
	}

}
