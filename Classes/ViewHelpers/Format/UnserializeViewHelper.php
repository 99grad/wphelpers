<?php
namespace Nng\Nnhelpers\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * ```
 * {str->nn:format.unserialize()}
 * {nn:format.unserialize(obj:str)}
 * ```
 */
class UnserializeViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('str', null, 'String to unserialize');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $str = $arguments['str'] ?: $renderChildrenClosure();
        if (!is_string($str)) return $str;
        if (is_numeric($str)) return [];
        return unserialize($str) ?: [];
    }

}
