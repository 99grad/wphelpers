<?php
namespace Nng\Nnhelpers\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Shorten an URL to the domain, strip the path behind the domain
 * 
 * ```
 * // www.99grad.de
 * {myurl->nn:format.shortUrl()}
 * {nn:format.shortUrl(url:'https://www.99grad.de/some/path')}
 * ```
 */
class ShortUrlViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('url', 'string', 'URL to shorten');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $str = $arguments['url'] ?: $renderChildrenClosure();
        $str = preg_replace('/((http)?s)?((:\/\/)?)([^\/]*)(.*)/', '\5', $str);
        return $str;
    }

}
