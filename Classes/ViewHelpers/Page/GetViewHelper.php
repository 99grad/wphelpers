<?php
namespace Nng\Nnhelpers\ViewHelpers\Page;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Holt die Daten zu einer Seite.
 * Da Seite = Beitrag = Media ist das hier ein Alias zu `{nn:post.get(ID:14)}`
 * ```
 * {nn:page.get(uid:14)->f:debug()}
 * ```
 */
class GetViewHelper extends \Nng\Nnhelpers\ViewHelpers\Post\GetViewHelper
{
    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('uid', 'integer', 'UID of the page to get');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $arguments['ID'] = $arguments['ID'] ?: $arguments['uid'] ?: $renderChildrenClosure();
        return parent::renderStatic( $arguments, $renderChildrenClosure, $renderingContext );
    }

}
