<?php
namespace Nng\Nnhelpers\ViewHelpers\Arrays;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Find something in an array
 * 
 * ```
 * {nn:arrays.in(arr:myArr, value:'1')}
 * {myArray->nn:arrays.in(value:'1')}
 * {myValue->nn:arrays.in(arr:myArr)}
 * ```
 */
class InViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('arr', 'mixed', 'Array to check', false, false);
        $this->registerArgument('value', 'mixed', 'value to find', false, false);
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $child = $renderChildrenClosure();
        $arr = $arguments['arr'];
        $val = $arguments['value'];

        if ($arr === false) {
            $arr = $child;
        }
        if ($val === false) {
            $val = $child;
        }
        if (!is_array($arr)) return false;
        return in_array( $val, $arr );
    }

}
