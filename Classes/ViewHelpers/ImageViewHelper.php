<?php
namespace Nng\Nnhelpers\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Image-ViewHelper, adaptiert für WordPress
 * Die meiste Logik übernimmt `\nn\wp::File()->process()`.
 * 
 * ```
 * <f:image src="{uid}" maxWidth="100" maxHeight="100" />
 * <f:image src="{uid}" width="100c" height="100" />
 * <f:image src="{uid}" class="nn-img-fluid" />
 * <f:image src="{uid}" data="{test:'hallo'}" />
 * <f:image src="{uid}" additionalAttributes="{myattr:'hallo'}" />
 * ```
 * 
 * `crop`			Array mit Koordinaten zum Beschneiden des Bildes:
 * 					['x'=>0, 'y'=>0, 'width'=>0.5, 'height'=>0.5]
 *  
 * `cropVariant`	Kann per `add_image_size()` definiert werden. Defaults sind: 
 * 					`thumb`, `thumbnail`, `medium`, `medium_large`, `large`, `post-thumbnail`
 * 					(ACHTUNG: noch nicht unterstützt!)
 */
class ImageViewHelper extends AbstractTagBasedViewHelper
{
	/**
	 * @var string
	 */
	protected $tagName = 'img';

	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerArgument('src', null, 'image URL oder uid');
		$this->registerArgument('width', null, 'Breite des Bildes');
		$this->registerArgument('height', null, 'Höhe des Bildes');
		$this->registerArgument('maxWidth', null, 'Breite des Bildes');
		$this->registerArgument('maxHeight', null, 'Höhe des Bildes');
		$this->registerArgument('cropVariant', null, 'Image Größe');
		$this->registerArgument('crop', 'array', 'Image Größe');
	}

	/**
	 * @return string
	 */
	public function render() {
		$src = $this->arguments['src'] ?: $this->renderChildren();
		
		$fileObj = \nn\wp::File()->process( $src, $this->arguments, true );
		if (!$fileObj) return '';

		$this->tag->addAttribute('src', $fileObj->publicUrl);
		$this->tag->addAttribute('width', $fileObj->width);
		$this->tag->addAttribute('height', $fileObj->height);

		$this->tag->forceClosingTag(true);
		return $this->tag->render();
	}

}
