<?php
namespace Nng\Nnhelpers\ViewHelpers\Link;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use nn\wp\GeneralUtility;

/**
 * 
 */
class TypolinkViewHelper extends AbstractTagBasedViewHelper
{
	/**
	 * @var string
	 */
	protected $tagName = 'a';
	
	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerArgument('parameter', 'mixed', 'UID of the post to link to or email, website etc.');
		$this->registerTagAttribute('target', 'string', 'Target of link', false);
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document', false);
	}

	/**
	 * @return string
	 */
	public function render() {

		$url = trim($this->arguments['parameter']);
		$child =  $this->renderChildren();
		
		if (!$url && $child) {
			$url = $child;
		}

		if (!$url) {
			return $child;
		}

		if (is_numeric($url)) {
			$url = \nn\wp::Post()->getLink($url);
		} 
		else if (strpos($url, '@') !== false) {
			$encryptedEmail = GeneralUtility::encryptEmail( 'mailto:' . $child, 2 ); 
			$url = sprintf(
				'javascript:nn_uncryptmailto(%s,%d);',
				rawurlencode(GeneralUtility::quoteJSvalue($encryptedEmail)),
				-2
			);
			$child = str_replace('@', '<i></i>', $child);
		}
		else if ((strpos($url, 'www') !== false || strpos($url, '.de') !== false) && strpos($url, 'http') === false) {
			$url = 'https://' . $url;
		}

		$this->tag->addAttribute('href', $url);
		if ($target = $this->arguments['target']) {
			$this->tag->addAttribute('target', $target);
		}
		if ($rel = $this->arguments['rel']) {
			$this->tag->addAttribute('rel', $rel);
		}
		$this->tag->setContent($child);
        $this->tag->forceClosingTag(true);

		return $this->tag->render();
	}

}
