<?php
namespace Nng\Nnhelpers\ViewHelpers\Link;

/**
 * 
 */
class PageViewHelper extends PostViewHelper {

    /**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('pageUid', 'integer', 'UID of the post to link to');
	}

	/**
	 * @return string
	 */
	public function render() {
        $this->arguments['postId'] = $this->arguments['postId'] ?: $this->arguments['pageUid'];
		return parent::render();
	}
}
