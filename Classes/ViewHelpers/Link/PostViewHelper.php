<?php
namespace Nng\Nnhelpers\ViewHelpers\Link;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * 
 */
class PostViewHelper extends AbstractTagBasedViewHelper
{
	/**
	 * @var string
	 */
	protected $tagName = 'a';
	
	/**
	 * @var boolean
	 */
	protected $escapeChildren = false;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = false;

	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerArgument('postId', 'integer', 'UID of the post to link to');
		$this->registerTagAttribute('target', 'string', 'Target of link', false);
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document', false);
	}

	/**
	 * @return string
	 */
	public function render() {

		$url = \nn\wp::Post()->getLink($this->arguments['postId']);
		$this->tag->addAttribute('href', $url);
		if ($target = $this->arguments['target']) {
			$this->tag->addAttribute('target', $target);
		}
		if ($rel = $this->arguments['rel']) {
			$this->tag->addAttribute('rel', $rel);
		}
		$this->tag->setContent($this->renderChildren());
        $this->tag->forceClosingTag(true);

		return $this->tag->render();
	}

}
