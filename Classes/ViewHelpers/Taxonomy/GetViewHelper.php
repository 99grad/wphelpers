<?php
namespace Nng\Nnhelpers\ViewHelpers\Taxonomy;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Get a taxonomy (category) from the table `terms`
 * 
 * ```
 * // get by UID
 * {nn:taxonomy.get(ID:33)->f:debug()}
 * {nn:taxonomy.get(ID:'33,34')->f:debug()}
 * {nn:taxonomy.get(ID:'{0:33,1:34}')->f:debug()}
 * 
 * // restrict to certain type
 * {nn:taxonomy.get(type:'category')->f:debug()}
 * 
 * // get as tree
 * {nn:taxonomy.get(type:'category', tree:1)->f:debug()}
 * ```
 */
class GetViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('ID', 'mixed', 'UID of the taxonomy to get');
        $this->registerArgument('type', 'string', 'type of taxonomy to get', false, 'category');
        $this->registerArgument('tree', 'boolean', 'get as tree');
    }

    /**
     * @return string
     */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $ID = $arguments['ID'] ?: $renderChildrenClosure();
        $type = $arguments['type'];
        $tree = $arguments['tree'];
        if ($tree) {
            return \nn\wp::SysCategory()->getTree( $ID, $type );
        }
        if ($ID) {
            return \nn\wp::SysCategory()->findByUid( $ID );
        }
        return \nn\wp::SysCategory()->findAll( $type );
    }

}
